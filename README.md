# tilting-point-back-end

> 

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/tilting-point-back-end; npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g feathers-cli             # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```

## Migrations
1. On the server should be environment "production"
export NODE_ENV=production
2. In root path of our project run:
node_modules/.bin/sequelize migration:create --name some-name-of-migration
3. Run migrate: npm run migrate

## PM2
pm2 start npm --name "tpc-backend" -- run <command>

## ACL Rules
1. All acl migrations is location in commands/acl. For acl we use node_acl and we chose mongo db for storing rules 
2. For migrating use this command - node src/commands/acl/201708181126-addBaseRules.js up. Where(201708181126 - year/month/day/hour/minutes)
3. For rejecting migrate use this command - node src/commands/acl/201708181126-addBaseRules.js down. Where(201708181126 - year/month/day/hour/minutes)
4. Important it does not work like postgres migrations. It need to watch which command you run. Because system does not save which migration was executed
## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2016

Licensed under the [MIT license](LICENSE).
