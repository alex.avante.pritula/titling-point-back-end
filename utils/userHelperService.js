class UserHelperService {
  constructor(app) {
    this.app = app;
  }

  /**
     Get link for recover password
      @param {String} code
      @return {String}
   */
  getRecoveryLink(code) {
    let constants = this.app.get('constants');
    let { HOST, PROTOCOL } = constants;
    PROTOCOL += '://';
    return `${PROTOCOL}${HOST}/recovery/${code}`;
  }

  /**
     Get link for registration of user
      @param {String} code
      @return {String}
   */
  getInviteLink(code) {
    let constants = this.app.get('constants');
    let { HOST, PROTOCOL } = constants;
    PROTOCOL += '://';
    return `${PROTOCOL}${HOST}/registration/${code}`;
  }
}

module.exports = function () {
  const app = this;
  app.set('userHelperService', new UserHelperService(app));
};
