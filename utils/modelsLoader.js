const fs = require('fs');
const path = require('path');

module.exports = function (app) {
  let dirName = __dirname + '/../models';
  let basename = path.basename(module.filename);
  let models = {};

  fs.readdirSync(dirName)
    .filter(function (file) {
      return (file.indexOf('.') !== 0) && (file !== basename);
    })
    .forEach(function (file) {
      let file_name = file.split('.');
      file_name = file_name[0];
      models[file_name] = require(path.join(dirName, file));
      models[file_name] = models[file_name](app);
    });

  return models;
};