const revalidator = require('revalidator');

function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      email: {
        description: '',
        format: 'email',
        type: 'string',
        required: true,
      },
      roleId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true,
        minimum: 1
      },
      departmentId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true,
        minimum: 1
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function patch(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: false
      },
      job: {
        description: '',
        type: 'string',
        maxLength: 255,
        required: false
      },
      phone: {
        description: '',
        type: 'string',
        required: false
      },
      biography: {
        description: '',
        type: 'string',
        maxLength: 255,
        required: false
      },
      contactInfo: {
        description: '',
        type: 'string',
        maxLength: 255
      },
      departmentId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: false,
        minimum: 1
      },
      roleId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: false,
        minimum: 1
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function patchCurrent(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: false
      },
      job: {
        description: '',
        type: 'string',
        maxLength: 255,
        required: false
      },
      phone: {
        description: '',
        type: 'string',
        required: false
      },
      biography: {
        description: '',
        type: 'string',
        maxLength: 255,
        required: false
      },
      contactInfo: {
        description: '',
        type: 'string',
        maxLength: 255
      },
      departmentId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: false,
        minimum: 1
      },
      password: {
        description: '',
        type: 'string',
        minLength: 6,
        required: false,
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function patchByCode(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      job: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      phone: {
        description: '',
        type: 'string',
        allowEmpty: false,
        required: true
      },
      biography: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      contactInfo: {
        description: '',
        type: 'string',
        maxLength: 255
      },
      departmentId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true,
        minimum: 1
      },
      password: {
        description: '',
        type: 'string',
        minLength: 6,
        required: true,
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

module.exports = {
  create: create,
  patchByCode: patchByCode,
  patchCurrent: patchCurrent,
  patch: patch
};