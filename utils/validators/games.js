const revalidator = require('revalidator');

function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      description: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: true,
        required: false
      },
      genres: {
        type: 'array',
        items: {
          type: 'string'
        },
        uniqueItems: true
      }
    }
  });


  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function getSimilarGames(data) {
  let validator = revalidator.validate(data, {
    properties: {
      genre: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function getAllGames(data) {
  let validator = revalidator.validate(data, {
    properties: {
      direction: {
        description: '',
        type: 'string',
        enum: ['desc', 'asc'],
        allowEmpty: true,
        required: false
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function update(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,

      },
      description: {
        description: '',
        type: 'string',
        maxLength: 255,
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function restrict(data) {
  let validator = revalidator.validate(data, {
    properties: {
      userId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true
      },
      ids: {
        type: 'array',
        items: {
          type: 'integer'
        },
        uniqueItems: true,
        minItems: 1
      }
    }
  });


  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

module.exports = {
  create: create,
  getSimilarGames: getSimilarGames,
  getAllGames: getAllGames,
  update: update,
  restrict: restrict
};
