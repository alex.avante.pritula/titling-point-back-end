module.exports = {
  blocks: require('./blocks'),
  items: require('./items'),
  news: require('./news'),
  links: require('./links'),
  games: require('./games'),
  users: require('./users')
};
