const revalidator = require('revalidator');

function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      title: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      newsDescription: {
        description: '',
        type: 'string',
        required: false
      },
      blockId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true
      },
      itemTypeId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

module.exports = {
  create: create,
};
