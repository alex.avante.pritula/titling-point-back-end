const revalidator = require('revalidator');

function create(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: true
      },
      dashboardId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true
      },
      blockTypeId: {
        description: '',
        type: 'integer',
        allowEmpty: false,
        required: true
      },
      position: {
        description: '',
        type: 'integer'
      }
    }
  });


  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

function update(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,

      },
      dashboardId: {
        description: '',
        type: 'integer',

      },
      blockTypeId: {
        description: '',
        type: 'integer',

      },
      position: {
        description: '',
        type: 'integer'
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

module.exports = {
  create: create,
  update:update
};
