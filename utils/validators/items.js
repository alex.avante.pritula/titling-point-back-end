const revalidator = require('revalidator');

function update(data) {
  let validator = revalidator.validate(data, {
    properties: {
      name: {
        description: '',
        type: 'string',
        maxLength: 255,
        allowEmpty: false,
        required: false
      }
    }
  });

  let err = null;
  if (!validator.valid) {
    err = {
      field: validator.errors[0].property,
      message: `${validator.errors[0].property} ${validator.errors[0].message}`,
    };
  }

  return err;
}

module.exports = {
  update: update,
};