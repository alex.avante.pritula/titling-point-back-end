const errors = require('feathers-errors');
const errorMethods = {
  400: 'BadRequest',
  401: 'NotAuthenticated',
  402: 'PaymentError',
  403: 'Forbidden',
  404: 'NotFound',
  405: 'MethodNotAllowed',
  406: 'NotAcceptable',
  408: 'Timeout',
  409: 'Conflict',
  411: 'LengthRequired',
  422: 'Unprocessable',
  429: 'TooManyRequests',
  500: 'GeneralError',
  501: 'NotImplemented',
  502: 'BadGateway',
  503: 'Unavailable'
};

module.exports = {
  getErrorObject: function (error) {
    if (typeof(error.code) === 'undefined') {
      return error;
    }
    return new errors[errorMethods[error.code]](error.message);
  }
};
