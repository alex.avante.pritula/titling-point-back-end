const AWS = require('aws-sdk');
const Promise = require('bluebird');
const path = require('path');
const uuid = require('uuid');
const mime = require('mime');
const size = require('s3-image-size');

class s3Service {
  constructor(configs){
    this.configs = Object.assign({}, configs);
    AWS.config.update(configs);
    this.s3 = new AWS.S3();
  }

  getSize(key) {
    const { bucket } =  this.configs;
    return new Promise((resolve, reject) => {
      size(this.s3, bucket, key, (err, dimensions, bytesRead) => {
        if (err) {
          return reject(err);
        }

        return resolve({
          dimensions,
          bytesRead
        });
      });
    });
  }

  renameDocument(oldPath, newPath){
    const { bucket } =  this.configs;

    let copyParams = {
      CopySource: `${bucket}/${oldPath}`,
      Key: newPath,
      ACL: 'public-read',
      Bucket: bucket
    };

    let deleteParams = {
      Key: oldPath,
      Bucket: bucket
    };

    return this.s3.copyObject(copyParams).promise()
      .then(() => {
        return this.s3.deleteObject(deleteParams);
      }).then(() => {
        return {};
      });
  }

  /*
  * todo - check
  * */
  renameFolder(oldPrefix, newPrefix){
    const {  bucket } =  this.configs;

    let options = {
      Bucket: bucket,
      Prefix: oldPrefix
    };

    let params = {
      Bucket: bucket
    };

    let copyParams = {};
    let dataObjects;

    return this.s3.listObjectsV2(options).promise()
      .then((data) => {
        if (!data) {
          return;
        }
        dataObjects = data;
        let filesMap = data.Contents.map(file => {
          copyParams = {
            CopySource: bucket + '/' + file.Key,
            Key: file.Key.replace(oldPrefix, newPrefix)
          };

          return this.s3.copyObject(copyParams).promise();
        });

        return Promise.all(filesMap);
      }).then(() => {
        if (!dataObjects) {
          return;
        }

        dataObjects.Contents.forEach(function (content) {
          params.Delete.Objects.push({Key: content.Key});
        });

        if (params.Delete.Objects.length > 0) {
          return this.s3.deleteObjects(params).promise();
        }
      }).then(() => {
        return {};
      });
  }

  deleteObjects(objects) {
    if (objects.length < 1) {
      return new Promise.resolve();
    }
    const {  bucket } =  this.configs;

    let params = {
      Bucket: bucket,
      Delete: {
        Objects: []
      }
    };

    objects.forEach(function (object) {
      params.Delete.Objects.push({Key: object});
    });

    return this.s3.deleteObjects(params).promise();
  }

  uploadDocument(target, data){
    const { bucket } =  this.configs;
    const { mimetype, buffer } = data;
    const ext = mime.extension(mimetype);

    let params = {
      Bucket: bucket,
      Key: path.join(target, uuid.v1() + '.' + ext),//uuid.v1() + '.' + ext,
      ContentType: mimetype,
      Body: new Buffer(buffer),
      ACL: 'public-read'
    };
    
    return this.s3.upload(params)
      .promise().then((result)=>{
        return {
          url : result.Location,
          name: result.Key
        };
      });
  }

  uploadDocuments(target, files){
    return Promise.mapSeries(files, (file)=>{
      return this.uploadDocument(target, file);
    });
  }

  deleteDocument(name){
    const {  bucket } =  this.configs;

    let options = {
      Bucket: bucket,
      Key: name
    };

    return this.s3.deleteObject(options).promise()
      .then((result)=>({ isDeleted: result.DeleteMarker }));
  }

  deleteDocuments(prefix) {
    const {  bucket } =  this.configs;

    let options = {
      Bucket: bucket,
      Prefix: prefix
    };

    let params = {
      Bucket: bucket
    };

    params.Delete = {Objects:[]};
    return this.s3.listObjectsV2(options).promise()
      .then((data) => {
        data.Contents.forEach(function(content) {
          params.Delete.Objects.push({Key: content.Key});
        });

        //if(data.Contents.length == 1000);
        if (params.Delete.Objects.length > 0) {
          return this.s3.deleteObjects(params).promise();
        }
      }).then((result) => {
        let deleted = {
          isDeleted: []
        };

        if (result) {
          deleted = {
            isDeleted: result.Deleted
          };
        }

        return deleted;

      });
  }
}

module.exports = function () {
  const app = this;
  const aws = app.get('aws');
  app.set('s3', new s3Service(aws));
};