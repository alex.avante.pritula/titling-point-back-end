const acl = require('acl');
const _ = require('lodash');
const errors = require('feathers-errors');

/*
 * todo
 * need to change logic and transfer to mongodb
 * */
const allowLimitedPermissions = {
  'api/v1/users': {
    /*'get': {
     'ADMIN': {
     'roles': ['USER']
     }
     },*/
    /*'find': {
     'ADMIN': ['USER']
     },*/
    'patch': {
      'ADMIN': {
        'roles': ['USER']
      }
    },
    'create': {
      'ADMIN': {
        'roles': ['USER']
      }
    },
    'remove': {
      'ADMIN': {
        'roles': ['USER']
      }
    }
  },
  'api/v1/games/restrict': {
    'find': {
      'ADMIN': {
        'roles': ['USER']
      }
    },
    'create': {
      'ADMIN': {
        'roles': ['USER']
      }
    },
    'remove': {
      'ADMIN': {
        'roles': ['USER']
      }
    }
  },
  'api/v1/users/:userId/resend': {
    'patch': {
      'ADMIN': {
        'roles': ['USER']
      }
    }
  }
};

//preliminary

class AclService {
  constructor(app) {
    this.app = app;
  }

  init() {
    const mongoService = this.app.get('mongoService');
    let self = this;
    return new Promise(function (resolve, reject) {
      mongoService.init().then((db) => {
        let mongodbBackend = new acl.mongodbBackend(db, '');
        let aclInstance = new acl(mongodbBackend);
        self.app.set('aclInstance', aclInstance);

        return resolve(aclInstance);
      }).catch((err) => {
        return reject(err);
      });
    });
  }

  getAllowLimitedPermissions() {
    return allowLimitedPermissions;
  }

  checkAllowUsersManipulation(handledData, data) {
    const constants = this.app.get('constants');
    const {USER_ROLES} = constants;
    let restrict = false;
    let currentRule = allowLimitedPermissions[data.path][data.method];
    let userRoleType = _.invert(USER_ROLES)[handledData.roleId];
    if (currentRule[data.roleType]['roles'].indexOf(userRoleType) == -1) {
      restrict = true;
    }

    return restrict;
  }

  /*let data = {
     params: hook.params,
     data: hook.data,
     path: hook.path,
     method: hook.method,
     roleType: roleType
   };*/
  checkAllowLimitedPermissions(data) {
    const sequelizeClient = this.app.get('sequelizeClient');
    let isVerified = true;
    let handledData = {};
    let userId = null;
    let isRestrict = true;

    switch (data.path) {
    case 'api/v1/users':
      switch (data.method) {
      case 'patch':
        if (!data.hook.id)
          return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));

        return sequelizeClient.models.users.getUserById(data.hook.id)
          .then((user) => {
            if (user == null)
              return Promise.reject(new errors.BadRequest('User is not exists', {errors: {}}, 400));
            handledData.roleId = user.role.id;
            isRestrict = this.checkAllowUsersManipulation(handledData, data);
            if (isRestrict) {
              return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
            }

            return isVerified;
          });
      case 'create':
        if (!data.data.roleId)
          return Promise.reject(new errors.BadRequest('Role id is required', {errors: {}}, 400));

        return new Promise((resolve, reject) => {
          handledData.roleId = data.data.roleId;
          isRestrict = this.checkAllowUsersManipulation(handledData, data);
          if (isRestrict) {
            return reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
          }

          return resolve(isVerified);
        });
      case 'remove':
        if (!data.hook.id)
          return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));

        return sequelizeClient.models.users.getUserById(data.hook.id)
          .then((user) => {
            if (user == null)
              return Promise.reject(new errors.BadRequest('User is not exists', {errors: {}}, 400));
            handledData.roleId = user.role.id;
            isRestrict = this.checkAllowUsersManipulation(handledData, data);
            if (isRestrict) {
              return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
            }

            return isVerified;
          });
      }
      break;
    case 'api/v1/games/restrict':
      switch (data.method) {
      case 'find':
        if (!data.params.query.userId)
          return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));

        return sequelizeClient.models.users.getUserById(data.params.query.userId)
          .then((user) => {
            if (user == null)
              return Promise.reject(new errors.BadRequest('User is not exists', {errors: {}}, 400));
            handledData.roleId = user.role.id;
            isRestrict = this.checkAllowUsersManipulation(handledData, data);
            if (isRestrict) {
              return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
            }

            return isVerified;
          });
      case 'create':
        if (!data.data.userId)
          return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));

        return sequelizeClient.models.users.getUserById(data.data.userId)
          .then((user) => {
            if (user == null)
              return Promise.reject(new errors.BadRequest('User is not exists', {errors: {}}, 400));
            handledData.roleId = user.role.id;
            isRestrict = this.checkAllowUsersManipulation(handledData, data);
            if (isRestrict) {
              return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
            }

            return isVerified;
          });
      case 'remove':
        if (!data.params.query.userId)
          return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));

        userId = data.params.query.userId;

        return sequelizeClient.models.users.getUserById(userId)
          .then((user) => {
            if (user == null)
              return Promise.reject(new errors.BadRequest('User is not exists', {errors: {}}, 400));
            handledData.roleId = user.role.id;
            isRestrict = this.checkAllowUsersManipulation(handledData, data);
            if (isRestrict) {
              return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
            }

            return isVerified;
          });
      }
      break;
    case 'api/v1/users/:userId/resend':
      switch (data.method) {
      case 'patch':
        if (!data.params.userId)
          return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));

        return sequelizeClient.models.users.getUserById(data.params.userId)
          .then((user) => {
            if (user == null)
              return Promise.reject(new errors.BadRequest('User is not exists', {errors: {}}, 400));
            handledData.roleId = user.role.id;
            isRestrict = this.checkAllowUsersManipulation(handledData, data);
            if (isRestrict) {
              return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 400));
            }

            return isVerified;
          });
      }
      break;
    default:
      return Promise.resolve();
    }
  }

  getUserRestrictGameIds(userId) {
    const aclInstance = this.app.get('aclInstance');
    let gameIds = [];
    let substring = 'games_';
    return aclInstance
      .whatResources(`user_${userId}`)
      .then((data) => {
        if (_.isEmpty(data)) {
          return [];
        }

        let resources = Object.keys(data);
        gameIds = resources.map(resource => {
          if (resource.indexOf(substring) !== -1) {
            return parseInt(resource.replace(substring, ''));
          }

          return;
        });

        return gameIds;
      });
  }

  removeUserRestrictGames(userId, ids) {
    const aclInstance = this.app.get('aclInstance');
    let resources = ids.map(id => {
      return `games_${id}`;
    });

    return aclInstance
      .removeAllow(`user_${userId}`, resources)
      .then(() => {
        return;
      });
  }
}

module.exports = function () {
  const app = this;
  app.set('aclService', new AclService(app));
};
