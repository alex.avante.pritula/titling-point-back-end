const authentication = require('feathers-authentication');
const jwt = require('feathers-authentication-jwt');
const local = require('feathers-authentication-local');
const { getErrorObject } = require('./errorHandler');

module.exports = function () {
  const app = this;
  const config = app.get('authentication');
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local(config.local));
  app.service('authentication').hooks({
    before: {
      create: [ authentication.hooks.authenticate(config.strategies) ],
      remove: [ authentication.hooks.authenticate('jwt') ]
    },
    error: {
      all: [ (hook) =>{ hook.error = getErrorObject(hook.error); }],
      find: [],
      get: [],
      create: [],
      update: [],
      patch: [],
      remove: []
    }
  });
};
