const winston = require('winston');
const path = require('path');
const logger = require('feathers-logger');

module.exports = function (logsDestination) {
  winston.configure({
    transports: [
      new (winston.transports.File)({
        level: 'info',
        name: 'info-file',
        filename: path.join(logsDestination, 'info.log'),
        json: true,
        colorize: false
      }),
      new (winston.transports.File)({
        level: 'error',
        name: 'error-file',
        filename: path.join(logsDestination, 'errors.log'),
        json: true,
        colorize: false
      }),
      new (winston.transports.Console)({
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true
      })
    ],
    exceptionHandlers: [
      new winston.transports.File({
        filename: path.join(logsDestination, 'exceptions.log'),
        json: true,
        colorize: false
      })
    ],
    exitOnError: false
  });

  return logger(winston);
};
