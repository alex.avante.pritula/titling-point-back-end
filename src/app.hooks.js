// Application hooks that run for every service
const { authenticate } = require('feathers-authentication').hooks;
const logger = require('./hooks/logger');
const _ = require('lodash');
const commonHooks = require('feathers-hooks-common');
const errors = require('feathers-errors');

function checkUnRestrictionMethods(hook) {
  let getPathes = [
    'users',
    'api/v1/users',
    'api/v1/users/code/:code'
  ];
  let findPathes = [
    'users',
    'api/v1/categories',
    'api/v1/users/code/:code'
  ];
  let patchPathes = [
    'api/v1/users/code/:code',
    'api/v1/users/password/reset'
  ];
  let updatePathes = [];
  let createPathes = [
    'users',
    'mailer',
    'authentication',
    'api/v1/users/password/reset'
  ];

  if(hook.method == 'get' && getPathes.indexOf(hook.path) !== -1) {
    return true;
  }

  if(hook.method == 'find' && findPathes.indexOf(hook.path) !== -1) {
    return true;
  }

  if(hook.method == 'create' && createPathes.indexOf(hook.path) !== -1) {
    return true;
  }

  if(hook.method == 'patch' && patchPathes.indexOf(hook.path) !== -1) {
    return true;
  }

  if(hook.method == 'update' && updatePathes.indexOf(hook.path) !== -1) {
    return true;
  }

  return false;
}

function checkUserPermissions(hook) {
  const sequelizeClient = hook.app.get('sequelizeClient');
  const constants = hook.app.get('constants');
  const {USER_ROLES} = constants;
  let aclInstance = hook.app.get('aclInstance');
  let aclService = hook.app.get('aclService');

  /**
   *  Just for unit testing purposes. Provider isn't set on Unit tests
   */
  if(!hook.params.provider) {
    return hook;
  }
  /*
  * todo
  * add to token role and userId
  * */
  const {userId} = hook.params.payload;
  return sequelizeClient.models.users
    .getUserById(userId).then((user) => {
      if (user.role.id === USER_ROLES.SUPER_ADMIN) {
        return hook;
      }

      let roleType = _.invert(USER_ROLES)[user.role.id];
      let data = {
        params: hook.params,
        data: hook.data,
        path: hook.path,
        method: hook.method,
        roleType: roleType,
        hook: hook
      };

      return aclInstance
        .isAllowed(`user_${userId}`,
          hook.path,
          hook.method)
        .then((res) => {
          if (!res) {
            return Promise.reject(new errors.BadRequest('Permission error', {errors: {}}, 401));
          }

          return aclService.checkAllowLimitedPermissions(data);
        }).then(() => {
          return hook;
        });
    });
}

function checkUserVerifying(hook) {
  const sequelizeClient = hook.app.get('sequelizeClient');

  if (hook.method == 'create' && hook.path == 'authentication') {
    if (!hook.data.email) {
      return Promise.reject(new errors.BadRequest('Error validation: email is empty' , {errors: {email: 'email is empty'}}, 400));
    }

    return sequelizeClient.models.users
      .getUserByEmail(hook.data.email)
      .then((user) => {
        if (!user || !user.isVerified) {
          return Promise.reject(new errors.BadRequest('Error validation: user is not verified' , {errors: {email: hook.data.email}}, 400));
        }
      });
  }

  return hook;
}

module.exports = {
  before: {
    all: [checkUserVerifying, commonHooks.iffElse(checkUnRestrictionMethods, [], [authenticate('jwt'), checkUserPermissions])],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [ logger() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [ logger() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
