const Sequelize = require('sequelize');
const logger = require('winston');
const sqlFormatter = require('sql-formatter');
const developmentEnv = process.env !== 'production';

module.exports = function () {
  const app = this;
  const {postgresConnectionUrl} = app.get('db');
  let logging = null;
  if(developmentEnv) {
    logging = (message) => logger.info(sqlFormatter.format(message));
  }
  const sequelize = new Sequelize(postgresConnectionUrl, {
    dialect: 'postgres',
    logging,
    define: {
      freezeTableName: true
    }
  });
  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function (...args) {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    });

    return result;
  };
};
