const mongodb = require('mongodb');
//const acl = require('acl');

class MongoService {
  constructor(app) {
    this.app = app;
  }

  init() {
    const {mongodbConnectionUrl} = this.app.get('db');
    let self = this;
    return new Promise(function(resolve, reject) {
      mongodb.connect(mongodbConnectionUrl, function(err, db) {
        if (err) {
          return reject(err);
        }

        self.app.set('mongoConnection', db);
        return resolve(db);
      });
    });
  }
}

module.exports = function () {
  const app = this;
  app.set('mongoService', new MongoService(app));
};
