const postgres = require('./postgresService');
const mongodb = require('./mongoService');

module.exports = {
  postgres,
  mongodb
};
