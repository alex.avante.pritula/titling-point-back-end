const multer = require('multer');
const uuidv4 = require('uuid/v4');
const multerS3 = require('multer-s3');
const maxSize = 50 * 1024 * 1024;

module.exports = function () {
  const app = this;
  const aws = app.get('aws');
  const { bucket } = aws;

  const s3Service = app.get('s3');
  let multipartMiddleware = multer({
    storage: multerS3({
      s3: s3Service.s3,
      bucket: bucket,
      acl: 'public-read',
      metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
      },
      key: function (req, file, cb) {
        if(file.mimetype.indexOf('image')=== -1){
          return cb(new Error('unsupported image format'), null);
        }
        cb(null, `general/${uuidv4()}.${file.originalname.split('.').pop()}`);
      }
    })/*,
    limits: { fileSize: maxSize }*/
  });

  app.use(
    multipartMiddleware.any('files'),
    (req,res,next) =>{
      req['feathers'].files = req.files;
      next();
    });
};