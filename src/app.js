const path = require('path');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');

const feathers = require('feathers');
const configuration = require('feathers-configuration');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
//const session = require('express-session');
const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const logger = require('../utils/logger.js');
const s3 = require('../utils/s3Service');
const userHelper = require('../utils/userHelperService');
const authentication = require('../utils/authentication');
const multer = require('./middleware/multer');
const morgan = require('morgan');
const {
  postgres,
  mongodb
} = require('./connectors');
const acl = require('../utils/aclService');
const app = feathers();
// Load app configuration
app.set('root', path.join(__dirname, '..'));
app.configure(configuration(app.get('root')));
/**
 * Need to add to each hook which need auth
 const { authenticate } = require('feathers-authentication').hooks;
 authenticate('jwt')
 */
const {destination} = app.get('logger');
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/swagger', feathers.static(app.get('swagger')));
app.use('/', feathers.static(app.get('public')));

// Set up Plugins and providers
app.configure(hooks());
app.configure(postgres);
app.configure(mongodb);
app.configure(acl);
app.configure(userHelper);

let aclService = app.get('aclService');
aclService.init();

app.configure(s3);
app.configure(rest());

app.use(morgan('dev'));

app.configure(logger(destination));
app.configure(authentication);
app.configure(multer);
app.configure(services);
app.configure(middleware);
app.hooks(appHooks);
module.exports = app;
