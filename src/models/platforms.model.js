const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const platforms = sequelizeClient.define('platforms', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    platform: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  platforms.associate = function (models) {
  };

  platforms.getExistingPlatformNames = function (platforms, options = {}) {
    return sequelizeClient.models.platforms.findAll({
      where: {
        platform: platforms
      },
      transaction: options.transaction
    });
  };

  platforms.createPlatforms = function (platforms, options = {}) {
    return sequelizeClient.models.platforms.bulkCreate(platforms, {
      transaction: options.transaction
    });
  };

  platforms.getPlatforms = function (limit = 10, offset = 0, options = {}) {
    return sequelizeClient.models.platforms.findAll({
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  return platforms;
};
