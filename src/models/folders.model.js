const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const folders = sequelizeClient.define('folders', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    tabId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'tab_id'
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  folders.associate = function (models) {
    this.belongsTo(models.tabs, {
      as: 'tab',
      foreignKey: 'tab_id'
    });
    this.hasMany(models['docs'], {
      as: 'docs',
      foreignKey: 'folder_id'
    });
  };

  /**
   * Function will make a query for one folder with associated creativities, tabs, games, docs.
   * @param id
   * @param options
   * @param limit
   * @param offset
   * @return {Promise.<Model>}
   */
  folders.getById = function (id, limit, offset, options = {}) {
    return sequelizeClient.models.folders.findOne({
      where: {
        id: id
      },
      include: [{
        model: sequelizeClient.models.tabs,
        as: 'tab',
        include: [{
          model: sequelizeClient.models.creativities,
          as: 'creativity',
          include: [{
            model: sequelizeClient.models.games,
            as: 'game'
          }]
        }]
      }, {
        model: sequelizeClient.models.docs,
        as: 'docs',
        limit: limit,
        offset: offset
      }],
      transaction: options.transaction
    });
  };

  folders.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.folders.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        tabId: data.tabId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  folders.decrementToPosition = function (data, options = {}) {
    return sequelizeClient.models.folders.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        tabId: data.tabId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  folders.createFolder = function (data, options = {}) {
    return sequelizeClient.models.folders.create({
      tabId: data.tabId,
      name: data.name,
      position: data.position,
    }, {
      transaction: options.transaction
    });
  };

  folders.updateFolder = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.folders.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  folders.deleteFolder = function (id, options = {}) {
    return sequelizeClient.models.folders.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  folders.moveOnCurrentPosition = function (data, options = {}) {
    return sequelizeClient.models.folders.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        tabId: data.tabId,
        position: {
          $and: [
            {$gte: data.position}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function get max position of folder in tab.
   * @param tabId
   * @param options
   * @return {Promise.<Model>}
   */
  folders.getMaxPosition = function (tabId, options = {}) {
    return sequelizeClient.models.folders.max(
      'position', {
        where: {
          tabId
        },
        transaction: options.transaction
      });
  };

  /**
   * Function decrement position of folders in tab.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  folders.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.folders.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        tabId: data.tabId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function increment position of folders in tab.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  folders.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.folders.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        tabId: data.tabId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      }
    }, {
      transaction: options.transaction
    });
  };

  return folders;
};
