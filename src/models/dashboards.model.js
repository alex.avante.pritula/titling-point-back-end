const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const dashboards = sequelizeClient.define('dashboards', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  dashboards.associate = function (models) {
    this.hasMany(models['blocks'], {
      as: 'blocks',
      foreignKey: 'dashboard_id'
    });
  };

  dashboards.getById = function (id, options = {}) {
    return sequelizeClient.models.dashboards.findOne({
      where: {
        id: id
      },
      include: [{
        model: sequelizeClient.models.blocks,
        as: 'blocks',
        include: [{
          model: sequelizeClient.models.items,
          as: 'items',
          include: [{
            model: sequelizeClient.models.news,
            as: 'news',
            include: [{
              model: sequelizeClient.models.news_images,
              as: 'newsImages',
            }]
          }, {
            model: sequelizeClient.models.links,
            as: 'links'
          }]
        }]
      }],
      transaction: options.transaction
    });
  };

  dashboards.getByIdOnlyDashboard = function (id, options = {}) {
    return sequelizeClient.models.dashboards.findOne({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  return dashboards;
};
