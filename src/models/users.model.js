const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define('users', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    email: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    password: {
      type: Sequelize.STRING,
      allowNull: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: true
    },

    job: {
      type: Sequelize.STRING,
      allowNull: true
    },

    phone: {
      type: Sequelize.STRING,
      allowNull: true
    },

    biography: {
      type: Sequelize.STRING,
      allowNull: true
    },

    contactInfo: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'contact_info'
    },

    avatarUrl: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'avatar_url'
    },

    roleId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'role_id'
    },

    departmentId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      require: false,
      field: 'department_id'
    },

    isVerified: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      field: 'is_verified'
    },

    isEnabled: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      field: 'is_enabled'
    },

    resetToken: {
      type: Sequelize.STRING,
      allowNull: true
    },

    verifyToken: {
      type: Sequelize.STRING,
      allowNull: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  users.associate = function (models) {
    this.belongsTo(models.roles, {
      as: 'role',
      foreignKey: 'role_id'
    });

    this.hasMany(models.confirm_codes, {
      as: 'confirmCodes',
      foreignKey: 'user_id'
    });

    this.belongsTo(models.departments, {
      as: 'department',
      foreignKey: 'department_id'
    });

    /*this.hasMany(models['items'], {
      as: 'items',
      foreignKey: 'user_id'
    });*/
  };

  users.getAndCountAll = function (filters, limit, offset, options = {}) {
    let whereRoleClause = {};
    let order = [];
    let whereClause = {};
    if (typeof filters['departmentId'] !== 'undefined') {
      whereClause = {
        departmentId: filters['departmentId']
      };
    }

    if (typeof filters['userId'] !== 'undefined') {
      whereClause.id = {
        $not: filters['userId']
      };
    }

    if (typeof filters['createdAt'] !== 'undefined') {
      whereClause.createdAt = {$gt: filters['createdAt']};
    }

    if (typeof filters['isVerified'] !== 'undefined') {
      whereClause.isVerified = filters['isVerified'];
    }

    if (typeof filters['admin_allow_role_view'] !== 'undefined') {
      whereRoleClause.id = {
        $in: filters['admin_allow_role_view']
      };
    }

    if (typeof filters['roleId'] !== 'undefined') {
      if (typeof filters['admin_allow_role_view'] !== 'undefined') {
        if (filters['admin_allow_role_view'].includes(filters['roleId'])) {
          whereRoleClause.id = filters['roleId'];
        } else {
          whereRoleClause.id = {
            $in: []
          };
        }
      } else {
        whereRoleClause.id = filters['roleId'];
      }
    }

    if (typeof filters['sortRoleId'] !== 'undefined') {
      order.push(['roleId', filters['sortRoleId']]);
    }

    if (typeof filters['sortCreatedAt'] !== 'undefined') {
      order.push(['createdAt', filters['sortCreatedAt']]);
    }

    if (typeof filters['sortIsVerified'] !== 'undefined') {
      order.push(['isVerified', filters['sortIsVerified']]);
    }

    return sequelizeClient.models.users.findAndCountAll({
      where: whereClause,
      limit: limit,
      offset: offset,
      attributes: [
        'id',
        'name',
        'email',
        'phone',
        'job',
        'biography',
        ['contact_info', 'contactInfo'],
        ['avatar_url', 'avatarUrl'],
        ['department_id', 'departmentId'],
        ['is_verified', 'isVerified'],
        ['is_enabled', 'isEnabled'],
        ['created_at', 'createdAt'],
        ['updated_at', 'updatedAt']
      ],
      include: [{
        model: sequelizeClient.models.roles,
        'as': 'role',
        where: whereRoleClause
      }, {
        model: sequelizeClient.models.departments,
        'as': 'department'
      }],
      order: order,
      transaction: options.transaction
    });
  };

  users.getOnlyUserById = function (userId, options = {}) {
    return sequelizeClient.models.users.findOne({
      where: {
        id: userId
      },
      transaction: options.transaction
    });
  };

  users.getUserById = function (userId, filters = {}, options = {}) {
    let whereRoleClause = {};
    if (typeof filters['admin_allow_role_view'] !== 'undefined') {
      whereRoleClause.id = {
        $in: filters['admin_allow_role_view']
      };
    }
    return sequelizeClient.models.users.findOne({
      where: {
        id: userId
      },
      attributes: [
        'id',
        'email',
        'name',
        'phone',
        'job',
        'biography',
        ['contact_info', 'contactInfo'],
        ['avatar_url', 'avatarUrl'],
        ['department_id', 'departmentId'],
        ['is_verified', 'isVerified'],
        ['is_enabled', 'isEnabled'],
        ['created_at', 'createdAt'],
        ['updated_at', 'updatedAt']
      ],
      include: [{
        model: sequelizeClient.models.roles,
        'as': 'role',
        where: whereRoleClause
      }, {
        model: sequelizeClient.models.departments,
        'as': 'department'
      }],
      transaction: options.transaction
    });
  };

  users.getFullUserById = function (userId, filters = {}, options = {}) {
    let whereRoleClause = {};
    if (typeof filters['admin_allow_role_view'] !== 'undefined') {
      whereRoleClause.id = {
        $in: filters['admin_allow_role_view']
      };
    }
    return sequelizeClient.models.users.findOne({
      where: {
        id: userId
      },
      include: [{
        model: sequelizeClient.models.roles,
        'as': 'role',
        where: whereRoleClause
      }, {
        model: sequelizeClient.models.departments,
        'as': 'department'
      }],
      transaction: options.transaction
    });
  };

  users.getUserByEmail = function (email, options = {}) {
    return sequelizeClient.models.users.findOne({
      where: {
        email: email
      },
      attributes: [
        'id',
        'email',
        'name',
        'phone',
        'job',
        'biography',
        ['contact_info', 'contactInfo'],
        ['avatar_url', 'avatarUrl'],
        ['department_id', 'departmentId'],
        ['is_verified', 'isVerified'],
        ['is_enabled', 'isEnabled'],
        ['created_at', 'createdAt'],
        ['updated_at', 'updatedAt']
      ],
      include: [{
        model: sequelizeClient.models.roles,
        'as': 'role',
      },{
        model: sequelizeClient.models.departments,
        'as': 'department'
      }]
    }, {
      transaction: options.transaction
    });
  };

  users.getUserByCode = function (code, options = {}) {
    return sequelizeClient.models.confirm_codes.findOne({
      where: {
        code: code
      },
      include: [{
        model: sequelizeClient.models.users,
        'as': 'user',
        attributes: [
          'id',
          'email',
          'job',
          'biography',
          ['contact_info', 'contactInfo'],
          ['avatar_url', 'avatarUrl'],
          ['department_id', 'departmentId'],
          ['is_verified', 'isVerified'],
          ['is_enabled', 'isEnabled'],
          ['created_at', 'createdAt'],
          ['updated_at', 'updatedAt']
        ]
      }],
      transaction: options.transaction
    }).then((code) => {
      if (!code) {
        return {};
      }

      return code.user;
    });
  };

  users.createUser = function (data, options = {}) {
    return sequelizeClient.models.users.create(data, {
      transaction: options.transaction
    });
  };

  users.updateUser = function (userId, toUpdate, options = {}) {
    return sequelizeClient.models.users.update(toUpdate, {
      where: {
        id: userId
      },
      transaction: options.transaction,
      returning: true
    });
  };

  users.updateResetToken = function (userId, resetToken, options = {}) {
    return sequelizeClient.models.users.update({
      resetToken: resetToken
    }, {
      where: {
        id: userId
      },
      transaction: options.transaction,
      returning: true
    });
  };

  users.updatePasswordByResetToken = function (resetToken, password, options = {}) {
    return sequelizeClient.models.users.update({
      password: password
    }, {
      where: {
        resetToken: resetToken
      },
      transaction: options.transaction,
      returning: true
    });
  };

  users.getUserByResetToken = function (resetToken, options = {}) {
    return sequelizeClient.models.users.findOne({
      where: {
        resetToken: resetToken
      },
      attributes: [
        'id',
        'email',
        'name',
        'phone',
        'job',
        'biography',
        ['contact_info', 'contactInfo'],
        ['avatar_url', 'avatarUrl'],
        ['department_id', 'departmentId'],
        ['is_verified', 'isVerified'],
        ['is_enabled', 'isEnabled'],
        ['created_at', 'createdAt'],
        ['updated_at', 'updatedAt']
      ],
      include: [{
        model: sequelizeClient.models.roles,
        'as': 'role',
      },{
        model: sequelizeClient.models.departments,
        'as': 'department'
      }]
    }, {
      transaction: options.transaction
    });
  };

  users.deleteUser = function (id, options = {}) {
    return sequelizeClient.models.users.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };
  return users;
};
