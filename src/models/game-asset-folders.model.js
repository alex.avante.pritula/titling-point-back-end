const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const gameAssetFolders = sequelizeClient.define('game_asset_folders', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    gameId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'game_id'
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      required: true
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  gameAssetFolders.associate = function (models) {
    this.belongsTo(models.games, {
      as: 'game',
      foreignKey: 'game_id'
    });
    this.hasMany(models['game_asset_docs'], {
      as: 'gameAssetDocs',
      foreignKey: 'folder_id'
    });
  };

  gameAssetFolders.getFolders = function(filters = {}, limit = 10, offset = 0, options = {}) {
    let whereClause = {};
    if (typeof filters['game_id'] !== 'undefined') {
      whereClause.gameId = filters['game_id'];
    }

    return sequelizeClient.models.game_asset_folders.findAndCountAll({
      where: whereClause,
      include: [{
        model: sequelizeClient.models.games,
        as: 'game'
      }],
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  gameAssetFolders.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_folders.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        gameId: data.gameId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  gameAssetFolders.createAssetFolder = function (data, options = {}) {
    return sequelizeClient.models.game_asset_folders.create({
      gameId: data.gameId,
      name: data.name,
      position: data.position,
    }, {
      transaction: options.transaction
    });
  };

  gameAssetFolders.updateAssetFolder = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.game_asset_folders.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  gameAssetFolders.deleteAssetFolder = function (id, options = {}) {
    return sequelizeClient.models.game_asset_folders.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  gameAssetFolders.moveOnCurrentPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_folders.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        gameId: data.gameId,
        position: {
          $and: [
            {$gte: data.position}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function decrement position of folders in tab.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetFolders.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_folders.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        gameId: data.gameId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function increment position of folders in tab.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetFolders.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_folders.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        gameId: data.gameId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      }
    }, {
      transaction: options.transaction
    });
  };

  /**
   * Function get max position of folder in tab.
   * @param gameId
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetFolders.getMaxPosition = function (gameId, options = {}) {
    return sequelizeClient.models.game_asset_folders.max(
      'position', {
        where: {
          gameId
        },
        transaction: options.transaction
      });
  };

  /**
   * Function will make a query for one folder with associated games, asset docs.
   * @param id
   * @param options
   * @param limit
   * @param offset
   * @return {Promise.<Model>}
   */
  gameAssetFolders.getById = function (id, limit, offset, options = {}) {
    return sequelizeClient.models.game_asset_folders.findOne({
      where: {
        id: id
      },
      include: [{
        model: sequelizeClient.models.games,
        as: 'game'
      }, {
        model: sequelizeClient.models.game_asset_docs,
        as: 'gameAssetDocs',
        limit: limit,
        offset: offset
      }],
      transaction: options.transaction
    });
  };

  gameAssetFolders.updateFolder = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.game_asset_folders.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  return gameAssetFolders;
};
