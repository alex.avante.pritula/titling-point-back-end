const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativityPlatforms = sequelizeClient.define('creativity_platforms', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    platformId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'platform_id'
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'creativity_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  creativityPlatforms.associate = function (models) {
    this.belongsTo(models.categories, {
      as: 'platform',
      foreignKey: 'platform_id'
    });

    this.belongsTo(models.creativities, {
      as: 'creativity',
      foreignKey: 'creativity_id'
    });
  };

  creativityPlatforms.connectCreativityPlatforms = function(creativityPlatforms, options = {}) {
    return sequelizeClient.models.creativity_platforms.bulkCreate(creativityPlatforms, {
      transaction: options.transaction
    });
  };

  creativityPlatforms.deleteCreativityPlatforms = function (data, options = {}) {
    return sequelizeClient.models.creativity_platforms.destroy({
      where: {
        creativityId: data.creativityId,
        platformId: data.platformIds,
      },
      transaction: options.transaction
    });
  };

  return creativityPlatforms;
};
