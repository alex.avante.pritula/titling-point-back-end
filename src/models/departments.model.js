const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const departments = sequelizeClient.define('departments', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  departments.associate = function (models) {
    this.hasMany(models.users, {
      as: 'users',
      foreignKey: 'department_id'
    });
  };

  departments.getRole = function (departmentId, options = {}) {
    return sequelizeClient.models.departments.findOne({
      where: {
        id: departmentId
      },
      transaction: options.transaction
    });
  };

  return departments;
};
