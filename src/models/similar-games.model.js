const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const similarGames = sequelizeClient.define('similar_games', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    gameId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'game_id'
    },

    similarGameId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'similar_game_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    indexes: [
      {
        unique: true,
        fields: ['game_id', 'similar_game_id']
      }
    ],
    underscored: true
  });

  similarGames.associate = function (models) {
    this.belongsTo(models.games, {
      as: 'game',
      foreignKey: 'game_id'
    });

    this.belongsTo(models.games, {
      as: 'similarGame',
      foreignKey: 'similar_game_id'
    });
  };

  similarGames.connectGameSimilarGames = function (gameSimilarGames, options = {}) {
    return sequelizeClient.models.similar_games.bulkCreate(gameSimilarGames, {
      transaction: options.transaction
    });
  };

  similarGames.deleteSimilarGames = function (gameId, similarGameIds, options = {}) {
    return sequelizeClient.models.similar_games.destroy({
      where: {
        gameId: gameId,
        similarGameId: similarGameIds
      },
      transaction: options.transaction
    });
  };

  similarGames.addSimilarGames = function (data, options = {}) {
    return sequelizeClient.models.similar_games.bulkCreate(data, {
      transaction: options.transaction
    });
  };

  return similarGames;
};
