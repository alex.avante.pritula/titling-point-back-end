const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const statuses = sequelizeClient.define('statuses', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    status: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  // eslint-disable-next-line no-unused-vars
  statuses.associate = function (models) {
  };

  statuses.getExistingStatusNames = function (statuses, options = {}) {
    return sequelizeClient.models.statuses.findAll({
      where: {
        status: statuses
      },
      transaction: options.transaction
    });
  };

  statuses.createStatuses = function (statuses, options = {}) {
    return sequelizeClient.models.statuses.bulkCreate(statuses, {
      transaction: options.transaction
    });
  };

  statuses.getStatuses = function (limit = 10, offset = 0, options = {}) {
    return sequelizeClient.models.statuses.findAll({
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  return statuses;
};
