const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const tabs = sequelizeClient.define('tabs', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'creativity_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  tabs.associate = function (models) {
    this.belongsTo(models.creativities, {
      as: 'creativity',
      foreignKey: 'creativity_id'
    });
    this.hasMany(models['folders'], {
      as: 'folders',
      foreignKey: 'tab_id'
    });
  };

  tabs.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.tabs.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        creativityId: data.creativityId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  tabs.createTab = function (data, options = {}) {
    return sequelizeClient.models.tabs.create({
      creativityId: data.creativityId,
      name: data.name,
      position: data.position,
    }, {
      transaction: options.transaction
    });
  };

  tabs.updateTab = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.tabs.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  tabs.deleteTab = function (id, options = {}) {
    return sequelizeClient.models.tabs.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  tabs.moveOnCurrentPosition = function (data, options = {}) {
    return sequelizeClient.models.tabs.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        creativityId: data.creativityId,
        position: {
          $and: [
            {$gte: data.position}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function get max position of tab in creativity.
   * @param creativityId
   * @param options
   * @return {Promise.<Model>}
   */
  tabs.getMaxPosition = function (creativityId, options = {}) {
    return sequelizeClient.models.tabs.max(
      'position', {
        where: {
          creativityId
        },
        transaction: options.transaction
      });
  };

  /**
   * Function decrement position of tabs in creativity.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  tabs.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.tabs.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        creativityId: data.creativityId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function increment position of tabs in creativity.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  tabs.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.tabs.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        creativityId: data.creativityId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      }
    }, {
      transaction: options.transaction
    });
  };

  /**
   * Function gets tab with folders.
   * @param id
   * @param limit
   * @param offset
   * @param options
   * @return {Promise.<Model>}
   */
  tabs.getById = function (id, limit, offset, options = {}, filters = {}) {
    let includeClause = [];

    if (typeof filters['includeDocs'] !== 'undefined') {
      includeClause = [{
        model: sequelizeClient.models.docs,
        as: 'docs'
      }];
    }

    return sequelizeClient.models.tabs.findOne({
      where: {
        id: id
      },
      include: [{
        model: sequelizeClient.models.folders,
        as: 'folders',
        limit: limit,
        offset: offset,
        include: includeClause
      }, {
        model: sequelizeClient.models.creativities,
        as: 'creativity',
        include: [{
          model: sequelizeClient.models.games,
          as: 'game'
        }]
      }],
      transaction: options.transaction
    });
  };

  return tabs;
};
