const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const genres = sequelizeClient.define('genres', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    genre: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  genres.associate = function (models) {
    this.belongsToMany(models.games, {
      through: {
        model: models.game_genres
      },
      foreignKey: 'genre_id'
    });
  };

  genres.getAll = function (genre, limit, offset, options = {}) {
    return sequelizeClient.models.genres.findAll({
      where: {
        genre: {
          ilike: `${genre}%`
        }
      },
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  genres.getAllByName = function (genres, limit, offset, options = {}) {
    return sequelizeClient.models.genres.findAll({
      where: {
        genre: genres
      },
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  genres.getGenres = function (limit, offset, options = {}) {
    return sequelizeClient.models.genres.findAll({
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  genres.createGenres = function (genres, options = {}) {
    return sequelizeClient.models.genres.bulkCreate(genres, {
      transaction: options.transaction
    });
  };

  genres.getExitstingGenreNames = function (genres, options = {}) {
    return sequelizeClient.models.genres.findAll({
      where: {
        genre: genres
      },
      transaction: options.transaction
    });
  };

  return genres;
};
