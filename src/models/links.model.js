const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const links = sequelizeClient.define('links', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    title: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    description: {
      type: Sequelize.STRING,
      allowNull: true
    },

    url: {
      type: Sequelize.TEXT,
      allowNull: true
    },

    itemId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'item_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  links.associate = function (models) {
    this.belongsTo(models.items, {
      as: 'item',
      foreignKey: 'item_id'
    });
  };

  links.createLink = function (data, options = {}) {
    return links.create({
      itemId: data.itemId,
      title: data.title,
      description: data.linkDescription,
      url: data.url
    }, {
      transaction: options.transaction
    });
  };

  links.getLinkById = function (id, options = {}) {
    return links.findById(id, {
      transaction: options.transaction
    });
  };

  links.getCount = function (data, options = {}) {
    return links.count({
      where: {
        itemId: data.itemId
      },
      transaction: options.transaction
    });
  };

  links.updateLink = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.links.update(
      toUpdate, {
        where: {
          id: id
        },
        transaction: options.transaction
      });
  };

  links.deleteLink = function (id, options = {}) {
    return sequelizeClient.models.links.destroy({
      where: {
        id
      },
      transaction: options.transaction
    });
  };

  links.deleteLinksByItemIdAndIds = function (ids, itemId, options = {}) {
    return sequelizeClient.models.links.destroy({
      where: {
        id: ids,
        itemId
      },
      transaction: options.transaction
    });
  };

  return links;
};
