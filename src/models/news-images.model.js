const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const newsImages = sequelizeClient.define('news_images', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    imageUrl: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      field: 'image_url'
    },

    newsId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'news_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  newsImages.associate = function (models) {
    this.belongsTo(models.news, {
      as: 'news',
      foreignKey: 'news_id'
    });
  };

  newsImages.deleteNewsImage = function (id, options = {}) {
    return sequelizeClient.models.news_images.destroy({
      where: {
        id
      },
      transaction: options.transaction
    });
  };

  newsImages.createImages = function (images, options = {}) {
    return sequelizeClient.models.news_images.bulkCreate(images, {
      transaction: options.transaction
    });
  };

  return newsImages;
};
