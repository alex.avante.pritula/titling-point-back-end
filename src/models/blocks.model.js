const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const blocks = sequelizeClient.define('blocks', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false
    },

    dashboardId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'dashboard_id'
    },

    blockTypeId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'block_type_id'
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false
    },

    created_at: {
      type: Sequelize.DATE,
    },

    updated_at: {
      type: Sequelize.DATE,
    }

  }, {
    underscored: true
  });

  blocks.associate = function (models) {
    this.hasMany(models['items'], {
      as: 'items',
      foreignKey: 'block_id'
    });

    this.belongsTo(models['block_types'], {
      as: 'blockType',
      foreignKey: 'block_type_id'
    });

    this.belongsTo(models['dashboards'], {
      as: 'dashboard',
      foreignKey: 'dashboard_id'
    });
  };

  blocks.createBlock = function (data, options = {}) {
    return sequelizeClient.models.blocks.create({
      dashboardId: data.dashboardId,
      name: data.name,
      blockTypeId: data.blockTypeId,
      position: data.position,
    }, {
      transaction: options.transaction
    });
  };

  blocks.updateBlock = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.blocks.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  blocks.findByPosition = function (dashboardId, position, options = {}) {
    return sequelizeClient.models.blocks.findAll({
      where: {
        dashboardId: dashboardId,
        position: position
      },
      transaction: options.transaction
    });
  };

  blocks.deleteBlock = function (id, options = {}) {
    return sequelizeClient.models.blocks.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  blocks.moveOnCurrentPosition = function (data, options = {}) {
    return sequelizeClient.models.blocks.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        dashboardId: data.dashboardId,
        position: {
          $and: [
            {$gte: data.position}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  blocks.getMaxPosition = function (dashboardId, options = {}) {
    return sequelizeClient.models.blocks.max(
      'position', {
        where: {
          dashboardId
        },
        transaction: options.transaction
      });
  };
  blocks.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.blocks.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        dashboardId: data.dashboardId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };
  blocks.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.blocks.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        dashboardId: data.dashboardId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      }
    }, {
      transaction: options.transaction
    });
  };
  blocks.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.blocks.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        dashboardId: data.dashboardId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  blocks.getById = function (id, options = {}) {
    return sequelizeClient.models.blocks.findOne({
      where: {
        id: id
      },
      order: [
        [sequelizeClient.models.items, 'position', 'ASC'],
      ],
      include: [{
        model: sequelizeClient.models.items,
        as: 'items',
        include: [{
          model: sequelizeClient.models.news,
          as: 'news',
          include: [{
            model: sequelizeClient.models.news_images,
            as: 'newsImages',
          }]
        }, {
          model: sequelizeClient.models.links,
          as: 'links'
        }]
      }],
      transaction: options.transaction
    });
  };

  blocks.getAll = function (limit, offset, options = {}) {
    return sequelizeClient.models.blocks.findAll({
      limit: limit,
      offset: offset,
      order: [
        ['position', 'ASC'],
        [sequelizeClient.models.items, 'position', 'ASC'],
      ],
      include: [{
        model: sequelizeClient.models.items,
        'as': 'items',
        include: [{
          model: sequelizeClient.models.news,
          as: 'news',
          include: [{
            model: sequelizeClient.models.news_images,
            as: 'newsImages',
          }]
        }, {
          model: sequelizeClient.models.links,
          as: 'links'
        }]
      }],
      transaction: options.transaction
    });
  };

  blocks.getCountBlocksInDashboard = function (id, options = {}) {
    return sequelizeClient.models.blocks.count({
      where: {
        dashboardId: id
      },
      transaction: options.transaction
    });
  };

  blocks.getBlocksByDashboardId = function (id, limit, offset, options = {}) {
    return sequelizeClient.models.blocks.findAll({
      where: {
        dashboardId: id
      },
      limit: limit,
      offset: offset,
      order: [
        ['position', 'ASC'],
        [sequelizeClient.models.items, 'position', 'ASC'],
      ],
      include: [{
        model: sequelizeClient.models.items,
        'as': 'items',
        include: [{
          model: sequelizeClient.models.news,
          as: 'news',
          include: [{
            model: sequelizeClient.models.news_images,
            as: 'newsImages',
          }]
        }, {
          model: sequelizeClient.models.links,
          as: 'links'
        }]
      }],
      transaction: options.transaction
    });
  };

  return blocks;
};
