const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const rolePermissions = sequelizeClient.define('permissions', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    permissionId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    roleId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  rolePermissions.associate = function (models) {
    this.belongsTo(models.roles, {
      as: 'role',
      foreignKey: 'role_id'
    });

    this.belongsTo(models.permissions, {
      as: 'permission',
      foreignKey: 'permission_id'
    });
  };

  return rolePermissions;
};
