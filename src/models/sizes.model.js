const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const sizes = sequelizeClient.define('sizes', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    width: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true
    },

    height: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  sizes.associate = function (models) {

  };

  sizes.getByWidthHeight = function (sizeWidth, sizeHeight, options = {}) {
    return sequelizeClient.models.sizes.findOne({
      where: {
        width: sizeWidth,
        height: sizeHeight
      },
      transaction: options.transaction
    });
  };

  sizes.createSize = function (data, options = {}) {
    return sequelizeClient.models.sizes.create(data, {
      transaction: options.transaction
    });
  };



  return sizes;
};
