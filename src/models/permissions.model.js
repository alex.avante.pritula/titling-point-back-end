const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const permissions = sequelizeClient.define('permissions', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    permission: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });


  // eslint-disable-next-line no-unused-vars
  permissions.associate = function (models) {
  };

  return permissions;
};
