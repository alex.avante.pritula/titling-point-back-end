const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const events = sequelizeClient.define('events', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    title: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    description: {
      type: Sequelize.STRING,
      allowNull: true
    },

    outlookId: {
      type: Sequelize.STRING,
      allowNull: false,
      required: true,
      field: 'outlook_id'
    },

    itemId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'item_id'
    },

    date: {
      type: Sequelize.DATE,
      allowNull: false,
      required: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  events.associate = function (models) {
    this.belongsTo(models.items, {
      as: 'item',
      foreignKey: 'item_id'
    });
  };

  return events;
};
