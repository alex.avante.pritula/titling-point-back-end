const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const items = sequelizeClient.define('items', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    itemTypeId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'item_type_id'
    },

    blockId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'block_id'
    },

    ownerId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      field: 'owner_id'
    },

    iconUrl: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'icon_url'
    },

    sourceUrl: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'source_url'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  items.associate = function (models) {
    this.belongsTo(models['blocks'], {
      as: 'block',
      foreignKey: 'block_id'
    });

    this.belongsTo(models['users'], {
      as: 'owner',
      foreignKey: 'owner_id'
    });

    this.hasOne(models.news, {
      as: 'news',
      foreignKey: 'item_id'
    });

    this.hasMany(models.links, {
      as: 'links',
      foreignKey: 'item_id'
    });
  };

  items.getMaxPosition = function (blockId, options = {}) {
    return sequelizeClient.models.items.max('position', {
      where: {
        blockId: blockId
      },
      transaction: options.transaction
    });
  };

  items.updateItem = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.items.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  items.deleteItem = function (id, options = {}) {
    return sequelizeClient.models.items.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  items.createItem = function (data, options = {}) {
    return sequelizeClient.models.items.create({
      blockId: data.blockId,
      name: data.name,
      itemTypeId: data.itemTypeId,
      position: data.position,
      ownerId: data.userId,
    }, {
      transaction: options.transaction
    });
  };

  items.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.items.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        blockId: data.blockId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  items.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.items.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        blockId: data.blockId,
        position:
          {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  items.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.items.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        blockId: data.blockId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  items.moveOnCurrentPosition = function (data, options = {}) {
    return sequelizeClient.models.items.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        blockId: data.blockId,
        position: {
          $and: [
            {$gte: data.position}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  items.getItemById = function (id, options = {}) {
    return sequelizeClient.models.items.findOne({
      where: {
        id: id
      },
      include: [{
        model: sequelizeClient.models.news,
        as: 'news',
        include: [{
          model: sequelizeClient.models.news_images,
          as: 'newsImages',
        }]
      }, {
        model: sequelizeClient.models.links,
        as: 'links',
        order: [['created_at', 'ASC']]
      }, {
        model: sequelizeClient.models.blocks,
        as: 'block'
      }, {
        model: sequelizeClient.models.users,
        as: 'owner',
        attributes: [
          'id',
          'name',
          'email',
          'phone',
          'job',
          'biography',
          ['contact_info', 'contactInfo'],
          ['avatar_url', 'avatarUrl'],
          ['department_id', 'departmentId'],
          ['is_verified', 'isVerified'],
          ['is_enabled', 'isEnabled'],
          ['created_at', 'createdAt'],
          ['updated_at', 'updatedAt']
        ],
        include: [{
          model: sequelizeClient.models.roles,
          'as': 'role'
        }]
      }],
      transaction: options.transaction
    });
  };

  items.findByFilters = function (filters, options = {}) {
    let whereClause = {};
    if (typeof filters['text'] !== 'undefined') {
      let text = filters['text'];
      whereClause = {
        $or: [
          {
            name: {
              ilike: `%${text}%`
            }
          },
          {
            '$news.title$': {
              ilike: `%${text}%`
            }
          },
          {
            '$links.title$': {
              ilike: `%${text}%`
            }
          },
          {
            '$links.description$': {
              ilike: `%${text}%`
            }
          }
        ]
      };
    }
    return sequelizeClient.models.items.findAll({
      where: whereClause,
      include: [{
        model: sequelizeClient.models.news,
        as: 'news',
        include: [{
          model: sequelizeClient.models.news_images,
          as: 'newsImages',
        }]
      }, {
        model: sequelizeClient.models.links,
        as: 'links'
      }, {
        model: sequelizeClient.models.blocks,
        as: 'block'
      }, {
        model: sequelizeClient.models.users,
        as: 'owner',
        attributes: [
          'id',
          'name',
          'email',
          'phone',
          'job',
          'biography',
          ['contact_info', 'contactInfo'],
          ['avatar_url', 'avatarUrl'],
          ['department_id', 'departmentId'],
          ['is_verified', 'isVerified'],
          ['is_enabled', 'isEnabled'],
          ['created_at', 'createdAt'],
          ['updated_at', 'updatedAt']
        ]
      }],
      transaction: options.transaction
    });
  };

  items.countByFilters = function (filters, options = {}) {
    let whereClause = {};
    let include = [];
    if (typeof filters['text'] !== 'undefined') {
      let text = filters['text'];
      whereClause = {
        $or: [
          {
            name: {
              ilike: `%${text}%`
            }
          },
          {
            '$news.title$': {
              ilike: `%${text}%`
            }
          },
          {
            '$links.title$': {
              ilike: `%${text}%`
            }
          },
          {
            '$links.description$': {
              ilike: `%${text}%`
            }
          }
        ]
      };

      include = [{
        model: sequelizeClient.models.news,
        as: 'news'
      }, {
        model: sequelizeClient.models.links,
        as: 'links'
      }];

    }
    return sequelizeClient.models.items.count({
      where: whereClause,
      include: include,
      transaction: options.transaction
    });
  };

  return items;
};
