const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const games = sequelizeClient.define('games', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    description: {
      type: Sequelize.STRING,
      allowNull: true
    },

    imageUrl: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'image_url'
    },

    publishDate: {
      type: Sequelize.DATE,
      field: 'publish_date'
    },

    ownerId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      field: 'owner_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  games.associate = function (models) {
    this.belongsTo(models.users, {
      as: 'owner',
      foreignKey: 'owner_id'
    });

    this.belongsToMany(models.genres, {
      through: {
        model: models.game_genres
      },
      foreignKey: 'game_id'
    });

    this.belongsToMany(models.games, {
      through: {
        model: models.similar_games
      },
      as: 'similarGames',
      foreignKey: 'gameId',
      otherKey: 'similar_game_id'
    });

    this.hasMany(models.creativities, {
      as: 'creativities',
      foreignKey: 'game_id'
    });

  };

  games.updateGame = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.games.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };


  games.getCountGames = function (filters = {}, options = {}) {
    let whereClause = {};
    let whereGenreClause = {};
    let whereSimilarClause = {};
    if (typeof filters['title'] !== 'undefined') {
      whereClause = {
        name: {
          ilike: `${filters['title']}%`
        }
      };
    }

    if (typeof filters['description'] !== 'undefined') {
      whereClause.description = {
        ilike: `${filters['description']}%`
      };
    }

    if (typeof filters['publishDate'] !== 'undefined') {
      whereClause.publishDate = {$gt: filters['publishDate']};
    }

    if (typeof filters['genre'] !== 'undefined') {
      whereGenreClause['genre'] = {
        ilike: `${filters['genre']}%`
      };
    }

    if (typeof filters['restrict_ids'] !== 'undefined') {
      whereClause.id = {
        $notIn: filters['restrict_ids']
      };

      whereSimilarClause.id = {
        $notIn: filters['restrict_ids']
      };
    }

    return sequelizeClient.models.games.count({
      distinct: 'id',
      where: whereClause,
      include: [{
        model: sequelizeClient.models.genres,
        as: 'genres',
        through: 'genre_id',
        where: whereGenreClause
      }],
      transaction: options.transaction
    });
  };

  games.getAll = function (filters, limit, offset, direction, options = {}) {
    let whereClause = {};
    let whereGenreClause = {};
    let whereSimilarClause = {};
    if (typeof filters['title'] !== 'undefined') {
      whereClause = {
        name: {
          ilike: `${filters['title']}%`
        }
      };
    }

    if (typeof filters['description'] !== 'undefined') {
      whereClause.description = {
        ilike: `${filters['description']}%`
      };
    }

    if (typeof filters['publishDate'] !== 'undefined') {
      whereClause.publishDate = {$gt: filters['publishDate']};
    }

    if (typeof filters['genre'] !== 'undefined') {
      whereGenreClause['genre'] = {
        ilike: `${filters['genre']}%`
      };
    }

    if (typeof filters['restrict_ids'] !== 'undefined') {
      whereClause.id = {
        $notIn: filters['restrict_ids']
      };

      whereSimilarClause.id = {
        $notIn: filters['restrict_ids']
      };
    }

    return sequelizeClient.models.games.findAll({
      where: whereClause,
      include: [{
        model: sequelizeClient.models.genres,
        as: 'genres',
        attributes: ['id', 'genre'],
        through: {
          model: sequelizeClient.models.game_genres,
          attributes: []
        },
        where: whereGenreClause
      }, {
        model: sequelizeClient.models.games,
        as: 'similarGames',
        attributes: ['id', 'name', 'description', 'imageUrl', 'publishDate'],
        through: {
          model: sequelizeClient.models.similar_games,
          attributes: []
        }
      }],
      limit: limit,
      offset: offset,
      attributes: ['id', 'name', 'description', 'imageUrl', 'publishDate', 'created_at'],
      order: [['createdAt', direction]],
      transaction: options.transaction
    });
  };

  games.getByGenre = function (genres, filters, limit, offset, options = {}) {
    let whereClause = {};
    if (typeof filters['restrict_ids'] !== 'undefined') {
      whereClause.id = {
        $notIn: filters['restrict_ids']
      };
    }

    return sequelizeClient.models.games.findAll({
      where: whereClause,
      include: [{
        model: sequelizeClient.models.genres,
        as: 'genres',
        through: 'genre_id',
        where: {
          genre: genres
        },
      }],
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  games.createGame = function (data, options = {}) {
    return sequelizeClient.models.games.create(data, {
      transaction: options.transaction
    });
  };

  games.getById = function (id, options = {}, filters = {}) {
    let includeDocsClause;
    let includeClause = [];

    let similarGamesInclude = {
      model: sequelizeClient.models.games,
      as: 'similarGames',
      attributes: ['id', 'name', 'description', 'imageUrl', 'publishDate'],
      through: {
        model: sequelizeClient.models.similar_games,
        attributes: []
      }
    };

    let genresInclude = {
      model: sequelizeClient.models.genres,
      as: 'genres',
      attributes: ['id', 'genre'],
      through: {
        model: sequelizeClient.models.game_genres,
        attributes: []
      }
    };

    let usersInclude = {
      model: sequelizeClient.models.users,
      as: 'owner',
      attributes: [
        'id',
        'name',
        'email',
        'phone',
        'job',
        'biography',
        ['contact_info', 'contactInfo'],
        ['avatar_url', 'avatarUrl'],
        ['department_id', 'departmentId'],
        ['is_verified', 'isVerified'],
        ['is_enabled', 'isEnabled'],
        ['created_at', 'createdAt'],
        ['updated_at', 'updatedAt']
      ],
      include: [{
        model: sequelizeClient.models.roles,
        'as': 'role'
      }]
    };

    includeClause.push(similarGamesInclude);
    includeClause.push(genresInclude);
    includeClause.push(usersInclude);

    if (typeof filters['includeDocs'] !== 'undefined') {
      includeDocsClause = {
        model: sequelizeClient.models.creativities,
        as: 'creativities',
        include: [{
          model: sequelizeClient.models.tabs,
          as: 'tabs',
          include: [{
            model: sequelizeClient.models.folders,
            as: 'folders',
            include: [{
              model: sequelizeClient.models.docs,
              as: 'docs'
            }]
          }]
        }]
      };

      includeClause.push(includeDocsClause);
    }

    return sequelizeClient.models.games.findOne({
      where: {
        id: id
      },
      include: includeClause,
      transaction: options.transaction
    });
  };

  games.getByIds = function (ids, limit, offset, options = {}) {
    return sequelizeClient.models.games.findAndCountAll({
      where: {
        id: ids
      },
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  games.deleteGame = function (id, options = {}) {
    return sequelizeClient.models.games.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  games.getExistingGames = function (gameIds, options = {}) {
    return sequelizeClient.models.games.findAll({
      where: {
        id: gameIds
      },
      transaction: options.transaction
    });
  };

  /**
   * Function will make a query for one game with associated genres and similar games.
   * @param id
   * @param options
   * @return {Promise.<Model>}
   */
  games.getGameWithGenresAndSimilarGames = function (id, options = {}) {
    return sequelizeClient.models.games.findOne({
      where: {
        id: id
      },
      attributes: ['id', 'name', 'description', 'imageUrl', 'publishDate'],
      include: [{
        model: sequelizeClient.models.genres,
        as: 'genres',
        attributes: ['id', 'genre'],
        through: {
          model: sequelizeClient.models.game_genres,
          attributes: []
        }
      }, {
        model: sequelizeClient.models.games,
        as: 'similarGames',
        attributes: ['id', 'name', 'description', 'imageUrl', 'publishDate'],
        through: {
          model: sequelizeClient.models.similar_games,
          attributes: []
        }
      }],
      transaction: options.transaction
    });
  };

  return games;
};
