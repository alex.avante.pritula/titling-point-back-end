const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativityCategories = sequelizeClient.define('creativity_categories', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    categoryId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'category_id'
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'creativity_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  creativityCategories.associate = function (models) {
    this.belongsTo(models.categories, {
      as: 'category',
      foreignKey: 'category_id'
    });

    this.belongsTo(models.creativities, {
      as: 'creativity',
      foreignKey: 'creativity_id'
    });
  };

  creativityCategories.connectCreativityCategories = function(creativityCategories, options = {}) {
    return sequelizeClient.models.creativity_categories.bulkCreate(creativityCategories, {
      transaction: options.transaction
    });
  };

  creativityCategories.deleteCreativityCategories = function (data, options = {}) {
    return sequelizeClient.models.creativity_categories.destroy({
      where: {
        creativityId: data.creativityId,
        categoryId: data.categoryIds,
      },
      transaction: options.transaction
    });
  };

  return creativityCategories;
};
