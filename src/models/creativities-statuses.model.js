const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativitiesStatuses = sequelizeClient.define('creativities_statuses', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    statusId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'status_id'
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'creativity_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true
  });

  creativitiesStatuses.associate = function (models) {
  };

  creativitiesStatuses.connectCreativityStatuses = function(creativityStatuses, options = {}) {
    return sequelizeClient.models.creativities_statuses.bulkCreate(creativityStatuses, {
      transaction: options.transaction
    });
  };

  creativitiesStatuses.deleteCreativityStatuses = function (data, options = {}) {
    return sequelizeClient.models.creativities_statuses.destroy({
      where: {
        creativityId: data.creativityId,
        statusId: data.statusIds,
      },
      transaction: options.transaction
    });
  };

  return creativitiesStatuses;
};