const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativitiesFormats = sequelizeClient.define('creativities_formats', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    formatId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'format_id'
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'creativity_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true
  });

  creativitiesFormats.associate = function (models) {
  };

  creativitiesFormats.connectCreativityFormats = function(creativityFormats, options = {}) {
    return sequelizeClient.models.creativities_formats.bulkCreate(creativityFormats, {
      transaction: options.transaction
    });
  };

  creativitiesFormats.deleteCreativityFormats = function (data, options = {}) {
    return sequelizeClient.models.creativities_formats.destroy({
      where: {
        creativityId: data.creativityId,
        formatId: data.formatIds,
      },
      transaction: options.transaction
    });
  };

  return creativitiesFormats;
};
