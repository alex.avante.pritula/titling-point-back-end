const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const categories = sequelizeClient.define('categories', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    category: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },

    parentCategoryId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      required: true,
      field: 'parent_category_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  categories.associate = function (models) {
    this.belongsTo(models.categories, {
      as: 'parentCategory',
      foreignKey: 'parent_category_id'
    });

    this.belongsToMany(models.creativities, {
      as: 'creativities',
      through: {model: models.creativity_categories},
      foreignKey: 'creativity_id'
    });

    this.hasMany(models.categories, {
      as: 'subCategories',
      foreignKey: 'parent_category_id'
    });
  };


  categories.getCategoriesWithSubCategories = function (options = {}) {
    return sequelizeClient.models.categories.findAll({
      where: {
        parentCategoryId: null
      },
      include: [{
        model: sequelizeClient.models.categories,
        'as': 'subCategories'
      }],
      transaction: options.transaction
    });
  };

  categories.getCategories = function (limit = 10, offset = 0, options = {}) {
    return sequelizeClient.models.categories.findAll({
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  categories.getExistingCategoryNames = function (categories, options = {}) {
    return sequelizeClient.models.categories.findAll({
      where: {
        category: categories
      },
      transaction: options.transaction
    });
  };

  categories.createCategories = function (categories, options = {}) {
    return sequelizeClient.models.categories.bulkCreate(categories, {
      transaction: options.transaction
    });
  };

  return categories;
};
