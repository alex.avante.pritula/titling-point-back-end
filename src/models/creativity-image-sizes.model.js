const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativityImageSizes = sequelizeClient.define('creativity_image_sizes', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'creativity_id'
    },

    sizeId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'size_id'
    },

    fileId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      required: false,
      field: 'file_id'
    },

    isMainImage: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      field: 'is_main_image'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  creativityImageSizes.associate = function (models) {
  };

  creativityImageSizes.createImageSize = function(creativityImageSize, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.create(creativityImageSize, {
      transaction: options.transaction
    });
  };

  creativityImageSizes.createImageSizes = function(creativityImageSizes, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.bulkCreate(creativityImageSizes, {
      transaction: options.transaction
    });
  };

  creativityImageSizes.getMainImageCreativityImageSizeByCreativityId = function (creativityId, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.findOne({
      where: {
        creativityId: creativityId,
        isMainImage: true
      },
      transaction: options.transaction
    });
  };


  creativityImageSizes.deleteCreativityImageSizeMainImage = function (creativityId, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.destroy({
      where: {
        creativityId: creativityId,
        isMainImage: true
      },
      transaction: options.transaction
    });
  };

  creativityImageSizes.getCreativityImageSizesNotMainImage = function (creativityId, sizeId, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.findOne({
      where: {
        creativityId: creativityId,
        sizeId: sizeId,
        isMainImage: false
      },
      transaction: options.transaction
    });
  };

  creativityImageSizes.getCreativityImageSizesByFileIds = function (fileIds, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.findAll({
      where: {
        fileId: fileIds
      },
      transaction: options.transaction
    });
  };

  creativityImageSizes.getImageCreativitySizes = function (creativityId, sizeIds, options = {}) {
    return sequelizeClient.models.creativity_image_sizes.findAll({
      where: {
        creativityId: creativityId,
        sizeId: sizeIds
      },
      transaction: options.transaction
    });
  };

  return creativityImageSizes;
};
