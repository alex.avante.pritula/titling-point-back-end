const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const roles = sequelizeClient.define('roles', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    role: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  roles.associate = function (models) {
    this.hasMany(models.users, {
      as: 'users',
      foreignKey: 'role_id'
    });
  };

  roles.getRole = function (roleId, options = {}) {
    return sequelizeClient.models.roles.findOne({
      where: {
        id: roleId
      },
      transaction: options.transaction
    });
  };

  return roles;
};
