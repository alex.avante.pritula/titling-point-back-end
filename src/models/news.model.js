const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const news = sequelizeClient.define('news', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    title: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    description: {
      type: Sequelize.STRING,
      allowNull: true
    },

    itemId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'item_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  news.associate = function (models) {
    this.belongsTo(models.items, {
      as: 'items',
      foreignKey: 'item_id'
    });

    this.hasMany(models.news_images, {
      as: 'newsImages',
      foreignKey: 'news_id'
    });
  };

  news.createNews = function (data, options = {}) {
    return sequelizeClient.models.news.create({
      itemId: data.itemId,
      title: data.title,
      description: data.newsDescription
    }, {
      transaction: options.transaction
    });
  };

  news.updateNews = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.news.update(toUpdate, {
      where: {
        id
      },
      transaction: options.transaction
    });
  };

  return news;
};
