const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativitiesSizes = sequelizeClient.define('creativities_sizes', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    sizeId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'size_id'
    },

    creativityId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'creativity_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }
  }, {
    underscored: true
  });

  creativitiesSizes.associate = function (models) {
  };

  creativitiesSizes.getCreativitySize = function (creativityId, sizeId, options = {}) {
    return sequelizeClient.models.creativities_sizes.findOne({
      where: {
        creativityId: creativityId,
        sizeId: sizeId
      },
      transaction: options.transaction
    });
  };

  creativitiesSizes.connectCreativitySizes = function(creativitySizes, options = {}) {
    return sequelizeClient.models.creativities_sizes.bulkCreate(creativitySizes, {
      transaction: options.transaction
    });
  };

  creativitiesSizes.deleteCreativitySizes = function (creativityId, sizeIds, options = {}) {
    return sequelizeClient.models.creativities_sizes.destroy({
      where: {
        creativityId: creativityId,
        sizeId: sizeIds
      },
      transaction: options.transaction
    });
  };

  return creativitiesSizes;
};
