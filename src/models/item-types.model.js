const Sequelize = require('sequelize');
module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const itemTypes = sequelizeClient.define('item_types', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    type: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  // eslint-disable-next-line no-unused-vars
  itemTypes.associate = function (models) {
  };

  return itemTypes;
};
