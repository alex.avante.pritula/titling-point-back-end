const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const gameGenres = sequelizeClient.define('game_genres', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    gameId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'game_id'
    },

    genreId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'genre_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  gameGenres.associate = function (models) {
    this.belongsTo(models.categories, {
      as: 'game',
      foreignKey: 'game_id'
    });

    this.belongsTo(models.creativities, {
      as: 'genre',
      foreignKey: 'genre_id'
    });
  };

  gameGenres.connectGameGenres = function (gameGenres, options = {}) {
    return sequelizeClient.models.game_genres.bulkCreate(gameGenres, {
      transaction: options.transaction
    });
  };

  gameGenres.deleteGameGenres = function (gameId, genreIds, options = {}) {
    return sequelizeClient.models.game_genres.destroy({
      where: {
        gameId: gameId,
        genreId: genreIds
      },
      transaction: options.transaction
    });
  };

  return gameGenres;
};
