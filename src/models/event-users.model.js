const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const eventUsers = sequelizeClient.define('event_users', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    eventId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'event_id'
    },

    item_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'item_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  eventUsers.associate = function (models) {
    this.belongsTo(models.users, {
      as: 'users',
      foreignKey: 'user_id'
    });

    this.belongsTo(models.users, {
      as: 'events',
      foreignKey: 'event_id'
    });
  };

  return eventUsers;
};
