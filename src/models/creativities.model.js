const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const creativities = sequelizeClient.define('creativities', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      required: true
    },

    mainImageUrl: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'main_image_url'
    },

    avalableSizes: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'avalable_sizes'
    },

    avalableLanguages: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'avalable_languages'
    },

    feedback: {
      type: Sequelize.STRING,
      allowNull: true
    },

    jiraTicketUrl: {
      type: Sequelize.STRING,
      allowNull: true,
      field: 'jira_ticket_url'
    },

    isUsed: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true,
      field: 'is_used'
    },

    gameId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'game_id'
    },

    ownerId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      field: 'owner_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  creativities.associate = function (models) {
    this.belongsTo(models.users, {
      as: 'user',
      foreignKey: 'owner_id'
    });
    this.belongsTo(models.games, {
      as: 'game',
      foreignKey: 'game_id'
    });
    this.hasMany(models.tabs, {
      as: 'tabs',
      foreignKey: 'creativity_id'
    });
    this.belongsToMany(models.platforms, {
      through: {
        model: models.creativity_platforms
      },
      as: 'platforms'
    });
    this.belongsToMany(models.formats, {
      through: {
        model: models.creativities_formats
      },
      as: 'formats'
    });
    this.belongsToMany(models.statuses, {
      through: {
        model: models.creativities_statuses
      },
      as: 'statuses'
    });
    this.belongsToMany(models.categories, {
      through: {
        model: models.creativity_categories
      },
      as: 'categories'
    });
    this.belongsToMany(models.sizes, {
      through: {
        model: models.creativities_sizes
      },
      as: 'sizes'
    });
  };

  creativities.createCreativityAsset = function(data, options = {}) {
    return sequelizeClient.models.creativities.create(data, {
      transaction: options.transaction
    });
  };

  creativities.getCreativityById = function(creativityId, options = {}) {
    return sequelizeClient.models.creativities.findOne({
      where: {
        id: creativityId
      },
      include: [{
        model: sequelizeClient.models.games,
        as: 'game'
      }, {
        model: sequelizeClient.models.tabs,
        as: 'tabs'
      }],
      transaction: options.transaction
    });
  };

  creativities.getCreativities = function(filters = {}, limit = 10, offset = 0, options = {}) {
    let whereClause = {};
    let whereCategoryClause = {};
    let wherePlatformClause = {};
    let whereStatusClause = {};
    let whereFormatClause = {};
    let whereSizeClause = {};
    let whereUserClause = {};

    if (typeof filters['gameId'] !== 'undefined') {
      whereClause.gameId = filters['gameId'];
    }

    if (typeof filters['categoryId'] !== 'undefined') {
      whereCategoryClause.categoryId = filters['categoryId'];
    }

    if (typeof filters['platformId'] !== 'undefined') {
      wherePlatformClause.platformId = filters['platformId'];
    }

    if (typeof filters['statusId'] !== 'undefined') {
      whereStatusClause.statusId = filters['statusId'];
    }

    if (typeof filters['formatId'] !== 'undefined') {
      whereFormatClause.formatId = filters['formatId'];
    }

    if (typeof filters['sizeId'] !== 'undefined') {
      whereSizeClause.sizeId = filters['sizeId'];
    }

    if (typeof filters['departmentId'] !== 'undefined') {
      whereUserClause.departmentId = filters['departmentId'];
    }

    if (typeof filters['createdAtFrom'] !== 'undefined') {
      whereClause.createdAt = {
        $gte: filters['createdAtFrom']
      };
    }

    if (typeof filters['createdAtTill'] !== 'undefined') {
      if (whereClause.createdAt) {
        whereClause.createdAt['$lte'] =  filters['createdAtTill'];
      } else {
        whereClause.createdAt = {
          $lte: filters['createdAtTill']
        };
      }
    }

    return sequelizeClient.models.creativities.findAndCountAll({
      distinct: 'creativities.id',
      where: whereClause,
      include: [{
        model: sequelizeClient.models.games,
        as: 'game'
      }, {
        model: sequelizeClient.models.users,
        as: 'user',
        where: whereUserClause
      }, {
        model: sequelizeClient.models.tabs,
        as: 'tabs'
      },
      {
        model: sequelizeClient.models.platforms,
        as: 'platforms',
        through: 'platform_id',
        where: wherePlatformClause,
        required: false
      }, {
        model: sequelizeClient.models.formats,
        as: 'formats',
        through: 'format_id',
        where: whereFormatClause,
        required: false
      }, {
        model: sequelizeClient.models.statuses,
        as: 'statuses',
        through: 'status_id',
        where: whereStatusClause,
        required: false
      }, {
        model: sequelizeClient.models.categories,
        as: 'categories',
        through: 'category_id',
        where: whereCategoryClause,
        required: false
      }, {
        model: sequelizeClient.models.sizes,
        as: 'sizes',
        through: 'size_id',
        where: whereSizeClause,
        required: false
      }],
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  creativities.updateCreativity = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.creativities.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  creativities.getById = function (id, options = {}, filters = {}) {
    let includeDocsClause;
    let includeClause = [];
    if (typeof filters['includeDocs'] !== 'undefined') {
      includeDocsClause = {
        model: sequelizeClient.models.tabs,
        as: 'tabs',
        include: [{
          model: sequelizeClient.models.folders,
          as: 'folders',
          include: [{
            model: sequelizeClient.models.docs,
            as: 'docs'
          }]
        }]
      };

      includeClause.push(includeDocsClause);
    }

    let gameIncludeClause = {
      model: sequelizeClient.models.games,
      as: 'game'
    };

    let platformIncludeClause = {
      model: sequelizeClient.models.platforms,
      as: 'platforms',
      through: 'platform_id'
    };

    let formatIncludeClause = {
      model: sequelizeClient.models.formats,
      as: 'formats',
      through: 'format_id'
    };

    let statusIncludeClause = {
      model: sequelizeClient.models.statuses,
      as: 'statuses',
      through: 'status_id'
    };

    let categoryIncludeClause = {
      model: sequelizeClient.models.categories,
      as: 'categories',
      through: 'category_id'
    };

    let sizeIncludeClause = {
      model: sequelizeClient.models.sizes,
      as: 'sizes',
      through: 'size_id'
    };

    includeClause.push(gameIncludeClause);
    includeClause.push(platformIncludeClause);
    includeClause.push(statusIncludeClause);
    includeClause.push(formatIncludeClause);
    includeClause.push(categoryIncludeClause);
    includeClause.push(sizeIncludeClause);

    return sequelizeClient.models.creativities.findOne({
      where: {
        id: id
      },
      include: includeClause,
      transaction: options.transaction
    });
  };

  creativities.deleteCreativity = function (id, options = {}) {
    return sequelizeClient.models.creativities.destroy({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  return creativities;
};
