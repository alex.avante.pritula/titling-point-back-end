const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const docs = sequelizeClient.define('docs', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true
    },

    documentUrl: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'document_url'
    },

    folderId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'folder_id'
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  docs.associate = function (models) {
    this.belongsTo(models.folders, {
      as: 'folder',
      foreignKey: 'folder_id'
    });
  };

  docs.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.docs.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        folderId: data.folderId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  docs.decrementToPosition = function (data, options = {}) {
    return sequelizeClient.models.docs.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        folderId: data.folderId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  docs.updateDoc = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.docs.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  docs.deleteDocs = function (ids, options = {}) {
    return sequelizeClient.models.docs.destroy({
      where: {
        id: ids
      },
      transaction: options.transaction
    });
  };

  docs.deleteDocsByIdsAndFolderId = function (ids, folderId, options = {}) {
    return sequelizeClient.models.docs.destroy({
      where: {
        id: ids,
        folderId: folderId
      },
      transaction: options.transaction
    });
  };

  /**
   * Function get max position of doc in folder.
   * @param folderId
   * @param options
   * @return {Promise.<Model>}
   */
  docs.getMaxPosition = function (folderId, options = {}) {
    return sequelizeClient.models.docs.max(
      'position', {
        where: {
          folderId
        },
        transaction: options.transaction
      });
  };

  /**
   * Function decrement position of docs in folder.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  docs.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.docs.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        folderId: data.folderId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function increment position of docs in folder.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  docs.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.docs.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        folderId: data.folderId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      }
    }, {
      transaction: options.transaction
    });
  };

  docs.createDoc = function (data, options = {}) {
    return sequelizeClient.models.docs.create({
      folderId: data.folderId,
      name: data.name,
      documentUrl: data.documentUrl,
      position: data.position,
    }, {
      transaction: options.transaction
    });
  };

  /**
   * Function will make a query for getting docs by id.
   * @param ids
   * @param options
   * @return {Promise.<Model>}
   */
  docs.getByIds = function (ids, options = {}) {
    return sequelizeClient.models.docs.findAll({
      where: {
        id: ids
      },
      transaction: options.transaction
    });
  };

  /**
   * Function will make a query for getting doc by id.
   * @param id
   * @param options
   * @return {Promise.<Model>}
   */
  docs.getById = function (id, options = {}) {
    return sequelizeClient.models.docs.findOne({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  return docs;
};
