const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const confirmCodes = sequelizeClient.define('confirm_codes', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    code: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true,
      field: 'user_id'
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  confirmCodes.associate = function (models) {
    this.belongsTo(models.users, {
      as: 'user',
      foreignKey: 'user_id'
    });
  };

  confirmCodes.createCode = function (data, options = {}) {
    return sequelizeClient.models.confirm_codes.create(data, {
      transaction: options.transaction
    });
  };

  confirmCodes.updateCode = function (userId, code, options = {}) {
    return sequelizeClient.models.confirm_codes.update({
      code: code
    }, {
      where: {
        userId: userId
      },
      transaction: options.transaction
    });
  };

  confirmCodes.invalidateCode = function (code, options = {}) {
    return sequelizeClient.models.confirm_codes.destroy({
      where: {
        code: code
      },
      transaction: options.transaction
    });
  };

  return confirmCodes;
};
