const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const formats = sequelizeClient.define('formats', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    format: {
      type: Sequelize.STRING,
      allowNull: false,
      require: true,
      unique: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  // eslint-disable-next-line no-unused-vars
  formats.associate = function (models) {

  };

  formats.getExistingFormatNames = function (formats, options = {}) {
    return sequelizeClient.models.formats.findAll({
      where: {
        format: formats
      },
      transaction: options.transaction
    });
  };

  formats.createFormats = function (formats, options = {}) {
    return sequelizeClient.models.formats.bulkCreate(formats, {
      transaction: options.transaction
    });
  };

  formats.getFormats = function (limit = 10, offset = 0, options = {}) {
    return sequelizeClient.models.formats.findAll({
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  return formats;
};
