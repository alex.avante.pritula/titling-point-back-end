const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const gameAssetDocs = sequelizeClient.define('game_asset_docs', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    folderId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      required: true,
      field: 'folder_id'
    },

    name: {
      type: Sequelize.STRING,
      allowNull: false,
      required: true
    },

    documentUrl: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'document_url'
    },

    position: {
      type: Sequelize.INTEGER,
      allowNull: false,
      require: true
    },

    createdAt: {
      type: Sequelize.DATE,
      field: 'created_at'
    },

    updatedAt: {
      type: Sequelize.DATE,
      field: 'updated_at'
    }

  }, {
    underscored: true
  });

  gameAssetDocs.associate = function (models) {
    this.belongsTo(models.game_asset_folders, {
      as: 'game_asset_folder',
      foreignKey: 'folder_id'
    });
  };

  gameAssetDocs.getDocs = function(filters = {}, limit = 10, offset = 0, options = {}) {
    let whereClause = {};
    if (typeof filters['folder_id'] !== 'undefined') {
      whereClause.gameId = filters['folder_id'];
    }

    return sequelizeClient.models.game_asset_docs.findAndCountAll({
      where: whereClause,
      limit: limit,
      offset: offset,
      transaction: options.transaction
    });
  };

  /**
   * Function decrement position of docs in folder.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetDocs.decrementPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_docs.update({
      position: sequelizeClient.literal('position -1')
    }, {
      where: {
        folderId: data.folderId,
        position: {
          $and: [
            {$gte: data.currentPosition},
            {lte: data.newPosition}
          ]
        }
      },
      transaction: options.transaction
    });
  };

  /**
   * Function increment position of docs in folder.
   * @param data
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetDocs.incrementPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_docs.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        folderId: data.folderId,
        position: {
          $and: [
            {$gte: data.newPosition},
            {lte: data.currentPosition}
          ]
        }
      }
    }, {
      transaction: options.transaction
    });
  };

  gameAssetDocs.incrementFromPosition = function (data, options = {}) {
    return sequelizeClient.models.game_asset_docs.update({
      position: sequelizeClient.literal('position +1')
    }, {
      where: {
        folderId: data.folderId,
        position: {$gte: data.position}
      },
      transaction: options.transaction
    });
  };

  gameAssetDocs.updateDoc = function (id, toUpdate, options = {}) {
    return sequelizeClient.models.game_asset_docs.update(toUpdate, {
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  gameAssetDocs.deleteDocs = function (ids, options = {}) {
    return sequelizeClient.models.game_asset_docs.destroy({
      where: {
        id: ids
      },
      transaction: options.transaction
    });
  };

  gameAssetDocs.createDoc = function (data, options = {}) {
    return sequelizeClient.models.game_asset_docs.create({
      folderId: data.folderId,
      name: data.name,
      documentUrl: data.documentUrl,
      position: data.position,
    }, {
      transaction: options.transaction
    });
  };

  /**
   * Function will make a query for getting doc by id.
   * @param id
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetDocs.getById = function (id, options = {}) {
    return sequelizeClient.models.game_asset_docs.findOne({
      where: {
        id: id
      },
      transaction: options.transaction
    });
  };

  gameAssetDocs.deleteDocsByIdsAndFolderId = function (ids, folderId, options = {}) {
    return sequelizeClient.models.game_asset_docs.destroy({
      where: {
        id: ids,
        folderId: folderId
      },
      transaction: options.transaction
    });
  };

  /**
   * Function will make a query for getting docs by id.
   * @param ids
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetDocs.getByIds = function (ids, options = {}) {
    return sequelizeClient.models.game_asset_docs.findAll({
      where: {
        id: ids
      },
      transaction: options.transaction
    });
  };

  /**
   * Function get max position of doc in folder.
   * @param folderId
   * @param options
   * @return {Promise.<Model>}
   */
  gameAssetDocs.getMaxPosition = function (folderId, options = {}) {
    return sequelizeClient.models.game_asset_docs.max(
      'position', {
        where: {
          folderId
        },
        transaction: options.transaction
      });
  };

  return gameAssetDocs;
};
