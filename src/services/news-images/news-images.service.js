const path = require('path');
const _ = require('lodash');
const createModel = require('../../models/news-images.model');
const createItemsModel = require('../../models/items.model');
const hooks = require('./news-images.hooks');
const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const itemsModel = createItemsModel(app);
  const s3Service = app.get('s3');
  const sequelize = app.get('sequelizeClient');
  const routePrefix = app.get('prefix');
  //const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'news-images')
  };

  class NewsImage {
    setup(app) {
      this.app = app;
    }

    remove(id) {
      let newsImageObj = null;
      return Model.findById(id)
        .then((newsImage) => {
          if (!newsImage) {
            return Promise.reject(new errors.BadRequest('Invalid news image Id', {errors: {id}}, 400));
          }
          newsImageObj = newsImage;
          return Model.deleteNewsImage(id);
        })
        .then(() => {
          const path = _.takeRight(newsImageObj.imageUrl.split('/'), 5).join('/');
          return s3Service.deleteDocument(path);
        });
    }

    create(data, params) {
      let { itemId, position } = data;
      const { files } = params;
      let newsId = null;
      let blockId = null;
      let promises = [];
      let newsImageUrl = '';
      let newsImageUrls = [];

      return sequelize.transaction((_transaction) => {
        return itemsModel.getItemById(itemId)
          .then((item) => {
            if (_.isEmpty(item['news'])) {
              return Promise.reject(new errors.BadRequest('Invalid item Id', {errors: {id: itemId}}, 400));
            }

            newsId = item['news'].id;
            blockId = item.blockId;
            let newsDirectory = `dashboard_${item.block.dashboardId}/block_${blockId}/items_${itemId}/news_${newsId}`;

            files.forEach((file) => {
              let fileName = _.takeRight(file.key.split('/'), 1).join('/');
              let host = _.takeWhile(file.location.split(file.key)).join('/');
              newsImageUrl = `${host}${newsDirectory}/${fileName}`;
              newsImageUrls.push(newsImageUrl);
              promises.push(s3Service.renameDocument(file.key, `${newsDirectory}/${fileName}`));
            });

            return Promise.all(promises);
          }).then(()=>{
            let images = newsImageUrls.map((url, index) => {
              if (typeof position === 'undefined') {
                position = index + 1;
              }
              return {imageUrl: url, newsId, position};
            });
            return Model.createImages(images, {
              transaction: _transaction
            });
          });
      });
    }
  }

  app.use('/' + routes['basic'], new NewsImage());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
};
