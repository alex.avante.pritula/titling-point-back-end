// Initializes the `folders` service on path `/folders`
const createService = require('feathers-sequelize');
const createModel = require('../../models/folders.model');
const hooks = require('./folders.hooks');
const filters = require('./folders.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'folders',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/folders', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('folders');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
