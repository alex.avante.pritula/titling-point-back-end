const validators = require('../../../utils/validators');
const { authenticate } = require('feathers-authentication').hooks;
const { getErrorObject } = require('../../../utils/errorHandler');

function createValidators(hook) {
  let validateErrors = null;

  if (hook.path === 'api/v1/games') {
    validateErrors = validators.games.create(hook.data);
  }

  if (hook.path === 'api/v1/games/restrict') {
    validateErrors = validators.games.restrict(hook.data);
  }

  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }

  return hook;
}

function findValidators(hook) {
  let validateErrors = null;
  if (hook.path === 'api/v1/games/similar') {
    validateErrors = validators.games.getSimilarGames(hook.params.query);
  }

  if (hook.path === 'api/v1/games') {
    validateErrors = validators.games.getAllGames(hook.params.query);
  }

  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }

  return hook;
}

function patchValidators(hook) {
  let validateErrors = null;
  if (hook.path === 'api/v1/games/:id') {
    validateErrors = validators.games.update(hook.data);
  }

  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }

  return hook;
}

function formateResponse(hook) {
  return hook;
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [findValidators],
    get: [],
    create: [createValidators],
    update: [],
    patch: [patchValidators],
    remove: []
  },

  after: {
    all: [formateResponse],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [function (hook) {
      hook.error = getErrorObject(hook.error);
    }],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
