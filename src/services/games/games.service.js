const path = require('path');
const _ = require('lodash');
const createModel = require('../../models/games.model');
const createGenresModel = require('../../models/genres.model');
const createGameGenresModel = require('../../models/game-genres.model');
const createSimilarGamesModel = require('../../models/similar-games.model');
const hooks = require('./games.hooks');
const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const similarGamesModel = createSimilarGamesModel(app);
  const gameGenresModel = createGameGenresModel(app);
  const genresModel = createGenresModel(app);
  const sequelize = app.get('sequelizeClient');
  const routePrefix = app.get('prefix');
  const s3Service = app.get('s3');
  const constants = app.get('constants');
  const aclService = app.get('aclService');
  const routes = {
    basic: path.join(routePrefix, 'games'),
    images: path.join(routePrefix, 'games/images'),
    genres_games: path.join(routePrefix, 'games/genres'),
    remove_genres_games: path.join(routePrefix, 'games/:gameId/genres'),
    similar_games: path.join(routePrefix, 'games/:gameId/similar'),
    restrict_games: path.join(routePrefix, 'games/restrict'),
  };

  class Game {
    setup(app) {
      this.app = app;
    }

    get(id, params) {
      const {payload} = params;
      const {userId} = payload;

      return aclService
        .getUserRestrictGameIds(userId)
        .then((gameIds) => {
          if (gameIds.indexOf(parseInt(id)) == -1) {
            return Model.getById(id);
          }
          return {};
        }).then((game) => {
          return game;
        });
    }

    patch(id, body) {
      let {name, description, publishDate} = body;

      if (publishDate) {
        publishDate = new Date(parseInt(publishDate));
      }

      const toUpdate = {name, description, publishDate};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return Model.updateGame(id, toUpdate)
        .then(() => {
          return {};
        });
    }

    find(params) {
      const {query, payload} = params;
      const {userId} = payload;
      let {limit, page, offset, direction, title, description, publishDate, genre} = query;
      const {SEARCH} = constants;
      direction = direction || 'DESC';
      let gamesObj = [];
      if (publishDate) {
        publishDate = new Date(parseInt(publishDate));
      }

      let filters = {
        title,
        description,
        publishDate,
        genre
      };

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return aclService
        .getUserRestrictGameIds(userId)
        .then((gameIds) => {
          filters['restrict_ids'] = gameIds;
          return Model.getAll(filters, _limit, _offset, direction);
        }).then(games => {
          gamesObj = games;
          return Model.getCountGames(filters);
        }).then(count => {
          gamesObj = gamesObj.map(gameObj => gameObj.toJSON());

          return {
            count,
            games: gamesObj
          };
        });
    }

    create(data, params) {
      const { payload } = params;
      const { userId } = payload;
      let { name, publishDate, description, genres, similarGames } = data;
      if (genres.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of genres must be at least 1', {errors: {}}, 400));
      }

      if (!publishDate) {
        publishDate = new Date();
      } else {
        publishDate = new Date(parseInt(publishDate));
      }

      let notExistingGenres = [];
      let notExistingGames = [];
      let gameId = null;
      genres = genres.map(genre => genre.toLowerCase());
      return sequelize.transaction((transaction) => {
        const options = {transaction};

        return genresModel.getExitstingGenreNames(genres, options)
          .then((existingGenres) => {
            let existingGenresMap = existingGenres.map(existingGenre => existingGenre.genre);
            notExistingGenres = _.difference(genres, existingGenresMap);
            let notExistingGenresMap = notExistingGenres.map(notExistingGenre => ({genre: notExistingGenre}));
            return genresModel.createGenres(notExistingGenresMap, options);
          })
          .then(() => {
            return Model.createGame({
              ownerId: userId,
              name,
              description,
              publishDate
            }, options);
          })
          .then(game => {
            return gameId = game.id;
          })
          .then(() => {
            return genresModel.getExitstingGenreNames(genres, options);
          })
          .then((requestedGenres) => {
            const gameGenres = requestedGenres.map(requestedGenre => ({
              genreId: requestedGenre.id,
              gameId: gameId
            }));

            return gameGenresModel.connectGameGenres(gameGenres, options);
          })
          .then(() => {
            return Model.getExistingGames(similarGames, options);
          })
          .then((existingGames) => {
            const existingGamesMap = existingGames.map(existingGame => existingGame.id);
            notExistingGames = _.difference(similarGames, existingGamesMap);
            if(notExistingGames.length > 0) {
              return Promise.reject(new errors.BadRequest(`Invalid similar game id: ${notExistingGames.join(', ')}`, {errors: {}}, 400));
            }
          })
          .then(() => {
            const gameSimilarGames = similarGames.map(similarGameId => ({
              similarGameId: similarGameId,
              gameId: gameId
            }));

            return similarGamesModel.connectGameSimilarGames(gameSimilarGames, options);
          })
          .then(() => {
            return Model.getGameWithGenresAndSimilarGames(gameId, options);
          });
      });
    }

    remove(id) {
      let docsMap = [];
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        let filters = {
          includeDocs: true
        };
        return Model.getById(id, options, filters)
          .then((game) => {

            game.creativities.map(creativity => {
              creativity.tabs.map(tab => {
                tab.folders.map(folder => {
                  return folder.docs.map(doc => {
                    docsMap.push(_.takeRight(doc.documentUrl.split('/'), 5).join('/'));
                  });
                });
              });
            });

            docsMap.push(_.takeRight(game.imageUrl.split('/'), 2).join('/'));
            return Model.deleteGame(id, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class GameImage {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {gameId} = data;
      const {files} = params;
      let gamesDirectory = `game_${gameId}`;
      let imageUrl = '';

      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {}}, 400));
      }

      const file = files[0];
      let fileName = _.takeRight(file.key.split('/'), 1).join('/');
      let host = _.takeWhile(file.location.split(file.key)).join('/');
      imageUrl = `${host}${gamesDirectory}/${fileName}`;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return s3Service.renameDocument(file.key, `${gamesDirectory}/${fileName}`)
          .then(() => {
            return Model.updateGame(gameId, {imageUrl: imageUrl}, options);
          }).then(() => {
            return {
              imageUrl: imageUrl
            };
          });
      });
    }
  }

  class GenreGame {
    setup(app) {
      this.app = app;
    }

    create(body) {
      let { genres, similarGames, gameId } = body;
      if (genres.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of genres must be at least 1', {errors: {}}, 400));
      }

      let notExistingGenres = [];
      genres = genres.map(genre => genre.toLowerCase());
      return genresModel.getExitstingGenreNames(genres)
        .then((existingGenres) => {
          let existingGenresMap = [];
          existingGenresMap = existingGenres.map(existingGenre => existingGenre.genre);
          notExistingGenres = _.difference(genres, existingGenresMap);
          let notExistingGenresMap = [];
          notExistingGenresMap = notExistingGenres.map(notExistingGenre => ({genre: notExistingGenre}));
          return genresModel.createGenres(notExistingGenresMap);
        }).then(() => {
          return genresModel.getExitstingGenreNames(genres);
        }).then((requestedGenres) => {
          return sequelize.transaction((_transaction) => {
            let gameGenres = [];
            gameGenres = requestedGenres.map(requestedGenre => ({
              genreId: requestedGenre.id,
              gameId: gameId
            }));

            return gameGenresModel.connectGameGenres(gameGenres, {transaction: _transaction})
              .then(() => {
                let gameSimilarGames = [];
                gameSimilarGames = similarGames.map(similarGameId => ({
                  similarGameId: similarGameId,
                  gameId: gameId
                }));

                return similarGamesModel.connectGameSimilarGames(gameSimilarGames, {transaction: _transaction});
              });
          });
        });
    }

    find(params) {
      const {query, payload} = params;
      const {userId} = payload;
      let {limit, page, offset, genres} = query;
      const {SEARCH} = constants;
      if (!genres || genres.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of genres must be at least 1', {errors: {}}, 400));
      }

      genres = genres.map(genre => genre.toLowerCase());

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      let filters = {};

      return aclService
        .getUserRestrictGameIds(userId)
        .then((gameIds) => {
          filters['restrict_ids'] = gameIds;
          return Model.getByGenre(genres, filters, _limit, _offset);
        }).then(games => {
          games = games.map(game => game.toJSON());
          return games;
        });
    }
  }

  class RemoveGenreGame {
    setup(app) {
      this.app = app;
    }

    remove(id, params) {
      const { gameId, query } = params;
      const { genres } = query;

      if (!gameId) {
        return Promise.reject(new errors.BadRequest('Game id and similar id are required', {errors: {}}, 400));
      }

      if (!genres || genres.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of similar game ids should be at least 1', {errors: {}}, 400));
      }
      return sequelize.transaction((_transaction) => {
        return Model.getByGenre(genres)
          .then(games => {
            games = games.map(game => game.id);
            return similarGamesModel.deleteSimilarGames(gameId, games, {transaction: _transaction});
          }).then(() => {
            return genresModel.getAllByName(genres);
          }).then((genres) => {
            let genreIds = [];
            genreIds = genres.map(genre => genre.id);
            return gameGenresModel.deleteGameGenres(gameId, genreIds, {transaction: _transaction});
          }).then(() => {
            return {};
          });
      });
    }
  }

  class SimilarGame {
    setup(app) {
      this.app = app;
    }

    patch(id, body, params) {
      const { gameId } = params;
      const { ids } = body;
      let data = [];

      if (!gameId) {
        return Promise.reject(new errors.BadRequest('Game id and similar id are required', {errors: {}}, 400));
      }

      if (!ids || ids.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of similar game ids should be at least 1', {errors: {}}, 400));
      }

      data = ids.map(id => ({gameId, similarGameId: id}));

      return similarGamesModel.addSimilarGames(data)
        .then(() => {
          return {};
        });
    }

    remove(id, params) {
      const { gameId, query } = params;
      const { ids } = query;

      if (!gameId) {
        return Promise.reject(new errors.BadRequest('Game id and similar id are required', {errors: {}}, 400));
      }

      if (!ids || ids.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of similar game ids should be at least 1', {errors: {}}, 400));
      }

      return similarGamesModel.deleteSimilarGames(gameId, ids)
        .then(() => {
          return {};
        });
    }
  }

  class RestrictGame {
    setup(app) {
      this.app = app;
    }

    create(body) {
      const { ids, userId } = body;
      const aclInstance = this.app.get('aclInstance');

      let resources = ids.map(id => {
        return `games_${id}`;
      });

      return aclInstance.allow([
        {
          roles:[`user_${userId}`],
          allows:[
            {resources: resources, permissions:['games_view']},
          ]
        }
      ]).then(() => {
        return {};
      });
    }

    find(params) {
      const { query } = params;
      const { userId } = query;

      if (!userId) {
        return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));
      }

      const aclService = this.app.get('aclService');
      return aclService
        .getUserRestrictGameIds(userId).then((gameIds) => {
          return Model.getByIds(gameIds);
        }).then((games) => {
          return games;
        });
    }

    /*
    * add logic removing from permissions
    * */
    remove(id, params) {
      const {query} = params;
      const {userId, ids} = query;

      if (!userId) {
        return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));
      }

      if (!userId) {
        return Promise.reject(new errors.BadRequest('User id is required', {errors: {}}, 400));
      }

      const aclService = this.app.get('aclService');
      return aclService.removeUserRestrictGames(userId, ids)
        .then(() => {
          return {};
        });
    }
  }

  app.use('/' + routes['images'], new GameImage());
  const gameImageService = app.service(routes['images']);

  app.use('/' + routes['genres_games'], new GenreGame());
  const genreGameService = app.service(routes['genres_games']);

  app.use('/' + routes['remove_genres_games'], new RemoveGenreGame());
  const removeGenreGameService = app.service(routes['remove_genres_games']);

  app.use('/' + routes['similar_games'], new SimilarGame());
  const similarGameService = app.service(routes['similar_games']);

  app.use('/' + routes['restrict_games'], new RestrictGame());
  const restrictGameService = app.service(routes['restrict_games']);

  app.use('/' + routes['basic'], new Game());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
  gameImageService.hooks(hooks);
  genreGameService.hooks(hooks);
  removeGenreGameService.hooks(hooks);
  similarGameService.hooks(hooks);
  restrictGameService.hooks(hooks);
};

