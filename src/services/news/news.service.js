const path = require('path');
const createModel = require('../../models/news.model');
const createItemsModel = require('../../models/items.model');
const hooks = require('./news.hooks');
const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const itemsModel = createItemsModel(app);
  const sequelize = app.get('sequelizeClient');
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'news')
  };

  class News {
    setup(app) {
      this.app = app;
    }

    get(id) {
      return Model.getById(id);
    }

    patch(id, data) {
      const {title, description} = data;
      const toUpdate = {title, description};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return Model.updateNews(id, toUpdate);
    }

    create(data, params) {
      let {name, blockId, itemTypeId, newsDescription, title} = data;
      const {payload} = params;
      const {userId} = payload;
      let itemId = null;
      const position = 1;

      if (constants.ITEM_TYPES.NEWS_BLOCK !== itemTypeId && constants.ITEM_TYPES.TEXTAREA !== itemTypeId) {
        return Promise.reject(new errors.BadRequest('Invalid item type', {errors: {}}, 400));
      }

      return sequelize.transaction((_transaction) => {
        return itemsModel.incrementFromPosition({
          blockId,
          position
        }, {
          transaction: _transaction
        }).then(() => {
          return itemsModel.createItem({
            name,
            blockId,
            itemTypeId,
            position,
            userId
          }, {
            transaction: _transaction
          });
        }).then((item) => {
          itemId = item.id;
          return Model.createNews({
            itemId,
            title,
            newsDescription
          }, {
            transaction: _transaction
          });
        }).then((news) => {
          return {
            itemId: itemId,
            newsId: news.id
          };
        }).catch((err) => {
          _transaction.rollback(err);
          return Promise.reject(err);
        });
      });
    }
  }

  app.use('/' + routes['basic'], new News());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
};
