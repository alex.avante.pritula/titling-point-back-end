// Initializes the `departments` service on path `/departments`
const createService = require('feathers-sequelize');
const createModel = require('../../models/departments.model');
const hooks = require('./departments.hooks');
const filters = require('./departments.filters');
const path = require('path');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');
  const routePrefix = app.get('prefix');
  const routes = {
    basic: path.join(routePrefix, 'departments')
  };

  const options = {
    name: 'departments',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/' + routes['basic'], createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service(routes['basic']);

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
