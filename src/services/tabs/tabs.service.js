// Initializes the `tabs` service on path `/tabs`
const createService = require('feathers-sequelize');
const createModel = require('../../models/tabs.model');
const hooks = require('./tabs.hooks');
const filters = require('./tabs.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'tabs',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/tabs', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('tabs');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
