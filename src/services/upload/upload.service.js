// Initializes the `upload` service on path `/upload`
//const createService = require('./upload.class.js');
const hooks = require('./upload.hooks');
const filters = require('./upload.filters');

module.exports = function () {
  const app = this;
  const s3 = app.get('s3');
  // Initialize our service with any options it requires
  app.use('/upload', {
    create(data, params){
      const { files } = params;
      return s3.uploadDocuments(files);
    }
  });

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('upload');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
