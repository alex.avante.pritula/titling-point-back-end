const path = require('path');
//const _ = require('lodash');
const createModel = require('../../models/items.model');
const hooks = require('./items.hooks');
const errors = require('feathers-errors');


module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const s3Service = app.get('s3');
  const routePrefix = app.get('prefix');
  const sequelize = app.get('sequelizeClient');

  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'items'),
    position: path.join(routePrefix, 'items/position'),
  };

  class Item {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const {query} = params;
      const {limit, page, offset, text} = query;
      const {SEARCH} = constants;
      let filters = {
        text
      };
      let itemsObj = [];

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      return Model.findByFilters(filters, _limit, _offset)
        .then(items => {
          itemsObj = items;
          return Model.countByFilters(filters);
        }).then(count => {
          itemsObj = itemsObj.map(itemObj => itemObj.toJSON());

          return {
            count,
            items: itemsObj
          };
        });
    }

    get(id) {
      return Model.getItemById(id);
    }

    /*todo
     * add to transaction LOCK (LOCK TABLE items IN ROW EXCLUSIVE MODE;)
     * */
    remove(id) {
      let position = null;
      let blockId = null;
      let itemId = null;
      let dashboardId = null;
      return sequelize.transaction((_transaction) => {
        return Model.getItemById(id)
          .then((item) => {
            if (!item) {
              return Promise.reject(new errors.BadRequest('Invalid item Id', {errors: {id: id}}, 400));
            }

            position = item.position;
            itemId = item.id;
            blockId = item.blockId;
            dashboardId = item.block.dashboardId;
            return Model.deleteItem(id);
          }).then(() => {
            return Model.moveOnCurrentPosition({blockId, position}, {transaction: _transaction});
          }).then(() => {
            const itemPrefix = `dashboard_${dashboardId}/block_${blockId}/items_${itemId}/`;
            return s3Service.deleteDocuments(itemPrefix);
          });
      });
    }

    patch(id, body) {
      const {name} = body;
      const toUpdate = {name};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return Model.updateItem(id, toUpdate)
        .then(() => {
          return {};
        });
    }
  }

  class ItemPosition {
    setup(app) {
      this.app = app;
    }

    /*todo
     * add to transaction LOCK (LOCK TABLE items IN ROW EXCLUSIVE MODE;)
     * */
    patch(id, body) {
      let item;
      let {newPosition} = body;
      let blockId;
      let currentPosition;

      return sequelize.transaction((_transaction) => {
        return Model.findById(id)
          .then(itemInstance => {
            if (!itemInstance) {
              return Promise.reject(new errors.BadRequest('item not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'item not found'
                }
              }, 400));
            }

            item = itemInstance;
            return Model.getMaxPosition(item.blockId);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject(new errors.BadRequest(`max position ${maxPosition}`, {
                errors: {
                  field: 'position',
                  code: 400,
                  message: `max position ${maxPosition}`
                }
              }, 400));
            }
            blockId = item.blockId;
            currentPosition = item.position;

            if (newPosition > currentPosition) {
              return Model.decrementPosition({
                blockId,
                newPosition,
                currentPosition
              }, {
                transaction: _transaction
              });
            }
            if (newPosition < currentPosition) {
              return Model.incrementPosition({
                blockId,
                newPosition,
                currentPosition
              }, {
                transaction: _transaction
              });
            }
          })
          .then(() => {
            return Model.updateItem(id, {position: newPosition}, {transaction: _transaction});
          })
          .then(() => {
            return {message: 'ok'};
          })
          .catch(err => {
            return Promise.reject(err);
          });
      });
    }
  }

  app.use('/' + routes['position'], new ItemPosition());
  const itemPositionService = app.service(routes['position']);

  app.use('/' + routes['basic'], new Item());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
  itemPositionService.hooks(hooks);
};
