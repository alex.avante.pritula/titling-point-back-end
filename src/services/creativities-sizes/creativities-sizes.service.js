// Initializes the `creativities-sizes` service on path `/creativities-sizes`
const createService = require('feathers-sequelize');
const createModel = require('../../models/creativities-sizes.model');
const hooks = require('./creativities-sizes.hooks');
const filters = require('./creativities-sizes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'creativities-sizes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/creativities-sizes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('creativities-sizes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
