const createModel = require('../../models/game-asset-docs.model');
const createAssetFolderModel = require('../../models/game-asset-folders.model');
const hooks = require('./game-asset-docs.hooks');
const path = require('path');
const errors = require('feathers-errors');
const _ = require('lodash');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const assetFolderModel = createAssetFolderModel(app);
  const sequelize = app.get('sequelizeClient');
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const s3Service = app.get('s3');

  const routes = {
    basic: path.join(routePrefix, 'game-asset/docs'),
    game_asset_docs_position: path.join(routePrefix, 'game-asset/docs/position')
  };

  class GameAssetDocs {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {assetFolderId, name} = data;
      const {files} = params;
      let position = 1;
      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {}}, 400));
      }

      const file = files[0];
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return assetFolderModel.getById(assetFolderId, null, null, options)
          .then((folder) => {
            if (!folder) {
              return Promise.reject(new errors.BadRequest('Invalid folder Id', {errors: {id: assetFolderId}}, 400));
            }

            return Model.incrementFromPosition({
              folderId: assetFolderId,
              position
            }, options);
          }).then(() => {
            return Model.createDoc({
              folderId: assetFolderId,
              name,
              position,
              documentUrl: file.location
            }, options);
          }).then(() => {
            return {
              documentUrl: file.location
            };
          });
      });
    }

    patch(id, body) {
      const {name} = body;
      const toUpdate = {name};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return Model.updateDoc(id, toUpdate);
    }

    find(params) {
      const { folderId, query } = params;
      const {SEARCH} = constants;
      let {limit, page, offset} = query;
      let filters = {
        folderId
      };

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getDocs(filters, _limit, _offset)
        .then((docs) => {
          return docs;
        });
    }

    remove(id, params) {
      const {query} = params;
      const {ids, assetFolderId} = query;
      let docsMap = [];

      if (ids.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of docs should be at least 1', {errors: {}}, 400));
      }

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model
          .getByIds(ids)
          .then((docs) => {
            docsMap = docs.map(doc => {
              return _.takeRight(doc.documentUrl.split('/'), 2).join('/');
            });
            return Model.deleteDocsByIdsAndFolderId(ids, assetFolderId, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class GameAssetDocPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let doc;
      let {newPosition} = body;
      let folderId;
      let currentPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model.getById(id, options)
          .then(docInstance => {
            if (!docInstance) {
              return Promise.reject(new errors.BadRequest('asset doc not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'asset doc not found'
                }
              }, 400));
            }
            doc = docInstance;
            return Model.getMaxPosition(doc.folderId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            folderId = doc.folderId;
            currentPosition = doc.position;
            if (newPosition > currentPosition) {
              return Model.decrementPosition(
                {
                  folderId,
                  newPosition,
                  currentPosition
                }, options);
            }
            if (newPosition < currentPosition) {
              return Model.incrementPosition({
                folderId,
                newPosition,
                currentPosition
              }, options);
            }
          })
          .then(() => {
            return Model.updateDoc(id, {position: newPosition}, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  app.use('/' + routes['basic'], new GameAssetDocs());
  const service = app.service(routes['basic']);

  app.use('/' + routes['game_asset_docs_position'], new GameAssetDocPosition());
  const gameAssetDocPositionService = app.service(routes['game_asset_docs_position']);

  service.hooks(hooks);
  gameAssetDocPositionService.hooks(hooks);
};
