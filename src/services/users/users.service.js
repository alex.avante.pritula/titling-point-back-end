const createService = require('feathers-sequelize');
const path = require('path');
const createModel = require('../../models/users.model');
const createConfirmCodesModel = require('../../models/confirm-codes.model');
const hooks = require('./users.hooks');
const filters = require('./users.filters');
const errors = require('feathers-errors');
const _ = require('lodash');
const uuid = require('uuid');

const isNull = require('lodash/isNull');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const confirmCodesModelModel = createConfirmCodesModel(app);
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const paginate = app.get('paginate');
  const s3Service = app.get('s3');
  const sequelize = app.get('sequelizeClient');
  const mailerConfig = app.get('mailer');

  const options = {
    name: 'users',
    Model,
    paginate
  };

  const routes = {
    basic: path.join(routePrefix, 'users'),
    avatar: path.join(routePrefix, 'users/avatar'),
    current: path.join(routePrefix, 'users/current'),
    code: path.join(routePrefix, 'users/code/:code'),
    resend: path.join(routePrefix, 'users/:userId/resend'),
    password_reset: path.join(routePrefix, 'users/password/reset'),
    profile_password_reset: path.join(routePrefix, 'users/profile/password'),
  };

  class User {
    setup(app) {
      this.app = app;
    }

    get(id, params) {
      const constants = this.app.get('constants');
      const {USER_ROLES} = constants;
      const {payload} = params;
      const {userId} = payload;
      let filters = {};
      return Model.getUserById(userId)
        .then((user) => {
          if (USER_ROLES.ADMIN == user.role.id) {
            filters['admin_allow_role_view'] = [USER_ROLES.USER];
          }

          return Model.getUserById(id, filters);
        })
        .then((user) => {
          return user;
        });
    }

    find(params) {
      const constants = this.app.get('constants');
      const {USER_ROLES} = constants;
      const {query, payload} = params;
      const {userId} = payload;
      let {limit, page, offset, departmentId, createdAt, isVerified, roleId, sortRoleId, sortCreatedAt, sortIsVerified} = query;
      const {SEARCH} = constants;

      if (createdAt) {
        createdAt = new Date(parseInt(createdAt));
      }

      let filters = {
        departmentId,
        userId,
        createdAt,
        isVerified,
        roleId,
        sortRoleId,
        sortCreatedAt,
        sortIsVerified
      };

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getUserById(userId)
        .then((user) => {
          if (USER_ROLES.ADMIN == user.role.id) {
            filters['admin_allow_role_view'] = [USER_ROLES.USER];
          }

          return Model.getAndCountAll(filters, _limit, _offset);
        }).then((users) => {
          let count = users.count;
          users = users.rows.map(user => user.toJSON());

          return {
            count,
            users
          };
        });
    }

    create(data) {
      let {departmentId, roleId, email} = data;
      const {USER_ROLES} = constants;
      const roleType = _.invert(USER_ROLES)[roleId];
      let userId = null;
      const code = uuid.v1();

      return sequelize.transaction((_transaction) => {
        return Model.createUser({
          departmentId,
          roleId,
          email
        }, {
          transaction: _transaction
        }).then((user) => {
          userId = user.id;
          return confirmCodesModelModel.createCode({
            userId,
            code
          }, {
            transaction: _transaction
          });
        }).then(() => {
          let aclInstance = app.get('aclInstance');
          return aclInstance.addUserRoles(`user_${userId}`, [roleType, `user_${userId}`]);
        }).then(() => {
          //send mail
          const userHelperService = app.get('userHelperService');
          let link = userHelperService.getInviteLink(code);
          let emailData = {
            from: mailerConfig.from,
            to: email,
            subject: 'Your user was created on tilting point',
            html: `your link for registration: ${link}`
          };
          return app.service('mailer').create(emailData);
        }).then(() => {
          return {};
        }).catch(err => Promise.reject(err));
      });
    }

    /*it is same with CurrentUser*/
    patch(id, body) {
      const {name, phone, job, roleId, biography, contactInfo, departmentId} = body;
      const toUpdate = {name, phone, job, roleId, biography, contactInfo, departmentId};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return Model.updateUser(id, toUpdate)
        .then(() => {
          return {};
        });
    }

    remove(id) {
      let aclInstance = app.get('aclInstance');
      const {USER_ROLES} = constants;
      let roleType = null;
      return Model.getUserById(id)
        .then((user) => {
          roleType = _.invert(USER_ROLES)[user.role.id];
          return Model.deleteUser(id);
        }).then(() => {
          return aclInstance.removeUserRoles(`user_${id}`, [roleType, `user_${id}`]);
        }).then(() => {
          return {};
        });
    }
  }

  class CurrentUser {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const {userId} = params.payload;

      return Model.getUserById(userId)
        .then((user) => {
          return user;
        });
    }

    patch(id, body, params) {
      const {userId} = params.payload;
      const {name, phone, job, password, biography, contactInfo, departmentId} = body;
      const toUpdate = {name, phone, job, password, biography, contactInfo, departmentId};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return Model.updateUser(userId, toUpdate)
        .then(() => {
          return {};
        });
    }
  }

  class UserCode {
    setup(app) {
      this.app = app;
      this.sequelizeClient = app.get('sequelizeClient');
    }

    find(params) {
      const {code} = params;
      return Model.getUserByCode(code)
        .then((user) => {
          if (_.isEmpty(user)) {
            return Promise.reject(new errors.BadRequest('Invalid code', {errors: {code: code}}, 400));
          }

          return user;
        });
    }

    patch(id, body, params) {
      const {code} = params;
      const {name, job, phone, password, biography, contactInfo, departmentId} = body;
      const isVerified = true;
      const toUpdate = {name, job, password, phone, biography, contactInfo, departmentId, isVerified};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return this.sequelizeClient.transaction((transaction) => {
        const options = {transaction};

        return Model.getUserByCode(code)
          .then((user) => {
            if (_.isEmpty(user)) {
              return Promise.reject(new errors.BadRequest('Invalid code', {errors: {code: code}}, 400));
            }
            return Model.updateUser(user.id, toUpdate, options);
          })
          .tap(() => confirmCodesModelModel.invalidateCode(code, options))
          .spread((affectedCount, [user]) => {
            const plainUserData = user.get({plain: true});
            return {
              id: plainUserData.id,
              email: plainUserData.email,
              name: plainUserData.name,
              job: plainUserData.job,
              biography: plainUserData.biography,
              departmentId: plainUserData.department_id,
              phone: plainUserData.phone,
              contactInfo: plainUserData.contactInfo
            };
          });
      });
    }
  }

  class ResendUser {
    setup(app) {
      this.app = app;
    }

    patch(id, data, params) {
      let { userId } = params;
      const code = uuid.v1();

      return sequelize.transaction((_transaction) => {
        return confirmCodesModelModel.updateCode(userId, code, {
          transaction: _transaction
        }).then(() => {
          //send mail
          return {};
        }).catch(err => Promise.reject(err));
      });
    }
  }

  class UserPasswordReset {
    setup(app) {
      this.app = app;
    }

    create(data) {
      const resetToken = uuid.v1();
      let { email } = data;

      return sequelize.transaction((_transaction) => {
        return Model.getUserByEmail(email, {
          transaction: _transaction
        }).then((user) => {
          if (isNull(user)) {
            return Promise.reject(new errors.BadRequest('User not found', {errors: {}}, 400));
          }

          return Model.updateResetToken(
            user.id,
            resetToken,
            {transaction: _transaction});
        }).then(() => {
          const userHelperService = app.get('userHelperService');
          let link = userHelperService.getRecoveryLink(resetToken);

          let emailData = {
            from: mailerConfig.from,
            to: email,
            subject: 'Your password was reset',
            html: `your link for recover password: ${link}`
          };
          return app.service('mailer').create(emailData);
        }).then(() => {
          return {};
        });
      });
    }

    patch(id, data) {
      let { password, resetToken } = data;
      let userObj = null;

      return sequelize.transaction((_transaction) => {
        return Model.getUserByResetToken(resetToken, {transaction: _transaction})
          .then((user) => {
            if (!user) {
              return Promise.reject(new errors.BadRequest('Reset token is not correct', {errors: {}}, 400));
            }

            userObj = user;
            return Model.updatePasswordByResetToken(
              resetToken,
              password,
              {transaction: _transaction});
          }).then(() => {
            let emailData = {
              from: mailerConfig.from,
              to: userObj.email,
              subject: 'Your password was changed on tilting point',
              html: 'Your password was changed on tilting point'
            };
            return app.service('mailer').create(emailData);
          }).then(() => {
            return Model.updateUser(userObj.id, {resetToken: ''}, {transaction: _transaction});
          }).then(() => {
            return this.app.passport.createJWT({
              userId: userObj.id,
              email: userObj.email,
              name: userObj.name,
            }, this.app.get('authentication'));
          }).then(accessToken => {
            return {accessToken: accessToken};
          });
      });
    }
  }

  class UserProfilePassword {
    setup(app) {
      this.app = app;
    }

    patch(id, body, params) {
      const { userId } = params.payload;
      const {password} = body;
      const toUpdate = {
        password
      };
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return Model.updateUser(userId, toUpdate)
        .then(() => {
          return {};
        });
    }
  }

  class UserAvatar {
    setup(app) {
      this.app = app;
    }

    /*
     * todo
     * change (transfer to file service )
     * */
    patch(id, data, params) {
      const {userId} = params.payload;
      const {files} = params;
      let currentUser = null;
      let newUserAvatar = '';

      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {}}, 400));
      }

      const file = files[0];
      let userDirectory = `users_${userId}`;

      return Model.findById(userId)
        .then((user) => {
          if (!user) {
            return Promise.reject(new errors.BadRequest('Invalid user Id', {errors: {id: userId}}, 400));
          }

          currentUser = user;
          let fileName = _.takeRight(file.key.split('/'), 1).join('/');
          let host = _.takeWhile(file.location.split(file.key)).join('/');
          newUserAvatar = `${host}${userDirectory}/${fileName}`;
          return s3Service.renameDocument(file.key, `${userDirectory}/${fileName}`);
        }).then(() => {
          const {avatarUrl} =  currentUser;
          if (!avatarUrl) return;

          const path = _.takeRight(currentUser.avatarUrl.split('/'), 2).join('/');
          return s3Service.deleteDocument(path);
        }).then(() => {
          currentUser.avatarUrl = newUserAvatar;
          return currentUser.save();
        }).then(() => {
          return {};
        });
    }

    /*
     * todo
     * change (transfer to file service )
     * */
    remove(id, params) {
      const {userId} = params.payload;
      let avatarUrl = '';
      let currentUser = null;

      return Model.findById(userId)
        .then((user) => {
          if (!user) {
            return Promise.reject(new errors.BadRequest('Invalid user Id', {errors: {userId}}, 400));
          }

          currentUser = user;
          return Model.updateUser(userId, {avatarUrl});
        })
        .then(() => {
          const path = _.takeRight(currentUser.avatarUrl.split('/'), 2).join('/');
          return s3Service.deleteDocument(path);
        }).then(() => {
          return {};
        });
    }
  }

  app.use('/users', createService(options));
  const service = app.service('users');

  app.use('/' + routes['avatar'], new UserAvatar());
  const userAvatarService = app.service(routes['avatar']);

  app.use('/' + routes['current'], new CurrentUser());
  const currentUserService = app.service(routes['current']);

  app.use('/' + routes['resend'], new ResendUser());
  const resendUserService = app.service(routes['resend']);

  app.use('/' + routes['code'], new UserCode());
  const userCodeService = app.service(routes['code']);

  app.use('/' + routes['password_reset'], new UserPasswordReset());
  const userPasswordResetService = app.service(routes['password_reset']);

  app.use('/' + routes['profile_password_reset'], new UserProfilePassword());
  const userProfilePasswordService = app.service(routes['profile_password_reset']);

  app.use('/' + routes['basic'], new User());
  const userApiService = app.service(routes['basic']);

  currentUserService.hooks(hooks);
  resendUserService.hooks(hooks);
  userCodeService.hooks(hooks);
  userApiService.hooks(hooks);
  userAvatarService.hooks(hooks);
  userPasswordResetService.hooks(hooks);
  userProfilePasswordService.hooks(hooks);
  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
