const validators = require('../../../utils/validators');
const { authenticate } = require('feathers-authentication').hooks;
const { getErrorObject } = require('../../../utils/errorHandler');
const {hashPassword} = require('feathers-authentication-local').hooks;
const commonHooks = require('feathers-hooks-common');
const errors = require('feathers-errors');
const bcrypt = require('bcryptjs');

function checkRestrictionMethods(hook) {
  if ((hook.method == 'get' || hook.method == 'find' || hook.method == 'patch') && hook.path == 'api/v1/users') {
    return true;
  }

  if ((hook.method == 'find' || hook.method == 'patch') && hook.path == 'api/v1/users/current') {
    return true;
  }

  if (((hook.method == 'patch') || (hook.method == 'remove')) && hook.path == 'api/v1/users/avatar') {
    return true;
  }

  return false;
}

function createValidators(hook) {
  let validateErrors = null;
  if (hook.path === 'api/v1/users') {
    validateErrors = validators.users.create(hook.data);
  }

  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }

  return hook;
}

function patchValidators(hook) {
  const sequelizeClient = hook.app.get('sequelizeClient');
  let validateErrors = null;
  if (hook.path === 'api/v1/users/current') {
    validateErrors = validators.users.patchCurrent(hook.data);
  }

  if (hook.path === 'api/v1/users') {
    validateErrors = validators.users.patch(hook.data);
  }

  if (hook.path === 'api/v1/users/code') {
    validateErrors = validators.users.patchByCode(hook.data);
  }

  if (hook.path === 'api/v1/users/profile/password') {
    if (!hook.data.password || !hook.data.oldPassword) {
      return Promise.reject(new errors.BadRequest('Old password and password are required', { errors: {} }, 400));
    }

    const userId = hook.params.payload.userId;
    return sequelizeClient.models.users
      .getFullUserById(userId)
      .then((user) => {
        if (!user) {
          return Promise.reject(new errors.BadRequest('User is not exists', { errors: { userId : userId } }, 400));
        }

        let password = user.password;

        return new Promise((resolve, reject) => {
          bcrypt.compare(hook.data.oldPassword, password, (err, res) => {
            if (err || !res) {
              return reject(new errors.BadRequest('Old password is incorrect ', {errors: {oldPassword: hook.data.oldPassword}}, 400));
            }

            return resolve(hook);
          });
        });
      });
  }


  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }
  return hook;
}

module.exports = {
  before: {
    all: [commonHooks.iffElse(checkRestrictionMethods, authenticate('jwt'), [])],
    find: [],
    get: [],
    create: [createValidators],
    update: [],
    patch: [hashPassword(), patchValidators],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [function (hook) {
      hook.error = getErrorObject(hook.error);
    }],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
