// Initializes the `confirm-codes` service on path `/confirm-codes`
const createService = require('feathers-sequelize');
const createModel = require('../../models/confirm-codes.model');
const hooks = require('./confirm-codes.hooks');
const filters = require('./confirm-codes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'confirm-codes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/confirm-codes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('confirm-codes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
