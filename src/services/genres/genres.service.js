const path = require('path');
const createModel = require('../../models/genres.model');
const hooks = require('./genres.hooks');
//const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  //const sequelize = app.get('sequelizeClient');
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'genres')
  };

  class Genre {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const {query} = params;
      let {limit, page, offset, genre} = query;
      const {SEARCH} = constants;
      genre = genre || '';

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      return Model.getAll(genre, _limit, _offset);
    }
  }

  app.use('/' + routes['basic'], new Genre());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
};

