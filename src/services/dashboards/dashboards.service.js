const path = require('path');
const createModel = require('../../models/dashboards.model');
const createVlocksModel = require('../../models/blocks.model');
const hooks = require('./dashboards.hooks');
const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const blocksModel = createVlocksModel(app);
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'dashboards')
  };

  class Dashboard {
    setup(app) {
      this.app = app;
    }

    get(id, params) {
      const {query} = params;
      const {limit, page, offset} = query;
      const {SEARCH} = constants;
      let dashboardObj = {};
      let blocksObj = {};

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      return Model.getByIdOnlyDashboard(id)
        .then((dashboard) => {
          if (!dashboard) {
            return Promise.reject(new errors.BadRequest('Invalid dashboard Id', {errors: {id: id}}, 400));
          }
          dashboardObj = dashboard;
          return blocksModel.getBlocksByDashboardId(id, _limit, _offset);
        }).then((blocks) => {
          blocksObj = blocks;
          return blocksModel.getCountBlocksInDashboard(id);
        }).then((count) => {
          let blocks = blocksObj.map(function(blockObj){ return blockObj.toJSON(); });
          dashboardObj = dashboardObj.toJSON();

          dashboardObj['blocks'] = blocks;
          dashboardObj['blocksCount'] = count;
          return dashboardObj;
        });
    }
  }

  app.use('/' + routes['basic'], new Dashboard());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
};
