const path = require('path');
const createModel = require('../../models/links.model');
const hooks = require('./links.hooks');
const createItemsModel = require('../../models/items.model');
const errors = require('feathers-errors');
const _ = require('lodash');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const itemsModel = createItemsModel(app);
  const sequelize = app.get('sequelizeClient');
  const s3Service = app.get('s3');
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'links'),
    base_links: path.join(routePrefix, 'links/base/'),
    links_docs: path.join(routePrefix, 'links/docs'),
    links_video: path.join(routePrefix, 'links/video'),
  };

  class Link {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      let {name, blockId, itemTypeId, linkDescription, title, url} = data;
      const {payload} = params;
      const {userId} = payload;
      const position = 1;

      linkDescription = linkDescription || '';
      url = url || '';

      let avaliableTypes = [
        constants.ITEM_TYPES.NEW_DOCUMENT,
        constants.ITEM_TYPES.VIDEO_EMBED,
        constants.ITEM_TYPES.IMAGE_LINK
      ];
      if (avaliableTypes.indexOf(itemTypeId) === -1) {
        return Promise.reject(new errors.BadRequest('Invalid item type', {errors: {}}, 400));
      }

      return sequelize.transaction((_transaction) => {
        return itemsModel.incrementFromPosition({
          blockId,
          position
        }, {
          transaction: _transaction
        }).then(() => {
          return itemsModel.createItem({
            name,
            blockId,
            itemTypeId,
            position,
            userId
          }, {
            transaction: _transaction
          });
        }).then((item) => {
          let itemId = item.id;
          return Model.createLink({
            itemId,
            title,
            linkDescription,
            url
          }, {
            transaction: _transaction
          });
        }).catch((err) => {
          _transaction.rollback(err);
          return Promise.reject(err);
        });
      });
    }

    patch(id, body) {
      const {title, description, url} = body;
      const toUpdate = {title, description, url};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return Model.updateLink(id, toUpdate)
        .then(() => {
          return {};
        });
    }
  }

  class DocsLink {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {itemId} = data;
      const {files} = params;
      let blockId = null;
      let url = '';
      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {id: itemId}}, 400));
      }

      const file = files[0];
      let itemObj = null;
      let fileName = _.takeRight(file.key.split('/'), 1).join('/');
      let host = _.takeWhile(file.location.split(file.key)).join('/');

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return itemsModel.getItemById(itemId)
          .then((item) => {
            if (item['links'].length < 1) {
              return Promise.reject(new errors.BadRequest('There are not links', {errors: {id: itemId}}, 400));
            }
            itemObj = item;
            blockId = item.blockId;
            let docsDirectory = `dashboard_${item.block.dashboardId}/block_${blockId}/items_${itemId}/docs`;
            url = `${host}${docsDirectory}/${fileName}`;
            return s3Service.renameDocument(file.key, `${docsDirectory}/${fileName}`);
          }).then(() => {
            let link = itemObj['links'][0];
            return Model.updateLink(link.id, {url: url}, options);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class VideoLink {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {itemId} = data;
      const {files} = params;
      let blockId = null;
      let url = '';
      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {id: itemId}}, 400));
      }

      const file = files[0];
      let itemObj = null;
      let fileName = _.takeRight(file.key.split('/'), 1).join('/');
      let host = _.takeWhile(file.location.split(file.key)).join('/');

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return itemsModel.getItemById(itemId)
          .then((item) => {
            if (item['links'].length < 1) {
              return Promise.reject(new errors.BadRequest('There are not links', {errors: {id: itemId}}, 400));
            }

            itemObj = item;
            blockId = item.blockId;
            let videoDirectory = `dashboard_${item.block.dashboardId}/block_${blockId}/items_${itemId}/video`;
            url = `${host}${videoDirectory}/${fileName}`;
            return s3Service.renameDocument(file.key, `${videoDirectory}/${fileName}`);
          }).then(() => {
            let link = itemObj['links'][0];
            return Model.updateLink(link.id, {url: url}, options);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class BaseLink {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      let {name, blockId, itemTypeId, links, itemId} = data;
      const {payload} = params;
      const {userId} = payload;
      const position = 1;

      links = links || [];

      let avaliableTypes = [
        constants.ITEM_TYPES.LIST_OF_LINKS
      ];
      if (avaliableTypes.indexOf(itemTypeId) === -1) {
        return Promise.reject(new errors.BadRequest('Invalid item type', {errors: {}}, 400));
      }

      return sequelize.transaction((_transaction) => {
        return new Promise(function(resolve) {
          if (typeof(itemId) === 'undefined') {
            return itemsModel.incrementFromPosition({
              blockId,
              position
            }, {
              transaction: _transaction
            }).then(() => {
              return itemsModel.createItem({
                name,
                blockId,
                itemTypeId,
                position,
                userId
              }, {
                transaction: _transaction
              });
            }).then((item) => {
              return resolve(item);
            });
          }

          return resolve();
        }).then((item) => {
          if (typeof(item) !== 'undefined') {
            itemId = item.id;
          }

          links = links.map((link) => {
            return {
              itemId: itemId,
              title: link.title,
              url: link.url,
            };
          });

          return Model.bulkCreate(links, {
            transaction: _transaction
          });
        });
      });
    }

    patch(id, body) {
      const {links} = body;
      let toUpdate = {};
      let promises = [];

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        links.forEach((link) => {
          toUpdate = {
            title: link.title,
            description: link.description,
            url: link.url
          };
          promises.push(Model.updateLink(link.id, toUpdate, options));
        });

        return Promise.all(promises).then(() => {
          return {};
        });
      });
    }

    remove(id, params) {
      const {query} = params;
      const {ids} = query;
      let itemId = null;

      if (ids.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of links should be at least 1', {errors: {}}, 400));
      }

      let firstId = ids[0];
      return Model.getLinkById(firstId)
        .then((link) => {
          itemId = link.itemId;
          return Model.getCount({
            itemId: link.itemId
          });
        }).then((countLinks) => {
          if ((countLinks - ids.length) < 1) {
            return Promise.reject(new errors.BadRequest('You cannot remove item', {errors: {}}, 400));
          }
          return Model.deleteLinksByItemIdAndIds(ids, itemId);
        }).then(() => {
          return {};
        });
    }
  }

  app.use('/' + routes['base_links'], new BaseLink());
  const baseLinkService = app.service(routes['base_links']);

  app.use('/' + routes['basic'], new Link());
  const service = app.service(routes['basic']);

  app.use('/' + routes['links_docs'], new DocsLink());
  const docsLinkService = app.service(routes['links_docs']);

  app.use('/' + routes['links_video'], new VideoLink());
  const videoLinkService = app.service(routes['links_video']);

  service.hooks(hooks);
  baseLinkService.hooks(hooks);
  docsLinkService.hooks(hooks);
  videoLinkService.hooks(hooks);
};
