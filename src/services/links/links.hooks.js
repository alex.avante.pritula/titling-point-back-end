const { authenticate } = require('feathers-authentication').hooks;
const validators = require('../../../utils/validators');
const { getErrorObject } = require('../../../utils/errorHandler');

function createValidators(hook) {
  let validateErrors = null;

  if (hook.path === 'api/v1/links') {
    validateErrors = validators.links.create(hook.data);
  }

  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }
  return hook;
}

function patchValidators(hook) {
  let validateErrors = null;

  if (validateErrors !== null) {
    validateErrors.code = 400;
    return Promise.reject(validateErrors);
  }
  return hook;
}

function formateResponse(hook) {
  return hook;
}
module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [createValidators],
    update: [],
    patch: [patchValidators],
    remove: []
  },

  after: {
    all: [formateResponse],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [function (hook) {
      hook.error = getErrorObject(hook.error);
    }],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
