// Initializes the `creativity-image-sizes` service on path `/creativity-image-sizes`
const createService = require('feathers-sequelize');
const createModel = require('../../models/creativity-image-sizes.model');
const hooks = require('./creativity-image-sizes.hooks');
const filters = require('./creativity-image-sizes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'creativity-image-sizes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/creativity-image-sizes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('creativity-image-sizes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
