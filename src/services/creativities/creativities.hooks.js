const { authenticate } = require('feathers-authentication').hooks;
const { getErrorObject } = require('../../../utils/errorHandler');
const errors = require('feathers-errors');

function patchValidators(hook) {
  const sequelizeClient = hook.app.get('sequelizeClient');
  if (hook.path === 'api/v1/creativities') {

    let {
      createStatuses,
      createCategories,
      createFormats,
      createPlatforms,
    } = hook.data;


    let categories = createCategories.map(createCategory => createCategory.toLowerCase());
    let platforms = createPlatforms.map(createPlatform => createPlatform.toLowerCase());
    let formats = createFormats.map(createFormat => createFormat.toLowerCase());
    let statuses = createStatuses.map(createStatus => createStatus.toLowerCase());

    return sequelizeClient.models.formats
      .getExistingFormatNames(formats)
      .then((formatsList) => {
        if (formatsList.length > 0) {
          return Promise.reject(new errors.BadRequest('Unique violation - format must be unique', { errors: {} }, 400));
        }
        return sequelizeClient.models.platforms.getExistingPlatformNames(platforms);
      }).then((platformsList) => {
        if (platformsList.length > 0) {
          return Promise.reject(new errors.BadRequest('Unique violation - platform must be unique', { errors: {} }, 400));
        }
        return sequelizeClient.models.statuses.getExistingStatusNames(statuses);
      }).then((statusesList) => {
        if (statusesList.length > 0) {
          return Promise.reject(new errors.BadRequest('Unique violation - status must be unique', { errors: {} }, 400));
        }
        return sequelizeClient.models.categories.getExistingCategoryNames(categories);
      }).then((categoriesList) => {
        if (categoriesList.length > 0) {
          return Promise.reject(new errors.BadRequest('Unique violation - category must be unique', { errors: {} }, 400));
        }
      });
  }
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [patchValidators],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [function (hook) {
      hook.error = getErrorObject(hook.error);
    }],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
