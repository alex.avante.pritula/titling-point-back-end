const path = require('path');
const _ = require('lodash');
const createModel = require('../../models/creativities.model');
const createTabsModel = require('../../models/tabs.model');
const createFoldersModel = require('../../models/folders.model');
const createDocsModel = require('../../models/docs.model');
const createCategoriesModel = require('../../models/categories.model');
const createPlatormsModel = require('../../models/platforms.model');
const createFormatsModel = require('../../models/formats.model');
const createStatusesModel = require('../../models/statuses.model');
const createSizesModel = require('../../models/sizes.model');
const createCreativitiesCategoriesModel = require('../../models/creativity-categories.model');
const createCreativitiesFormatsModel = require('../../models/creativities-formats.model');
const createCreativityPlatformsModel = require('../../models/creativity-platforms.model');
const createCreativitiesStatusesModel = require('../../models/creativities-statuses.model');
const createCreativitiesSizesModel = require('../../models/creativities-sizes.model');
const createCreativityImageSizesModel = require('../../models/creativity-image-sizes.model');
const hooks = require('./creativities.hooks');
const errors = require('feathers-errors');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const tabsModel = createTabsModel(app);
  const foldersModel = createFoldersModel(app);
  const docsModel = createDocsModel(app);
  const categoriesModel = createCategoriesModel(app);
  const platformsModel = createPlatormsModel(app);
  const formatsModel = createFormatsModel(app);
  const statusesModel = createStatusesModel(app);
  const sizesModel = createSizesModel(app);
  const creativitiesCategoriesModel = createCreativitiesCategoriesModel(app);
  const creativitiesFormatsModel = createCreativitiesFormatsModel(app);
  const creativitiesPlatformsModel = createCreativityPlatformsModel(app);
  const creativitiesStatusesModel = createCreativitiesStatusesModel(app);
  const creativitiesSizesModel = createCreativitiesSizesModel(app);
  const creativityImageSizesModel = createCreativityImageSizesModel(app);
  const routePrefix = app.get('prefix');
  const sequelize = app.get('sequelizeClient');
  const constants = app.get('constants');
  const s3Service = app.get('s3');
  const routes = {
    basic: path.join(routePrefix, '/creativities'),
    creativity_images: path.join(routePrefix, '/creativities/:creativityId/images'),
    creativity_tabs: path.join(routePrefix, '/creativities/tabs'),
    creativity_games: path.join(routePrefix, '/creativities/games/:gameId'),
    creativity_folders: path.join(routePrefix, '/creativities/folders'),
    creativity_docs: path.join(routePrefix, '/creativities/docs'),
    creativity_tabs_position: path.join(routePrefix, '/creativities/tabs/position'),
    creativity_folders_position: path.join(routePrefix, '/creativities/folders/position'),
    creativity_docs_position: path.join(routePrefix, '/creativities/docs/position'),
    creativity_folders_docs_position: path.join(routePrefix, '/creativities/folders/docs/position'),
    creativity_tabs_folders_position: path.join(routePrefix, '/creativities/tabs/folders/position'),
  };

  class Creativities {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {payload} = params;
      const {userId} = payload;
      const ownerId = userId;

      let { gameId, name, feedback, jiraTicket, used, languages, statuses, categories, formats, platforms} = data;
      if (categories.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of categories must be at least 1', {errors: {}}, 400));
      }

      const toCreate = {gameId, name, feedback, jiraTicket, used, languages, ownerId};
      for (let key in toCreate) {
        if (!toCreate[key]) {
          delete toCreate[key];
        }
      }

      let notExistingCategories = [];
      let notExistingFormats = [];
      let notExistingPlatforms = [];
      let notExistingStatuses = [];
      let creativityId = null;

      categories = categories.map(category => category.toLowerCase());
      platforms = platforms.map(platform => platform.toLowerCase());
      formats = formats.map(format => format.toLowerCase());
      statuses = statuses.map(status => status.toLowerCase());

      return sequelize.transaction((transaction) => {
        const options = {transaction};

        return categoriesModel.getExistingCategoryNames(categories, options)
          .then((existingCategories) => {
            //handle categories
            let existingCategoriesMap = existingCategories.map(existingCategory => existingCategory.category);
            notExistingCategories = _.difference(categories, existingCategoriesMap);
            let notExistingCategoriesMap = notExistingCategories.map(notExistingCategory => ({category: notExistingCategory}));
            return categoriesModel.createCategories(notExistingCategoriesMap, options);
          })
          .then(() => {
            return formatsModel.getExistingFormatNames(formats, options);
          })
          .then((existingFormats) => {
            //handle formats
            let existingFormatsMap = existingFormats.map(existingFormat => existingFormat.format);
            notExistingFormats = _.difference(formats, existingFormatsMap);
            let notExistingFormatsMap = notExistingFormats.map(notExistingFromat => ({format: notExistingFromat}));
            return formatsModel.createFormats(notExistingFormatsMap, options);
          })
          .then(() => {
            return platformsModel.getExistingPlatformNames(platforms, options);
          })
          .then((existingPlatforms) => {
            //handle platforms
            let existingPlatformsMap = existingPlatforms.map(existingPlatform => existingPlatform.format);
            notExistingPlatforms = _.difference(platforms, existingPlatformsMap);
            let notExistingPlatformsMap = notExistingPlatforms.map(notExistingPlatform => ({platform: notExistingPlatform}));
            return platformsModel.createPlatforms(notExistingPlatformsMap, options);
          })
          .then(() => {
            return statusesModel.getExistingStatusNames(statuses, options);
          })
          .then((existingStatuses) => {
            //handle statuses
            let existingStatusesMap = existingStatuses.map(existingStatus => existingStatus.format);
            notExistingStatuses = _.difference(statuses, existingStatusesMap);
            let notExistingStatusesMap = notExistingStatuses.map(notExistingStatus => ({status: notExistingStatus}));
            return statusesModel.createStatuses(notExistingStatusesMap, options);
          })
          .then(() => {
            return Model.createCreativityAsset(toCreate, options);
          })
          .then(creativity => {
            creativityId = creativity.id;
            return categoriesModel.getExistingCategoryNames(categories, options);
          })
          .then((requestedCategories) => {
            const creativityCategories = requestedCategories.map(requestedCategory => ({
              categoryId: requestedCategory.id,
              creativityId: creativityId
            }));

            return creativitiesCategoriesModel.connectCreativityCategories(creativityCategories, options);
          })
          .then(() => {
            return formatsModel.getExistingFormatNames(formats, options);
          })
          .then((requestedFormats) => {
            const creativityFormats = requestedFormats.map(requestedFormat => ({
              formatId: requestedFormat.id,
              creativityId: creativityId
            }));

            return creativitiesFormatsModel.connectCreativityFormats(creativityFormats, options);
          })
          .then(() => {
            return platformsModel.getExistingPlatformNames(platforms, options);
          })
          .then((requestedPlatforms) => {
            const creativityPlatforms = requestedPlatforms.map(requestedPlatform => ({
              platformId: requestedPlatform.id,
              creativityId: creativityId
            }));

            return creativitiesPlatformsModel.connectCreativityPlatforms(creativityPlatforms, options);
          })
          .then(() => {
            return statusesModel.getExistingStatusNames(statuses, options);
          })
          .then((requestedStatuses) => {
            const creativityStatuses = requestedStatuses.map(requestedStatus => ({
              statusId: requestedStatus.id,
              creativityId: creativityId
            }));

            return creativitiesStatusesModel.connectCreativityStatuses(creativityStatuses, options);
          })
          .then(() => {
            return Model.getCreativityById(creativityId, options);
          });
      });
    }

    patch(id, body) {
      let {
        name,
        feedback,
        jiraTicket,
        used,
        languages,
        createStatuses,
        removeStatuses,
        createCategories,
        removeCategories,
        createFormats,
        removeFormats,
        createPlatforms,
        removePlatforms
      } = body;

      let notExistingCategories = [];
      let notExistingFormats = [];
      let notExistingPlatforms = [];
      let notExistingStatuses = [];

      let categories = createCategories.map(createCategory => createCategory.toLowerCase());
      let platforms = createPlatforms.map(createPlatform => createPlatform.toLowerCase());
      let formats = createFormats.map(createFormat => createFormat.toLowerCase());
      let statuses = createStatuses.map(createStatus => createStatus.toLowerCase());

      const toUpdate = {name, feedback, jiraTicket, used, languages};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model
          .updateCreativity(id, toUpdate, options)
          .then(() => {
            if (removeCategories && removeCategories.length > 0) {
              return creativitiesCategoriesModel.deleteCreativityCategories({
                creativityId: id,
                categoryIds: removeCategories
              }, options);
            }
          }).then(() => {
            if (removeFormats && removeFormats.length > 0) {
              return creativitiesFormatsModel.deleteCreativityFormats({
                creativityId: id,
                formatIds: removeFormats
              }, options);
            }
          }).then(() => {
            if (removeStatuses && removeStatuses.length > 0) {
              return creativitiesStatusesModel.deleteCreativityStatuses({
                creativityId: id,
                statusIds: removeStatuses
              }, options);
            }
          }).then(() => {
            if (removePlatforms && removePlatforms.length > 0) {
              return creativitiesPlatformsModel.deleteCreativityPlatforms({
                creativityId: id,
                platformIds: removePlatforms
              }, options);
            }
          }).then(() => {
            return categoriesModel.getExistingCategoryNames(categories, options);
          }).then((existingCategories) => {
            //handle categories
            let existingCategoriesMap = existingCategories.map(existingCategory => existingCategory.category);
            notExistingCategories = _.difference(categories, existingCategoriesMap);
            let notExistingCategoriesMap = notExistingCategories.map(notExistingCategory => ({category: notExistingCategory}));
            return categoriesModel.createCategories(notExistingCategoriesMap, options);
          })
          .then(() => {
            return formatsModel.getExistingFormatNames(formats, options);
          })
          .then((existingFormats) => {
            //handle formats
            let existingFormatsMap = existingFormats.map(existingFormat => existingFormat.format);
            notExistingFormats = _.difference(formats, existingFormatsMap);
            let notExistingFormatsMap = notExistingFormats.map(notExistingFromat => ({format: notExistingFromat}));
            return formatsModel.createFormats(notExistingFormatsMap, options);
          })
          .then(() => {
            return platformsModel.getExistingPlatformNames(platforms, options);
          })
          .then((existingPlatforms) => {
            //handle platforms
            let existingPlatformsMap = existingPlatforms.map(existingPlatform => existingPlatform.format);
            notExistingPlatforms = _.difference(platforms, existingPlatformsMap);
            let notExistingPlatformsMap = notExistingPlatforms.map(notExistingPlatform => ({platform: notExistingPlatform}));
            return platformsModel.createPlatforms(notExistingPlatformsMap, options);
          })
          .then(() => {
            return statusesModel.getExistingStatusNames(statuses, options);
          })
          .then((existingStatuses) => {
            //handle statuses
            let existingStatusesMap = existingStatuses.map(existingStatus => existingStatus.format);
            notExistingStatuses = _.difference(statuses, existingStatusesMap);
            let notExistingStatusesMap = notExistingStatuses.map(notExistingStatus => ({status: notExistingStatus}));
            return statusesModel.createStatuses(notExistingStatusesMap, options);
          })
          .then(() => {
            return categoriesModel.getExistingCategoryNames(categories, options);
          })
          .then((requestedCategories) => {
            const creativityCategories = requestedCategories.map(requestedCategory => ({
              categoryId: requestedCategory.id,
              creativityId: id
            }));

            return creativitiesCategoriesModel.connectCreativityCategories(creativityCategories, options);
          })
          .then(() => {
            return formatsModel.getExistingFormatNames(formats, options);
          })
          .then((requestedFormats) => {
            const creativityFormats = requestedFormats.map(requestedFormat => ({
              formatId: requestedFormat.id,
              creativityId: id
            }));

            return creativitiesFormatsModel.connectCreativityFormats(creativityFormats, options);
          })
          .then(() => {
            return platformsModel.getExistingPlatformNames(platforms, options);
          })
          .then((requestedPlatforms) => {
            const creativityPlatforms = requestedPlatforms.map(requestedPlatform => ({
              platformId: requestedPlatform.id,
              creativityId: id
            }));

            return creativitiesPlatformsModel.connectCreativityPlatforms(creativityPlatforms, options);
          })
          .then(() => {
            return statusesModel.getExistingStatusNames(statuses, options);
          })
          .then((requestedStatuses) => {
            const creativityStatuses = requestedStatuses.map(requestedStatus => ({
              statusId: requestedStatus.id,
              creativityId: id
            }));

            return creativitiesStatusesModel.connectCreativityStatuses(creativityStatuses, options);
          })
          .then(() => {
            return Model.getCreativityById(id, options);
          });
      });
    }

    get(id) {
      return Model.getById(id).then((creativity) => {
        return creativity.toJSON();
      });
    }

    find(params) {
      const { query } = params;
      const {SEARCH} = constants;
      let {
        limit,
        page,
        offset,
        gameId,
        categoryId,
        platformId,
        formatId,
        statusId,
        sizeId,
        departmentId,
        createdAtFrom,
        createdAtTill
      } = query;

      let filters = {
        gameId,
        categoryId,
        platformId,
        formatId,
        statusId,
        sizeId,
        departmentId,
        createdAtFrom,
        createdAtTill
      };

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getCreativities(filters, _limit, _offset)
        .then((creativities) => {
          let creativitiesMap = creativities.rows.map(creativity => {
            return creativity.toJSON();
          });

          return {
            count: creativities.count,
            rows: creativitiesMap
          };
        });
    }

    remove(id) {
      let docsMap = [];
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        let filters = {
          includeDocs: true
        };
        return Model.getById(id, options, filters)
          .then((creativity) => {
            if (!creativity) {
              return Promise.reject(new errors.BadRequest('Invalid creativity Id', {errors: {id: id}}, 400));
            }

            creativity.tabs.map(tab => {
              tab.folders.map(folder => {
                return folder.docs.map(doc => {
                  docsMap.push(_.takeRight(doc.documentUrl.split('/'), 5).join('/'));
                });
              });
            });

            return Model.deleteCreativity(id, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class CreativityImages {
    setup(app) {
      this.app = app;
    }

    patch(id, data, params) {
      let {files, creativityId} = params;
      let sizeWidth;
      let sizeHeight;
      let sizeId;
      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {}}, 400));
      }

      if(!creativityId) {
        return Promise.reject(new errors.BadRequest('Creativity is required', {errors: {}}, 400));
      }

      creativityId = parseInt(creativityId);

      const file = files[0];
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return s3Service.getSize(file.key)
          .then((data) => {
            sizeWidth = data.dimensions.width;
            sizeHeight = data.dimensions.height;
            return sizesModel.getByWidthHeight(sizeWidth, sizeHeight, options);
          }).then((size) => {
            if (!size) {
              return sizesModel.createSize({
                width: sizeWidth,
                height: sizeHeight}, options)
                .then((size) => {
                  sizeId = size.id;
                });
            } else {
              sizeId = size.id;
            }
          }).then(() => {
            return creativitiesSizesModel.getCreativitySize(creativityId, sizeId, options);
          }).then((creativitySize) => {
            if (!creativitySize) {
              return creativitiesSizesModel.connectCreativitySizes([{
                sizeId,
                creativityId: creativityId
              }], options);
            }
          }).then(() => {
            return creativityImageSizesModel.getMainImageCreativityImageSizeByCreativityId(creativityId, options);
          }).then((mainImageSize) => {
            if (!mainImageSize) {
              return creativityImageSizesModel.createImageSize({
                sizeId: sizeId,
                creativityId: creativityId,
                isMainImage: true,
                fileId: null
              }, options);
            }
          }).then(() => {
            return Model.updateCreativity(creativityId, {
              mainImageUrl: file.location
            }, options);
          }).then(() => {
            return {
              mainImageUrl: file.location
            };
          });
      });
    }

    remove(id, params) {
      let { creativityId } = params;
      let mainImageUrl = '';
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model.getById(creativityId, options)
          .then((creativity) => {
            if (!creativity) {
              return Promise.reject(new errors.BadRequest('Invalid creativity Id', {errors: {id: id}}, 400));
            }

            mainImageUrl = creativity.mainImageUrl;
            return creativityImageSizesModel.getMainImageCreativityImageSizeByCreativityId(creativityId, options);
          }).then((creativityImageSize) => {
            if (creativityImageSize) {
              return creativityImageSizesModel.getCreativityImageSizesNotMainImage(creativityId,
                creativityImageSize.sizeId, options
              ).then((creativityImageSize) => {
                if(creativityImageSize) {
                  return creativityImageSize.sizeId;
                } else {
                  return null;
                }
              });
            } else {
              return null;
            }
          }).then((sizeId) => {
            if (!sizeId) {
              return creativitiesSizesModel.deleteCreativitySizes(
                creativityId,
                sizeId, options)
                .then(() => {
                  return creativityImageSizesModel.deleteCreativityImageSizeMainImage(creativityId, options);
                });
            }
          }).then(() => {
            let mainImageKey = _.takeRight(mainImageUrl.split('/'), 2).join('/');
            return s3Service.deleteDocument(mainImageKey);
          }).then(() => {
            return Model.updateCreativity(creativityId, {
              mainImageUrl: ''
            }, options);
          }).then(() => {
            return {};
          });
      });
    }
  }

  /*
  * todo - change upload document(like upload main image)
  * todo - change removing doc/folder/tab(remove creativity size when there aren't more with same size)
  * */

  class CreativityTabs {
    setup(app) {
      this.app = app;
    }

    get(id, params) {
      const {query} = params;
      let {limit, page, offset} = query;
      const {SEARCH} = constants;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return tabsModel.getById(id, _limit, _offset)
        .then((tab) => {
          return tab.toJSON();
        });
    }

    create(data) {
      let {name, creativityId} = data;
      const position = 1;
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return tabsModel.incrementFromPosition({
          creativityId,
          position
        }, options).then(() => {
          return tabsModel.createTab({
            name,
            creativityId,
            position
          }, options);
        }).then((tab) => {
          return tab;
        });
      });
    }

    patch(id, body) {
      const {name} = body;
      const toUpdate = {name};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return tabsModel
        .updateTab(id, toUpdate)
        .then(() => {
          return {};
        });
    }

    remove(id) {
      let position = null;
      let creativityId = null;
      let docsMap = [];
      let sizesMap = [];
      let docIds = [];
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        let filters = {
          includeDocs: true
        };
        return tabsModel.getById(id, null, null, options, filters)
          .then((tab) => {
            if (!tab) {
              return Promise.reject(new errors.BadRequest('Invalid tab Id', {errors: {id: id}}, 400));
            }

            tab.folders.map(folder => {
              return folder.docs.map(doc => {
                docIds.push(doc.id);
                docsMap.push(_.takeRight(doc.documentUrl.split('/'), 2).join('/'));
              });
            });

            creativityId = tab.creativityId;
            position = tab.position;
            return creativityImageSizesModel.getCreativityImageSizesByFileIds(docIds, options);
          }).then((creativityImageSizes) => {
            sizesMap = creativityImageSizes.map(creativityImageSize => {
              return creativityImageSize.sizeId;
            });
            return tabsModel.deleteTab(id, options);
          }).then(() => {
            return tabsModel.moveOnCurrentPosition({creativityId, position}, options);
          }).then(() => {
            return creativityImageSizesModel.getImageCreativitySizes(creativityId, sizesMap, options);
          }).then((imageCreativitySizes) => {
            let existingImageCreativitySizeMap = imageCreativitySizes.map(
              imageCreativitySize => imageCreativitySize.sizeId
            );
            let notExistingImageCreativitySizes = _.difference(sizesMap, existingImageCreativitySizeMap);
            return creativitiesSizesModel.deleteCreativitySizes(
              creativityId,
              notExistingImageCreativitySizes, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class CreativityFolders {
    setup(app) {
      this.app = app;
    }

    get(id, params) {
      const {query} = params;
      let {limit, page, offset} = query;
      const {SEARCH} = constants;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return foldersModel.getById(id, _limit, _offset)
        .then((tab) => {
          return tab.toJSON();
        });
    }

    patch(id, body) {
      const {name} = body;
      const toUpdate = {name};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return foldersModel
        .updateFolder(id, toUpdate)
        .then(() => {
          return {};
        });
    }

    create(data) {
      let {name, tabId} = data;
      const position = 1;
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return foldersModel.incrementFromPosition({
          tabId,
          position
        }, options).then(() => {
          return foldersModel.createFolder({
            name,
            tabId,
            position
          }, options);
        }).then((folder) => {
          return folder;
        });
      });
    }

    remove(id) {
      let position = null;
      let tabId = null;
      let docsMap = [];
      let sizesMap = [];
      let docIds = [];
      let creativityId = null;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return foldersModel.getById(id, null, null, options)
          .then((folder) => {
            if (!folder) {
              return Promise.reject(new errors.BadRequest('Invalid folder Id', {errors: {id: id}}, 400));
            }

            creativityId = folder.tab.creativityId;
            docsMap = folder.docs.map(doc => {
              docIds.push(doc.id);
              return _.takeRight(doc.documentUrl.split('/'), 2).join('/');
            });

            position = folder.position;
            tabId = folder.tabId;
            return creativityImageSizesModel.getCreativityImageSizesByFileIds(docIds, options);
          }).then((creativityImageSizes) => {
            sizesMap = creativityImageSizes.map(creativityImageSize => {
              return creativityImageSize.sizeId;
            });
            return foldersModel.deleteFolder(id, options);
          }).then(() => {
            return foldersModel.moveOnCurrentPosition({tabId, position}, options);
          }).then(() => {
            return creativityImageSizesModel.getImageCreativitySizes(creativityId, sizesMap, options);
          }).then((imageCreativitySizes) => {
            let existingImageCreativitySizeMap = imageCreativitySizes.map(
              imageCreativitySize => imageCreativitySize.sizeId
            );
            let notExistingImageCreativitySizes = _.difference(sizesMap, existingImageCreativitySizeMap);
            return creativitiesSizesModel.deleteCreativitySizes(
              creativityId,
              notExistingImageCreativitySizes, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class CreativityDocs {
    setup(app) {
      this.app = app;
    }

    create(data, params) {
      const {folderId, name} = data;
      const {files} = params;
      let isImage = false;
      let sizeWidth;
      let sizeHeight;
      let sizeId;
      let docId;
      let position = 1;
      let creativityId = null;
      if (!files || files.length < 1) {
        return Promise.reject(new errors.BadRequest('There are not files', {errors: {}}, 400));
      }

      const file = files[0];
      if(file.mimetype.indexOf('image') !== -1) {
        isImage = true;
      }

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return foldersModel.getById(folderId, null, null, options)
          .then((folder) => {
            if (!folder) {
              return Promise.reject(new errors.BadRequest('Invalid folder Id', {errors: {id: folderId}}, 400));
            }

            creativityId = folder.tab.creativityId;
            return docsModel.incrementFromPosition({
              folderId,
              position
            }, options);
          }).then(() => {
            return docsModel.createDoc({
              folderId,
              name,
              position,
              documentUrl: file.location
            }, options);
          }).then((doc) => {
            docId = doc.id;
            if (!isImage) {
              return null;
            }

            return s3Service.getSize(file.key).then((data) => {
              sizeWidth = data.dimensions.width;
              sizeHeight = data.dimensions.height;
              return sizesModel.getByWidthHeight(sizeWidth, sizeHeight, options);
            }).then((size) => {
              if (!size) {
                return sizesModel.createSize({
                  width: sizeWidth,
                  height: sizeHeight}, options)
                  .then((size) => {
                    sizeId = size.id;
                  });
              } else {
                sizeId = size.id;
              }
            }).then(() => {
              return creativitiesSizesModel.getCreativitySize(creativityId, sizeId, options);
            }).then((creativitySize) => {
              if (!creativitySize) {
                return creativitiesSizesModel.connectCreativitySizes([{
                  sizeId,
                  creativityId: creativityId
                }], options);
              }
            }).then(() => {
              return creativityImageSizesModel.createImageSize({
                sizeId: sizeId,
                creativityId: creativityId,
                isMainImage: false,
                fileId: docId
              }, options);
            });
          }).then(() => {
            return {
              documentUrl: file.location
            };
          });
      });
    }

    patch(id, body) {
      const {name} = body;
      const toUpdate = {name};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return docsModel.updateDoc(id, toUpdate);
    }

    remove(id, params) {
      const {query} = params;
      const {ids, folderId} = query;
      let docsMap = [];
      let sizesMap = [];
      let creativityId = null;

      if (ids.length < 1) {
        return Promise.reject(new errors.BadRequest('Count of docs should be at least 1', {errors: {}}, 400));
      }

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return foldersModel.getById(folderId, null, null, options)
          .then((folder) => {
            if (!folder) {
              return Promise.reject(new errors.BadRequest('Invalid folder Id', {errors: {id: folderId}}, 400));
            }

            creativityId = folder.tab.creativityId;
            return docsModel.getByIds(ids);
          }).then((docs) => {
            docsMap = docs.map(doc => {
              return _.takeRight(doc.documentUrl.split('/'), 2).join('/');
            });

            return creativityImageSizesModel.getCreativityImageSizesByFileIds(ids, options);
          }).then((creativityImageSizes) => {
            if (creativityImageSizes) {
              sizesMap = creativityImageSizes.map(creativityImageSize => {
                return creativityImageSize.sizeId;
              });
            }
            return docsModel.deleteDocsByIdsAndFolderId(ids, folderId, options);
          }).then(() => {
            return creativityImageSizesModel.getImageCreativitySizes(creativityId, sizesMap, options);
          }).then((imageCreativitySizes) => {
            let existingImageCreativitySizeMap = imageCreativitySizes.map(
              imageCreativitySize => imageCreativitySize.sizeId
            );
            let notExistingImageCreativitySizes = _.difference(sizesMap, existingImageCreativitySizeMap);
            return creativitiesSizesModel.deleteCreativitySizes(
              creativityId,
              notExistingImageCreativitySizes, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }
  }

  class CreativityFolderPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let folder;
      let {newPosition} = body;
      let tabId;
      let currentPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return foldersModel.getById(id, null, null, options)
          .then(folderInstance => {
            if (!folderInstance) {
              return Promise.reject(new errors.BadRequest('folder not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'folder not found'
                }
              }, 400));
            }
            folder = folderInstance;
            return foldersModel.getMaxPosition(folder.tabId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            tabId = folder.tabId;
            currentPosition = folder.position;
            if (newPosition > currentPosition) {
              return foldersModel.decrementPosition(
                {
                  tabId,
                  newPosition,
                  currentPosition
                }, options);
            }
            if (newPosition < currentPosition) {
              return foldersModel.incrementPosition({
                tabId,
                newPosition,
                currentPosition
              }, options);
            }
          })
          .then(() => {
            return foldersModel.updateFolder(id, {position: newPosition}, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  class CreativityDocPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let doc;
      let {newPosition} = body;
      let folderId;
      let currentPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return docsModel.getById(id, options)
          .then(docInstance => {
            if (!docInstance) {
              return Promise.reject(new errors.BadRequest('doc not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'doc not found'
                }
              }, 400));
            }
            doc = docInstance;
            return docsModel.getMaxPosition(doc.folderId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            folderId = doc.folderId;
            currentPosition = doc.position;
            if (newPosition > currentPosition) {
              return docsModel.decrementPosition(
                {
                  folderId,
                  newPosition,
                  currentPosition
                }, options);
            }
            if (newPosition < currentPosition) {
              return docsModel.incrementPosition({
                folderId,
                newPosition,
                currentPosition
              }, options);
            }
          })
          .then(() => {
            return docsModel.updateDoc(id, {position: newPosition}, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  class CreativityTabPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let tab;
      let {newPosition} = body;
      let creativityId;
      let currentPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return tabsModel.getById(id, null, null, options)
          .then(tabInstance => {
            if (!tabInstance) {
              return Promise.reject(new errors.BadRequest('tab not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'tab not found'
                }
              }, 400));
            }
            tab = tabInstance;
            return tabsModel.getMaxPosition(tab.creativityId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            creativityId = tab.creativityId;
            currentPosition = tab.position;
            if (newPosition > currentPosition) {
              return tabsModel.decrementPosition(
                {
                  creativityId,
                  newPosition,
                  currentPosition
                }, options);
            }
            if (newPosition < currentPosition) {
              return tabsModel.incrementPosition({
                creativityId,
                newPosition,
                currentPosition
              }, options);
            }
          })
          .then(() => {
            return tabsModel.updateTab(id, {position: newPosition}, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  class CreativityFolderDocPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let doc;
      let {newPosition, destinationFolderId} = body;
      let folderId;
      let oldPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return docsModel.getById(id, options)
          .then(docInstance => {
            if (!docInstance) {
              return Promise.reject(new errors.BadRequest('doc not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'doc not found'
                }
              }, 400));
            }
            doc = docInstance;
            folderId = doc.folderId;
            oldPosition = doc.position;
            return tabsModel.getMaxPosition(destinationFolderId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            return docsModel.incrementFromPosition({
              folderId: destinationFolderId,
              position: newPosition
            }, options);
          })
          .then(() => {
            return docsModel.decrementToPosition(
              {
                folderId,
                position: oldPosition
              }, options);
          })
          .then(() => {
            return docsModel.updateDoc(id, {
              position: newPosition,
              folderId: destinationFolderId
            }, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  class CreativityTabFolderPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let folder;
      let {newPosition, destinationTabId} = body;
      let tabId;
      let oldPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return foldersModel.getById(id, null, null, options)
          .then(folderInstance => {
            if (!folderInstance) {
              return Promise.reject(new errors.BadRequest('folder not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'folder not found'
                }
              }, 400));
            }
            folder = folderInstance;
            tabId = folder.folderId;
            oldPosition = folder.position;
            return tabsModel.getMaxPosition(destinationTabId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            return foldersModel.incrementFromPosition({
              tabId: destinationTabId,
              position: newPosition
            }, options);
          })
          .then(() => {
            return foldersModel.decrementToPosition(
              {
                tabId,
                position: oldPosition
              }, options);
          })
          .then(() => {
            return foldersModel.updateFolder(id, {
              position: newPosition,
              tabId: destinationTabId
            }, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  class CreativityGames {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const { gameId, query } = params;
      const {SEARCH} = constants;
      let {limit, page, offset} = query;
      let filters = {
        gameId
      };

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getCreativities(filters, _limit, _offset)
        .then((creativities) => {
          return creativities;
        });
    }
  }

  app.use('/' + routes['basic'], new Creativities());
  const service = app.service(routes['basic']);

  app.use('/' + routes['creativity_images'], new CreativityImages());
  const creativityImageService = app.service(routes['creativity_images']);

  app.use('/' + routes['creativity_tabs'], new CreativityTabs());
  const creativityTabService = app.service(routes['creativity_tabs']);

  app.use('/' + routes['creativity_folders'], new CreativityFolders());
  const creativityFolderService = app.service(routes['creativity_folders']);

  app.use('/' + routes['creativity_docs'], new CreativityDocs());
  const creativityDocService = app.service(routes['creativity_docs']);

  app.use('/' + routes['creativity_tabs_position'], new CreativityTabPosition());
  const creativityTabPositionService = app.service(routes['creativity_tabs_position']);

  app.use('/' + routes['creativity_folders_position'], new CreativityFolderPosition());
  const creativityFolderPositionService = app.service(routes['creativity_folders_position']);

  app.use('/' + routes['creativity_docs_position'], new CreativityDocPosition());
  const creativityDocPositionService = app.service(routes['creativity_docs_position']);

  app.use('/' + routes['creativity_folders_docs_position'], new CreativityFolderDocPosition());
  const creativityFolderDocPositionService = app.service(routes['creativity_folders_docs_position']);

  app.use('/' + routes['creativity_tabs_folders_position'], new CreativityTabFolderPosition());
  const creativityTabFolderPositionService = app.service(routes['creativity_tabs_folders_position']);

  app.use('/' + routes['creativity_games'], new CreativityGames());
  const creativityGamesService = app.service(routes['creativity_games']);

  service.hooks(hooks);
  creativityImageService.hooks(hooks);
  creativityTabService.hooks(hooks);
  creativityFolderService.hooks(hooks);
  creativityDocService.hooks(hooks);
  creativityFolderPositionService.hooks(hooks);
  creativityDocPositionService.hooks(hooks);
  creativityTabPositionService.hooks(hooks);
  creativityGamesService.hooks(hooks);
  creativityTabFolderPositionService.hooks(hooks);
  creativityFolderDocPositionService.hooks(hooks);
};