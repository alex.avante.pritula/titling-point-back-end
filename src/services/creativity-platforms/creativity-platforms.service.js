// Initializes the `creativity-platforms` service on path `/creativity-platforms`
const createService = require('feathers-sequelize');
const createModel = require('../../models/creativity-platforms.model');
const hooks = require('./creativity-platforms.hooks');
const filters = require('./creativity-platforms.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'creativity-platforms',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/creativity-platforms', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('creativity-platforms');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
