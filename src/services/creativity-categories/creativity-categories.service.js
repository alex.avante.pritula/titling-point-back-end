// Initializes the `creativity-categories` service on path `/creativity-categories`
const createService = require('feathers-sequelize');
const createModel = require('../../models/creativity-categories.model');
const hooks = require('./creativity-categories.hooks');
const filters = require('./creativity-categories.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'creativity-categories',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/creativity-categories', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('creativity-categories');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
