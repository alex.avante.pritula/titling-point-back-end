const createModel = require('../../models/game-asset-folders.model');
const hooks = require('./game-asset-folders.hooks');
const path = require('path');
const errors = require('feathers-errors');
const _ = require('lodash');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const sequelize = app.get('sequelizeClient');
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const s3Service = app.get('s3');

  const routes = {
    basic: path.join(routePrefix, 'game-asset/folders'),
    game_asset_folders_position: path.join(routePrefix, 'game-asset/folders/position')
  };

  class GameAssetFolders {
    setup(app) {
      this.app = app;
    }

    get(id, params) {
      const {query} = params;
      let {limit, page, offset} = query;
      const {SEARCH} = constants;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getById(id, _limit, _offset)
        .then((tab) => {
          return tab.toJSON();
        });
    }

    create(data) {
      let {name, gameId} = data;
      const position = 1;
      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model.incrementFromPosition({
          gameId,
          position
        }, options).then(() => {
          return Model.createAssetFolder({
            name,
            gameId,
            position
          }, options);
        }).then((folder) => {
          return folder;
        });
      });
    }

    patch(id, body) {
      const {name} = body;
      const toUpdate = {name};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return Model
        .updateAssetFolder(id, toUpdate)
        .then(() => {
          return {};
        });
    }

    remove(id) {
      let position = null;
      let gameId = null;
      let docsMap = [];

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model.getById(id, null, null, options)
          .then((folder) => {
            if (!folder) {
              return Promise.reject(new errors.BadRequest('Invalid folder Id', {errors: {id: id}}, 400));
            }

            docsMap = folder.gameAssetDocs.map(doc => {
              return _.takeRight(doc.documentUrl.split('/'), 2).join('/');
            });

            position = folder.position;
            gameId = folder.gameId;
            return Model.deleteAssetFolder(id, options);
          }).then(() => {
            return Model.moveOnCurrentPosition({gameId, position}, options);
          }).then(() => {
            return s3Service.deleteObjects(docsMap);
          }).then(() => {
            return {};
          });
      });
    }

    find(params) {
      //get all folfers of game assets by gameId
      const { gameId, query } = params;
      const {SEARCH} = constants;
      let {limit, page, offset} = query;
      let filters = {
        gameId
      };

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getFolders(filters, _limit, _offset)
        .then((folders) => {
          return folders;
        });
    }
  }

  class GameAssetFolderPosition {
    setup(app) {
      this.app = app;
    }

    patch(id, body) {
      let folder;
      let {newPosition} = body;
      let gameId;
      let currentPosition;

      return sequelize.transaction((transaction) => {
        const options = {transaction};
        return Model.getById(id, null, null, options)
          .then(folderInstance => {
            if (!folderInstance) {
              return Promise.reject(new errors.BadRequest('asset folder not found', {
                errors: {
                  field: 'id',
                  code: 400,
                  message: 'asset folder not found'
                }
              }, 400));
            }
            folder = folderInstance;
            return Model.getMaxPosition(folder.gameId, options);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }

            gameId = folder.gameId;
            currentPosition = folder.position;
            if (newPosition > currentPosition) {
              return Model.decrementPosition(
                {
                  gameId,
                  newPosition,
                  currentPosition
                }, options);
            }
            if (newPosition < currentPosition) {
              return Model.incrementPosition({
                gameId,
                newPosition,
                currentPosition
              }, options);
            }
          })
          .then(() => {
            return Model.updateFolder(id, {position: newPosition}, options);
          })
          .then(() => {
            return {};
          });
      });
    }
  }

  app.use('/' + routes['basic'], new GameAssetFolders());
  const service = app.service(routes['basic']);

  app.use('/' + routes['game_asset_folders_position'], new GameAssetFolderPosition());
  const gameAssetFolderPositionService = app.service(routes['game_asset_folders_position']);

  service.hooks(hooks);
  gameAssetFolderPositionService.hooks(hooks);
};