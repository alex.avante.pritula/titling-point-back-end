// Initializes the `game-genres` service on path `/game-genres`
const createService = require('feathers-sequelize');
const createModel = require('../../models/game-genres.model');
const hooks = require('./game-genres.hooks');
const filters = require('./game-genres.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'game-genres',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/game-genres', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('game-genres');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
