// Initializes the `creativities_formats` service on path `/creativities-formats`
const createService = require('feathers-sequelize');
const createModel = require('../../models/creativities-formats.model');
const hooks = require('./creativities-formats.hooks');
const filters = require('./creativities-formats.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'creativities-formats',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/creativities-formats', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('creativities-formats');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
