// Initializes the `creativities_statuses` service on path `/creativities-statuses`
const createService = require('feathers-sequelize');
const createModel = require('../../models/creativities-statuses.model');
const hooks = require('./creativities-statuses.hooks');
const filters = require('./creativities-statuses.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'creativities-statuses',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/creativities-statuses', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('creativities-statuses');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
