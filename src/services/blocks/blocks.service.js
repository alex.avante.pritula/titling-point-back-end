const path = require('path');
const hooks = require('./blocks.hooks');
const createModel = require('../../models/blocks.model');
const errors = require('feathers-errors');
//const createDashboardModel = require('../../models/dashboards.model');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  //const dashboardModel = createDashboardModel(app);
  const s3Service = app.get('s3');
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const sequelize = app.get('sequelizeClient');

  const routes = {
    basic: path.join(routePrefix, 'blocks'),
    position: path.join(routePrefix, 'blocks/position'),
  };

  class Blocks {
    setup(app) {
      this.app = app;
    }

    get(id) {
      return Model.getById(id);
    }

    find(params) {
      const {query} = params;
      const {limit, page, offset} = query;
      const {SEARCH} = constants;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;
      return Model.getAll(_limit, _offset);
    }

    /*todo
     * add to transaction LOCK (LOCK TABLE items IN ROW EXCLUSIVE MODE;)
     * */
    remove(id) {
      let position = null;
      let dashboardId = null;
      let blockId = null;
      return sequelize.transaction((_transaction) => {
        return Model.getById(id)
          .then((block) => {
            if (!block) {
              return Promise.reject(new errors.BadRequest('Invalid block Id', {errors: {id: id}}, 400));
            }

            dashboardId = block.dashboardId;
            position = block.position;
            blockId = block.id;
            return Model.deleteBlock(id, {transaction: _transaction});
          }).then(() => {
            return Model.moveOnCurrentPosition({dashboardId, position}, {transaction: _transaction});
          }).then(() => {
            const itemPrefix = `dashboard_${dashboardId}/block_${blockId}/`;
            return s3Service.deleteDocuments(itemPrefix);
          }).then(() => {
            return {};
          });
      });
    }

    patch(id, body) {
      const {name, description, dashboardId, blockTypeId} = body;
      const toUpdate = {name, description, dashboardId, blockTypeId};
      for (let key in toUpdate) {
        if (!toUpdate[key]) {
          delete toUpdate[key];
        }
      }
      return Model.updateBlock(id, toUpdate);
    }

    create(data) {
      let {name, dashboardId, blockTypeId} = data;
      const position = 1;
      return sequelize.transaction((_transaction) => {
        return Model.incrementFromPosition({
          dashboardId,
          position
        }, {
          transaction: _transaction
        }).then(() => {
          return Model.createBlock({
            name,
            dashboardId,
            blockTypeId,
            position
          }, {
            transaction: _transaction
          });
        }).then((block) => {
          return block;
        }).catch(err => Promise.reject(err));
      });
    }
  }

  class Position {
    setup(app) {
      this.app = app;
    }

    /*todo
     * add transaction
     * add to transaction LOCK (LOCK TABLE items IN ROW EXCLUSIVE MODE;)
     * */
    patch(id, body) {
      let block;
      let {newPosition} = body;
      let dashboardId;
      let currentPosition;

      return sequelize.transaction((_transaction) => {
        return Model.findById(id)
          .then(blockInstance => {
            if (!blockInstance) {
              return Promise.reject(new errors.BadRequest('block not found', {
                errors: {
                  field: 'id',
                  code: 404,
                  message: 'block not found'
                }
              }, 400));
            }
            block = blockInstance;
            return Model.getMaxPosition(block.dashboardId);
          })
          .then(maxPosition => {
            if (maxPosition < newPosition || maxPosition < 0) {
              return Promise.reject({field: 'position', code: 400, message: `max position ${maxPosition}`});
            }
            dashboardId = block.dashboardId;
            currentPosition = block.position;
            if (newPosition > currentPosition) {
              return Model.decrementPosition(
                {
                  dashboardId,
                  newPosition,
                  currentPosition
                }, {
                  transaction: _transaction
                });
            }
            if (newPosition < currentPosition) {
              return Model.incrementPosition({
                dashboardId,
                newPosition,
                currentPosition
              }, {
                transaction: _transaction
              });
            }
          })
          .then(() => {
            return Model.updateBlock(id, {position: newPosition}, {transaction: _transaction});
          })
          .then(() => {
            return {message: 'ok'};
          })
          .catch(err => Promise.reject(err));
      });
    }
  }
  app.use('/' + routes['position'], new Position());
  const position = app.service(routes['position']);

  app.use('/' + routes['basic'], new Blocks());
  const service = app.service(routes['basic']);


  service.hooks(hooks);
  position.hooks(hooks);
};
