// Initializes the `block-types` service on path `/block-types`
const createService = require('feathers-sequelize');
const createModel = require('../../models/block-types.model');
const hooks = require('./block-types.hooks');
const filters = require('./block-types.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'block-types',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/block-types', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('block-types');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
