const roles = require('./roles/roles.service.js');
const users = require('./users/users.service.js');
const permissions = require('./permissions/permissions.service.js');
const rolePermissions = require('./role-permissions/role-permissions.service.js');
const confirmCodes = require('./confirm-codes/confirm-codes.service.js');
const dashboards = require('./dashboards/dashboards.service.js');
const blockTypes = require('./block-types/block-types.service.js');
const blocks = require('./blocks/blocks.service.js');
const items = require('./items/items.service.js');
const genres = require('./genres/genres.service.js');
const games = require('./games/games.service.js');
const gameGenres = require('./game-genres/game-genres.service.js');
const statuses = require('./statuses/statuses.service.js');
const platforms = require('./platforms/platforms.service.js');
const formats = require('./formats/formats.service.js');
const categories = require('./categories/categories.service.js');
const creativities = require('./creativities/creativities.service.js');
const creativityPlatforms = require('./creativity-platforms/creativity-platforms.service.js');
const creativityCategories = require('./creativity-categories/creativity-categories.service.js');
const tabs = require('./tabs/tabs.service.js');
const folders = require('./folders/folders.service.js');
const docs = require('./docs/docs.service.js');
const news = require('./news/news.service.js');
const events = require('./events/events.service.js');
const eventUsers = require('./event-users/event-users.service.js');
const newsImages = require('./news-images/news-images.service.js');
const upload = require('./upload/upload.service.js');
const itemTypes = require('./item-types/item-types.service.js');
const links = require('./links/links.service.js');
const similarGames = require('./similar-games/similar-games.service.js');
const departments = require('./departments/departments.service.js');
const mailer = require('./mailer/mailer.service.js');
const gameAssetFolders = require('./game-asset-folders/game-asset-folders.service.js');
const gameAssetDocs = require('./game-asset-docs/game-asset-docs.service.js');
const creativitiesStatuses = require('./creativities-statuses/creativities-statuses.service.js');
const creativitiesFormats = require('./creativities-formats/creativities-formats.service.js');
const sizes = require('./sizes/sizes.service.js');
const creativitiesSizes = require('./creativities-sizes/creativities-sizes.service.js');
const creativityImageSizes = require('./creativity-image-sizes/creativity-image-sizes.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(roles);
  app.configure(users);
  app.configure(permissions);
  app.configure(rolePermissions);
  app.configure(confirmCodes);
  app.configure(dashboards);
  app.configure(blockTypes);
  app.configure(genres);
  app.configure(games);
  app.configure(gameGenres);
  app.configure(statuses);
  app.configure(platforms);
  app.configure(formats);
  app.configure(categories);
  app.configure(creativities);
  app.configure(creativityPlatforms);
  app.configure(creativityCategories);
  app.configure(tabs);
  app.configure(folders);
  app.configure(docs);
  app.configure(news);
  app.configure(items);
  app.configure(blocks);
  app.configure(events);
  app.configure(eventUsers);
  app.configure(newsImages);
  app.configure(upload);
  app.configure(itemTypes);
  app.configure(links);
  app.configure(similarGames);
  app.configure(departments);
  app.configure(mailer);
  app.configure(gameAssetFolders);
  app.configure(gameAssetDocs);
  app.configure(creativitiesStatuses);
  app.configure(creativitiesFormats);
  app.configure(sizes);
  app.configure(creativitiesSizes);
  app.configure(creativityImageSizes);
};
