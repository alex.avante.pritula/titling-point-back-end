const createModel = require('../../models/statuses.model');
const hooks = require('./statuses.hooks');
const path = require('path');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'statuses')
  };

  class Status {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const { query } = params;
      const {SEARCH} = constants;
      let {limit, page, offset} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getStatuses(_limit, _offset)
        .then((statuses) => {
          return statuses;
        });
    }
  }

  app.use('/' + routes['basic'], new Status());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
};