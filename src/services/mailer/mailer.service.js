const hooks = require('./mailer.hooks');
const filters = require('./mailer.filters');
const mailer = require('feathers-mailer');
const smtpTransport = require('nodemailer-smtp-transport');

module.exports = function () {
  const app = this;
  const mailerConfig = app.get('mailer');
  let mailerTransportParams = {
    service: mailerConfig.service,
    host: mailerConfig.host,
    port: mailerConfig.port,
    auth: mailerConfig.auth,
    secure: mailerConfig.secure || false, // use SSL
    tls: {
      rejectUnauthorized: false
    }
  };

  app.use('/mailer', mailer(smtpTransport(mailerTransportParams)));
  const service = app.service('mailer');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
