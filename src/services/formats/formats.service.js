const createModel = require('../../models/formats.model');
const hooks = require('./formats.hooks');
const path = require('path');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const routePrefix = app.get('prefix');
  const constants = app.get('constants');
  const routes = {
    basic: path.join(routePrefix, 'formats')
  };

  class Formats {
    setup(app) {
      this.app = app;
    }

    find(params) {
      const { query } = params;
      const {SEARCH} = constants;
      let {limit, page, offset} = query;

      const _limit = parseInt(limit, 10) || SEARCH['LIMIT'];
      const _page = parseInt(page, 10) - 1 || SEARCH['STEP'];
      const _offset = parseInt(offset, 10) || _page * _limit;

      return Model.getFormats(_limit, _offset)
        .then((formats) => {
          return formats;
        });
    }
  }

  app.use('/' + routes['basic'], new Formats());
  const service = app.service(routes['basic']);

  service.hooks(hooks);
};