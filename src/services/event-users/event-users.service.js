// Initializes the `event-users` service on path `/event-users`
const createService = require('feathers-sequelize');
const createModel = require('../../models/event-users.model');
const hooks = require('./event-users.hooks');
const filters = require('./event-users.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'event-users',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/event-users', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('event-users');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
