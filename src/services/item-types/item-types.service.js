// Initializes the `item-types` service on path `/item-types`
const createService = require('feathers-sequelize');
const createModel = require('../../models/item-types.model');
const hooks = require('./item-types.hooks');
const filters = require('./item-types.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'item-types',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/item-types', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('item-types');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
