/* eslint-disable no-console */
const path = require('path');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const acl = require('../../../utils/aclService');
const mongo = require('../../connectors/mongoService');
const app = feathers();
app.set('root', path.join(__dirname, '../../..'));
app.configure(configuration(app.get('root')));
app.configure(mongo);
app.configure(acl);

module.exports = {
  up: function(){
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addUpRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  },
  down: function() {
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addDownRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  }
};
require('make-runnable');

function addDownRules() {
  let aclInstance = app.get('aclInstance');

  /*FOR ADMIN*/
  let adminResetPasswordProfileRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/users/profile/password'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR ADMIN*/

  /*FOR EDITOR*/
  let editorResetPasswordProfileRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/users/profile/password'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR EDITOR*/

  /*FOR USER*/
  let userResetPasswordProfileRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/users/profile/password'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR USER*/

  return Promise.all([
    adminResetPasswordProfileRule,
    editorResetPasswordProfileRule,
    userResetPasswordProfileRule
  ]);
}

function addUpRules() {
  let aclInstance = app.get('aclInstance');
  return aclInstance.allow([
    {
      roles:['ADMIN'],
      allows:[
        {resources:['api/v1/users/profile/password'], permissions:['patch']},
      ]
    },
    {
      roles:['USER'],
      allows:[
        {resources:['api/v1/users/profile/password'], permissions:['patch']},
      ]
    },
    {
      roles:['EDITOR'],
      allows:[
        {resources:['api/v1/users/profile/password'], permissions:['patch']},
      ]
    },
  ]);
}
