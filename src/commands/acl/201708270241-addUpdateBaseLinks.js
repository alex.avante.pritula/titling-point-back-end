/* eslint-disable no-console */
const path = require('path');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const acl = require('../../../utils/aclService');
const mongo = require('../../connectors/mongoService');
const app = feathers();
app.set('root', path.join(__dirname, '../../..'));
app.configure(configuration(app.get('root')));
app.configure(mongo);
app.configure(acl);

module.exports = {
  up: function(){
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addUpRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  },
  down: function() {
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addDownRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  }
};
require('make-runnable');

function addDownRules() {
  let aclInstance = app.get('aclInstance');

  /*FOR ADMIN*/
  let adminLinksBaseRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/links/base'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR ADMIN*/

  return Promise.all([
    adminLinksBaseRule
  ]);
}

function addUpRules() {
  let aclInstance = app.get('aclInstance');
  return aclInstance.allow([
    {
      roles:['ADMIN'],
      allows:[
        {resources:['api/v1/links/base'], permissions:['patch']},
      ]
    }
  ]);
}
