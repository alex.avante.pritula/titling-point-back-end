/* eslint-disable no-console */
const path = require('path');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const acl = require('../../../utils/aclService');
const mongo = require('../../connectors/mongoService');
const app = feathers();
app.set('root', path.join(__dirname, '../../..'));
app.configure(configuration(app.get('root')));
app.configure(mongo);
app.configure(acl);

module.exports = {
  up: function(){
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addUpRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  },
  down: function() {
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addDownRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  }
};
require('make-runnable');

function addDownRules() {
  let aclInstance = app.get('aclInstance');

  /*FOR ADMIN*/
  let adminUsersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/users'], ['get', 'find', 'patch', 'create', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminUsersAvatarRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/users/avatar'], ['remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminUsersCurrentRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/users/current'], ['get', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminUsersResendRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/users/:userId/resend'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGamesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/games'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGamesRestrictRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/games/restrict'], ['get', 'find', 'create', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGamesSimilarRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/games/:gameId/similar'], ['patch', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGamesImagesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/games/images'], ['create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGamesGenresRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/games/genres'], ['get', 'find', 'create', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminDashboardsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/dashboards'], ['get'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminBlocksRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/blocks'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminBlocksPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/blocks/position'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminItemsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/items/position'], ['patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminItemsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/items'], ['get', 'find', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminNewsImagesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/news-images'], ['remove', 'create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminNewsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/news'], ['patch', 'create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminLinksRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/links'], ['patch', 'create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminLinksDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/links/docs'], ['create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminLinksVideoRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/links/video'], ['create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminLinksBaseRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/links/base'], ['create', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR ADMIN*/

  /*FOR EDITOR*/
  let editorUsersAvatarRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/users/avatar'], ['remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorUsersCurrentRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/users/current'], ['get', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGamesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/games'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGamesSimilarRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/games/:gameId/similar'], ['patch', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGamesImagesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/games/images'], ['create'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGamesGenresRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/games/genres'], ['get', 'find', 'create', 'remove'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorDashboardsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/dashboards'], ['get'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorBlocksRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/blocks'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorItemsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/items'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR EDITOR*/

  /*FOR USER*/
  let userUsersAvatarRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/users/avatar'], ['remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userUsersCurrentRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/users/current'], ['get', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userGamesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/games'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userGamesGenresRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/games/genres'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userDashboardsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/dashboards'], ['get'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userBlocksRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/blocks'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userItemsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/items'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR USER*/

  return Promise.all([adminUsersRule,
    adminUsersAvatarRule,
    adminUsersCurrentRule,
    adminUsersResendRule,
    adminGamesRule,
    adminGamesRestrictRule,
    adminGamesSimilarRule,
    adminGamesImagesRule,
    adminGamesGenresRule,
    adminDashboardsRule,
    adminBlocksRule,
    adminBlocksPositionRule,
    adminItemsPositionRule,
    adminItemsRule,
    adminNewsImagesRule,
    adminNewsRule,
    adminLinksRule,
    adminLinksDocsRule,
    adminLinksVideoRule,
    adminLinksBaseRule,
    editorUsersAvatarRule,
    editorUsersCurrentRule,
    editorGamesRule,
    editorGamesSimilarRule,
    editorGamesImagesRule,
    editorGamesGenresRule,
    editorDashboardsRule,
    editorBlocksRule,
    editorItemsRule,
    userUsersAvatarRule,
    userUsersCurrentRule,
    userGamesRule,
    userGamesGenresRule,
    userDashboardsRule,
    userBlocksRule,
    userItemsRule
  ]);
}

function addUpRules() {
  let aclInstance = app.get('aclInstance');
  return aclInstance.allow([
    {
      roles:['ADMIN'],
      allows:[
        {resources:['api/v1/users'], permissions:['get', 'find', 'patch', 'create', 'remove']},
        {resources:['api/v1/users/avatar'], permissions:['remove', 'patch']},
        {resources:['api/v1/users/current'], permissions:['get', 'patch']},
        {resources:['api/v1/users/:userId/resend'], permissions:['patch']},
        {resources:['api/v1/games'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/games/restrict'], permissions:['get', 'find', 'create', 'remove']},
        {resources:['api/v1/games/:gameId/similar'], permissions:['patch', 'remove']},
        {resources:['api/v1/games/images'], permissions:['create']},
        {resources:['api/v1/games/genres'], permissions:['get', 'find', 'create', 'remove']},
        {resources:['api/v1/dashboards'], permissions:['get']},
        {resources:['api/v1/blocks'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/blocks/position'], permissions:['patch']},
        {resources:['api/v1/items/position'], permissions:['patch']},
        {resources:['api/v1/items'], permissions:['get', 'find', 'remove', 'patch']},
        {resources:['api/v1/news-images'], permissions:['remove', 'create']},
        {resources:['api/v1/news'], permissions:['patch', 'create']},
        {resources:['api/v1/links'], permissions:['patch', 'create']},
        {resources:['api/v1/links/docs'], permissions:['create']},
        {resources:['api/v1/links/video'], permissions:['create']},
        {resources:['api/v1/links/base'], permissions:['create', 'remove']}
      ]
    },
    {
      roles:['EDITOR'],
      allows:[
        {resources:['api/v1/users/avatar'], permissions:['remove', 'patch']},
        {resources:['api/v1/users/current'], permissions:['get', 'patch']},
        {resources:['api/v1/games'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/games/:gameId/similar'], permissions:['patch', 'remove']},
        {resources:['api/v1/games/images'], permissions:['create']},
        {resources:['api/v1/games/genres'], permissions:['get', 'find', 'create', 'remove']},
        {resources:['api/v1/dashboards'], permissions:['get']},
        {resources:['api/v1/blocks'], permissions:['get', 'find']},
        {resources:['api/v1/items'], permissions:['get', 'find']}
      ]
    },{
      roles:['USER'],
      allows:[
        {resources:['api/v1/users/avatar'], permissions:['remove', 'patch']},
        {resources:['api/v1/users/current'], permissions:['get', 'patch']},
        {resources:['api/v1/games'], permissions:['get', 'find']},
        {resources:['api/v1/games/genres'], permissions:['get', 'find']},
        {resources:['api/v1/dashboards'], permissions:['get']},
        {resources:['api/v1/blocks'], permissions:['get', 'find']},
        {resources:['api/v1/items'], permissions:['get', 'find']}
      ]
    }
  ]);


  /*
    ROUTES
   GET /api/v1/users - ADMIN(roleId - only users)
   POST /api/v1/users - ADMIN(roleId - only users)
   DELETE /api/v1/users/avatar - ADMIN, USER, EDITOR
   PATCH /api/v1/users/avatar - ADMIN, EDITOR, USER
   GET /api/v1/users/current - ADMIN, EDITOR, USER
   PATCH /api/v1/users/current - ADMIN, EDITOR, USER
   DELETE /api/v1/users/{id} - ADMIN(only users)
   GET /api/v1/users/{id} - ADMIN(only users)
   PATCH /api/v1/users/{id} - ADMIN(roleId - only users)
   PATCH /api/v1/users/{userId}/resend - ADMIN(only users)

   GET /api/v1/games - ADMIN, USER, EDITOR
   POST /api/v1/games - ADMIN, EDITOR
   DELETE /api/v1/games/restrict - ADMIN(only users)
   GET /api/v1/games/restrict - ADMIN(only users)
   POST /api/v1/games/restrict - ADMIN(only users)
   DELETE /api/v1/games/{id} - ADMIN, EDITOR
   GET /api/v1/games/{id} - ADMIN, USER, EDITOR
   PATCH /api/v1/games/{id} - ADMIN, EDITOR
   DELETE /api/v1/games/{gameId}/similar - ADMIN, EDITOR
   PATCH /api/v1/games/{gameId}/similar - ADMIN, EDITOR
   POST /api/v1/games/images - ADMIN, EDITOR
   DELETE /api/v1/games/genres - ADMIN, EDITOR
   GET /api/v1/games/genres - ADMIN, USER, EDITOR
   POST /api/v1/games/genres - ADMIN, EDITOR


   GET /api/v1/dashboards/{id} - ADMIN, USER, EDITOR

   GET /api/v1/blocks - ADMIN, USER, EDITOR
   POST /api/v1/blocks - ADMIN
   PATCH /api/v1/blocks/position/{id} - ADMIN
   DELETE /api/v1/blocks/{id} - ADMIN
   GET /api/v1/blocks/{id} - ADMIN, USER, EDITOR
   PATCH /api/v1/blocks/{id} - ADMIN

   PATCH /api/v1/items/position/{id} - ADMIN
   DELETE /api/v1/items/{id} - ADMIN
   GET /api/v1/items/{id} - ADMIN, USER, EDITOR
   PATCH /api/v1/items/{id} - ADMIN
   GET /api/v1/items - ADMIN, USER, EDITOR

   POST /api/v1/news-images - ADMIN
   DELETE /api/v1/news-images/{id} - ADMIN

   POST /api/v1/news - ADMIN
   PATCH /api/v1/news/{id} - ADMIN

   POST /api/v1/links - ADMIN
   POST /api/v1/links/docs - ADMIN
   POST /api/v1/links/video - ADMIN
   PATCH /api/v1/links/{id} - ADMIN

   POST /api/v1/links/base - ADMIN
   DELETE /api/v1/links/base/{id} - ADMIN*/
}
