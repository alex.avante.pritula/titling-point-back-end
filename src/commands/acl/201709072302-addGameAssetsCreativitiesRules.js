/* eslint-disable no-console */
const path = require('path');
const feathers = require('feathers');
const configuration = require('feathers-configuration');
const acl = require('../../../utils/aclService');
const mongo = require('../../connectors/mongoService');
const app = feathers();
app.set('root', path.join(__dirname, '../../..'));
app.configure(configuration(app.get('root')));
app.configure(mongo);
app.configure(acl);

module.exports = {
  up: function(){
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addUpRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  },
  down: function() {
    let aclService = app.get('aclService');
    aclService.init().then(() => {
      addDownRules().then(() => {
        console.log('SUCCESS');
        process.exit();
      }).catch((err) => {
        console.log('ERROR: ', err);
        process.exit();
      });
    });
  }
};
require('make-runnable');

function addDownRules() {
  let aclInstance = app.get('aclInstance');

  /*FOR ADMIN*/
  let adminPlatformsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/platforms'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminStatusesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/statuses'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminFormatsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/formats'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesTabsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/tabs'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesTabsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/tabs/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesFoldersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/folders'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/folders/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesTabsFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/tabs/folders/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/docs'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/docs/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminCreativitiesFoldersDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/creativities/folders/docs/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGameAssetFoldersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/game-asset/folders'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGameAssetFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/game-asset/folders/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGameAssetDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/game-asset/docs'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let adminGameAssetDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('ADMIN', ['api/v1/game-asset/docs/position'], ['get', 'find', 'create', 'remove', 'patch'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR ADMIN*/

  /*FOR EDITOR*/
  let editorPlatformsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/platforms'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorStatusesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/statuses'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorFormatsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/formats'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesTabsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/tabs'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesTabsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/tabs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesFoldersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/folders'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/folders/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesTabsFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/tabs/folders/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/docs'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/docs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorCreativitiesFoldersDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/creativities/folders/docs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGameAssetFoldersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/game-asset/folders'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGameAssetFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/game-asset/folders/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGameAssetDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/game-asset/docs'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let editorGameAssetDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('EDITOR', ['api/v1/game-asset/docs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR EDITOR*/

  /*FOR USER*/
  let userPlatformsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/platforms'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userStatusesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/statuses'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userFormatsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/formats'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesTabsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/tabs'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesTabsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/tabs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesFoldersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/folders'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/folders/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesTabsFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/tabs/folders/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/docs'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/docs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userCreativitiesFoldersDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/creativities/folders/docs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userGameAssetFoldersRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/game-asset/folders'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userGameAssetFoldersPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/game-asset/folders/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userGameAssetDocsRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/game-asset/docs'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });

  let userGameAssetDocsPositionRule = new Promise(function (resolve, reject) {
    aclInstance.removeAllow('USER', ['api/v1/game-asset/docs/position'], ['get', 'find'], function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();
    });
  });
  /*FOR USER*/

  return Promise.all([
    adminPlatformsRule,
    adminStatusesRule,
    adminFormatsRule,
    adminCreativitiesRule,
    adminCreativitiesTabsRule,
    adminCreativitiesTabsPositionRule,
    adminCreativitiesFoldersRule,
    adminCreativitiesFoldersPositionRule,
    adminCreativitiesTabsFoldersPositionRule,
    adminCreativitiesDocsRule,
    adminCreativitiesDocsPositionRule,
    adminCreativitiesFoldersDocsPositionRule,
    adminGameAssetFoldersRule,
    adminGameAssetFoldersPositionRule,
    adminGameAssetDocsRule,
    adminGameAssetDocsPositionRule,
    adminGameAssetDocsPositionRule,
    editorPlatformsRule,
    editorStatusesRule,
    editorFormatsRule,
    editorCreativitiesRule,
    editorCreativitiesTabsRule,
    editorCreativitiesTabsPositionRule,
    editorCreativitiesFoldersRule,
    editorCreativitiesFoldersPositionRule,
    editorCreativitiesTabsFoldersPositionRule,
    editorCreativitiesDocsRule,
    editorCreativitiesDocsPositionRule,
    editorCreativitiesFoldersDocsPositionRule,
    editorGameAssetFoldersRule,
    editorGameAssetFoldersPositionRule,
    editorGameAssetDocsRule,
    editorGameAssetDocsPositionRule,
    userPlatformsRule,
    userStatusesRule,
    userFormatsRule,
    userCreativitiesRule,
    userCreativitiesTabsRule,
    userCreativitiesTabsPositionRule,
    userCreativitiesFoldersRule,
    userCreativitiesFoldersPositionRule,
    userCreativitiesTabsFoldersPositionRule,
    userCreativitiesDocsRule,
    userCreativitiesDocsPositionRule,
    userCreativitiesFoldersDocsPositionRule,
    userGameAssetFoldersRule,
    userGameAssetFoldersPositionRule,
    userGameAssetFoldersPositionRule,
    userGameAssetDocsRule,
    userGameAssetDocsPositionRule
  ]);
}

function addUpRules() {
  let aclInstance = app.get('aclInstance');
  return aclInstance.allow([
    {
      roles:['ADMIN'],
      allows:[
        {resources:['api/v1/platforms'], permissions:['get', 'find']},
        {resources:['api/v1/statuses'], permissions:['get', 'find']},
        {resources:['api/v1/formats'], permissions:['get', 'find']},
        {resources:['api/v1/creativities'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/tabs'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/tabs/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/folders'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/folders/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/tabs/folders/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/docs'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/docs/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/creativities/folders/docs/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/game-asset/folders'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/game-asset/folders/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/game-asset/docs'], permissions:['get', 'find', 'create', 'remove', 'patch']},
        {resources:['api/v1/game-asset/docs/position'], permissions:['get', 'find', 'create', 'remove', 'patch']},
      ]
    },
    {
      roles:['USER'],
      allows:[
        {resources:['api/v1/platforms'], permissions:['get', 'find']},
        {resources:['api/v1/statuses'], permissions:['get', 'find']},
        {resources:['api/v1/formats'], permissions:['get', 'find']},
        {resources:['api/v1/creativities'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/tabs'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/tabs/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/folders'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/folders/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/tabs/folders/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/docs'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/docs/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/folders/docs/position'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/folders'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/folders/position'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/docs'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/docs/position'], permissions:['get', 'find']},
      ]
    },
    {
      roles:['EDITOR'],
      allows:[
        {resources:['api/v1/platforms'], permissions:['get', 'find']},
        {resources:['api/v1/statuses'], permissions:['get', 'find']},
        {resources:['api/v1/formats'], permissions:['get', 'find']},
        {resources:['api/v1/creativities'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/tabs'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/tabs/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/folders'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/folders/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/tabs/folders/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/docs'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/docs/position'], permissions:['get', 'find']},
        {resources:['api/v1/creativities/folders/docs/position'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/folders'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/folders/position'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/docs'], permissions:['get', 'find']},
        {resources:['api/v1/game-asset/docs/position'], permissions:['get', 'find']},
      ]
    }
  ]);
}

/*
/api/v1/platforms
/api/v1/statuses
/api/v1/formats
/api/v1/creativities
/api/v1/creativities/{id}
/api/v1/creativities/tabs
/api/v1/creativities/tabs/position/{id}
/api/v1/creativities/tabs/{id}
/api/v1/creativities/folders
/api/v1/creativities/folders/position/{id}
/api/v1/creativities/tabs/folders/position/{id}
/api/v1/creativities/folders/{id}
/api/v1/creativities/docs
/api/v1/creativities/docs/position/{id}
/api/v1/creativities/folders/docs/position/{id}
/api/v1/creativities/docs/{id}
/api/v1/game-asset/folders
/api/v1/game-asset/folders/{id}
/api/v1/game-asset/folders/position/{id}
/api/v1/game-asset/docs
/api/v1/game-asset/docs/{id}
/api/v1/game-asset/docs/position/{id}*/
