const assert = require('assert');
const app = require('../../src/app');

describe('\'game-asset-folders\' service', () => {
  it('registered the service', () => {
    const service = app.service('game-asset-folders');

    assert.ok(service, 'Registered the service');
  });
});
