const assert = require('assert');
const app = require('../../src/app');

describe('\'tabs\' service', () => {
  it('registered the service', () => {
    const service = app.service('tabs');

    assert.ok(service, 'Registered the service');
  });
});
