const assert = require('assert');
const app = require('../../src/app');

describe('\'event-users\' service', () => {
  it('registered the service', () => {
    const service = app.service('event-users');

    assert.ok(service, 'Registered the service');
  });
});
