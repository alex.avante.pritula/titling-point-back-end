const assert = require('assert');
const app = require('../../src/app');

describe('\'blocks\' service', () => {

  it('registered the service', () => {
    const service = app.service('api/v1/blocks');

    assert.ok(service, 'Registered the service');
  });

  it('should create new block - news', function (done) {
    app.service('api/v1/blocks')
      .create({
        name: 'Test',
        dashboardId: 1,
        blockTypeId: 1
      })
      .then(block => {
        assert.ok(block.id);
        assert.ok(block.position);
        assert.equal(block.position, 1);
        assert.ok(block.name);
        assert.equal(block.name, 'Test');
        assert.ok(block.dashboardId);
        assert.equal(block.dashboardId, 1);
        assert.ok(block.blockTypeId);
        assert.equal(block.blockTypeId, 1);
        done();
      })
      .catch(done);
  });

  it('should create new block - events', function (done) {
    app.service('api/v1/blocks')
      .create({
        name: 'Test',
        dashboardId: 1,
        blockTypeId: 2
      })
      .then(block => {
        assert.ok(block.id);
        assert.ok(block.position);
        assert.equal(block.position, 1);
        assert.ok(block.name);
        assert.equal(block.name, 'Test');
        assert.ok(block.dashboardId);
        assert.equal(block.dashboardId, 1);
        assert.ok(block.blockTypeId);
        assert.equal(block.blockTypeId, 2);
        done();
      })
      .catch(done);
  });

  it('should not create new block with not existing blockTypeId', function (done) {
    app.service('api/v1/blocks')
      .create({
        name: 'Test',
        dashboardId: 1,
        blockTypeId: 999
      })
      .then(block => {
        done(block);
      })
      .catch(() => {
        done();
      });
  });

  it('should not create new block with not existing dashboardId', function (done) {
    app.service('api/v1/blocks')
      .create({
        name: 'Test',
        dashboardId: 999,
        blockTypeId: 1
      })
      .then(block => {
        done(block);
      })
      .catch(() => {
        done();
      });
  });

  it('should not create new block without name', function (done) {
    app.service('api/v1/blocks')
      .create({
        name: null,
        dashboardId: 1,
        blockTypeId: 1
      })
      .then(block => {
        done(block);
      })
      .catch(() => {
        done();
      });
  });
});
