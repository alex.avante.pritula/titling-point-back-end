const assert = require('assert');
const app = require('../../src/app');

describe('\'similar-games\' service', () => {
  it('registered the service', () => {
    const service = app.service('similar-games');

    assert.ok(service, 'Registered the service');
  });
});
