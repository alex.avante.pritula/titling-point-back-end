const assert = require('assert');
const app = require('../../src/app');

describe('\'creativities\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativities');

    assert.ok(service, 'Registered the service');
  });
});
