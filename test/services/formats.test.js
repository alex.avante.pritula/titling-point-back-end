const assert = require('assert');
const app = require('../../src/app');

describe('\'formats\' service', () => {
  it('registered the service', () => {
    const service = app.service('formats');

    assert.ok(service, 'Registered the service');
  });
});
