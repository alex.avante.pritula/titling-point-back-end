const assert = require('assert');
const app = require('../../src/app');

describe('\'creativities_formats\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativities-formats');

    assert.ok(service, 'Registered the service');
  });
});
