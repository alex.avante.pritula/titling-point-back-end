const assert = require('assert');
const app = require('../../src/app');

describe('\'creativities_statuses\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativities-statuses');

    assert.ok(service, 'Registered the service');
  });
});
