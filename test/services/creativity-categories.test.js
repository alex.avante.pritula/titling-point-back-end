const assert = require('assert');
const app = require('../../src/app');

describe('\'creativity-categories\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativity-categories');

    assert.ok(service, 'Registered the service');
  });
});
