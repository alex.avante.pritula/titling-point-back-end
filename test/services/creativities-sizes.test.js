const assert = require('assert');
const app = require('../../src/app');

describe('\'creativities-sizes\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativities-sizes');

    assert.ok(service, 'Registered the service');
  });
});
