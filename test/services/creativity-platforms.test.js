const assert = require('assert');
const app = require('../../src/app');

describe('\'creativity-platforms\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativity-platforms');

    assert.ok(service, 'Registered the service');
  });
});
