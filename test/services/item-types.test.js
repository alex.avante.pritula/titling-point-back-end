const assert = require('assert');
const app = require('../../src/app');

describe('\'item-types\' service', () => {
  it('registered the service', () => {
    const service = app.service('item-types');

    assert.ok(service, 'Registered the service');
  });
});
