const assert = require('assert');
const app = require('../../src/app');

describe('\'links\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/links');

    assert.ok(service, 'Registered the service');
  });
});
