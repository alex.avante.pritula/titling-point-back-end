const assert = require('assert');
const app = require('../../src/app');

describe('\'game-genres\' service', () => {
  it('registered the service', () => {
    const service = app.service('game-genres');

    assert.ok(service, 'Registered the service');
  });
});
