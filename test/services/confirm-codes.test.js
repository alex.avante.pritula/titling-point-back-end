const assert = require('assert');
const app = require('../../src/app');

describe('\'confirm-codes\' service', () => {
  it('registered the service', () => {
    const service = app.service('confirm-codes');

    assert.ok(service, 'Registered the service');
  });
});
