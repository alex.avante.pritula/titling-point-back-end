const assert = require('assert');
const app = require('../../src/app');

describe('\'creativity-image-sizes\' service', () => {
  it('registered the service', () => {
    const service = app.service('creativity-image-sizes');

    assert.ok(service, 'Registered the service');
  });
});
