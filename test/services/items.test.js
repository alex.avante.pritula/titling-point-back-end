const assert = require('assert');
const app = require('../../src/app');

describe('\'items\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/items');

    assert.ok(service, 'Registered the service');
  });
});
