const assert = require('assert');
const app = require('../../src/app');

describe('\'game-asset-docs\' service', () => {
  it('registered the service', () => {
    const service = app.service('game-asset-docs');

    assert.ok(service, 'Registered the service');
  });
});
