/* eslint-disable no-console */
const Promise   = require('bluebird');
const bcrypt = require('bcryptjs');
const genSalt = Promise.promisify(bcrypt.genSalt);
const hash = Promise.promisify(bcrypt.hash);

(()=>{
  const { argv } = process;
  genSalt(10).then((salt)=>{
    return hash(argv[2], salt);
  }).then(function(password){
    console.log(password);
    process.exit();
  }).catch((error)=>{
    console.log(error);
    process.exit();
  });
})();
