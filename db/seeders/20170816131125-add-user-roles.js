'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('roles', [{
      id: 2,
      role: 'ADMIN',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 3,
      role: 'EDITOR',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 4,
      role: 'USER',
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
                    delete from roles
                    where id in (2,3,4) `, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  }
};