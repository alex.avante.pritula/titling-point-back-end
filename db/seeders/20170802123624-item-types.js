'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('item_types', [{
      id: 1,
      type: 'News block',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 2,
      type: 'New document',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 3,
      type: 'Textarea',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 4,
      type: 'List of links',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 5,
      type: 'Video embed',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 6,
      type: 'Image Link',
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
                    delete from item_types
                    where id in (1,2,3,4,5,6) `, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  }
};