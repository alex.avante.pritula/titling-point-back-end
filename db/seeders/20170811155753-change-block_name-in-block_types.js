'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
            UPDATE block_types SET type = 'Default' 
            where type = 'News'`, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
            UPDATE block_types SET type = 'News' 
            where type = 'Default'`, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  }
};