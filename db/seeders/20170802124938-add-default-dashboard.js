'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('dashboards', [{
      id: 1,
      name: '',
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
                    delete from dashboards
                    where id in (1) `, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  }
};