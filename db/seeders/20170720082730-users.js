'use strict';

module.exports = {
  up: function (queryInterface /*, Sequelize*/) {
    return queryInterface.bulkInsert('users', [{
      email: 'super_admin_test@gmail.com',
      role_id: 1,
      password: '$2a$10$wSrASuTF3dtgwsaJ5x.q9eMwYcaSmiQPMwbnutESJS7LWr7IK7YMW',
      is_verified: true,
      is_enabled: true
    }], {});
  },

  down: function (queryInterface /*, Sequelize*/) {
    return queryInterface.bulkDelete('users', null, {});
  }
};
