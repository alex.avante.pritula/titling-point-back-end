'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('departments', [{
      id: 1,
      name: 'User Acquisition',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 2,
      name: 'Marketing',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 3,
      name: 'Design & Analysis',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 4,
      name: 'Executive',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 5,
      name: 'Creative Services',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 6,
      name: 'Business Development',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 7,
      name: 'Talent',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 8,
      name: 'Live Operations',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 9,
      name: 'Product Support',
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
                    delete from item_types
                    where id in (1,2,3,4,5,6,7,8,9) `, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  }
};