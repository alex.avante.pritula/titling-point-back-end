'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('block_types', [{
      id: 1,
      type: 'News',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      id: 2,
      type: 'Event',
      created_at: new Date(),
      updated_at: new Date()
    }]);
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return Promise.all([
        queryInterface.sequelize.query(`
                    delete from block_types
                    where id in (1,2) `, {
            type: Sequelize.QueryTypes.DELETE,
            transaction: transaction
          })
      ]);
    });
  }
};