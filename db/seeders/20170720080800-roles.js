'use strict';

module.exports = {
  up: function (queryInterface /*, Sequelize*/) {
    return queryInterface.bulkInsert('roles', [{
      role: 'SUPER_ADMIN'
    }], {});
  },

  down: function (queryInterface /*, Sequelize*/) {
    return queryInterface.bulkDelete('roles', null, {});
  }
};
