'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('news_images', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      image_url: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      news_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'news',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },      

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('news_images');
  }
};