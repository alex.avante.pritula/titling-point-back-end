'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('docs', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      name: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      document_url: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      folder_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'folders',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },      

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('docs');
  }
};