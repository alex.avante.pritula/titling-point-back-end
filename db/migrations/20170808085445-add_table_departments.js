'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('departments', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },

      name: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },

      created_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('departments');
  }
};
