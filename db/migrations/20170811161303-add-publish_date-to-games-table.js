'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'games',
        'publish_date',
        {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('games', 'publish_date')
    ];
  }
};