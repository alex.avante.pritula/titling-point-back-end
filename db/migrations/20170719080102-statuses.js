'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('statuses', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      status: {
        type     : Sequelize.STRING,
        allowNull: false,
        unique   : true
      },

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('statuses');
  }
};