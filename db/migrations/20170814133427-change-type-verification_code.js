'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.changeColumn(
        'confirm_codes',
        'code',
        {
          type: Sequelize.STRING
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.changeColumn(
        'confirm_codes',
        'code',
        {
          type: Sequelize.INTEGER
        }
      )
    ];
  }
};
