'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'games',
        'owner_id',
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'SET NULL'
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('games', 'owner_id')
    ];
  }
};