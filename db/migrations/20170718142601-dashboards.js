'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('dashboards', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },
        
        name: {
          type     : Sequelize.STRING,
          allowNull: false
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      },{
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query(`
          CREATE INDEX name ON dashboards (name);
        `, {
            type: Sequelize.QueryTypes.RAW,
            transaction: transaction
          });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('dashboards');
  }
};