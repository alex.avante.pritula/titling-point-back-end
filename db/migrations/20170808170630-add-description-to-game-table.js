'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'games',
        'description',
        {
          type: Sequelize.STRING,
          allowNull: true
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('games', 'description')
    ];
  }
};