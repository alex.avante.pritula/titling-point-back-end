'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.changeColumn(
        'news',
        'description',
        {
          type: Sequelize.TEXT
        }
      )
    ];
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.changeColumn(
        'news',
        'description',
        {
          type: Sequelize.STRING
        }
      )
    ];
  }
};
