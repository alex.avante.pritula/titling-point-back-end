'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('game_asset_docs', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      name: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      document_url: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      folder_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'game_asset_docs',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('game_asset_docs');
  }
};
