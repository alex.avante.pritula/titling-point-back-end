'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('creativity_image_sizes', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        creativity_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model: 'creativities',
            key  : 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        size_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'sizes',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        file_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'docs',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        is_main_image: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('creativity_image_sizes');
  }
};