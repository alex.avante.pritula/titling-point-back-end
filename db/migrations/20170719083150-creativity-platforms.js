'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('creativity_platforms', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      platform_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'platforms',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },

      creativity_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'creativities',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },      

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('creativity_platforms');
  }
};