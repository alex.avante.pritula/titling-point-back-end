'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('users', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        email: {
          type     : Sequelize.STRING,
          allowNull: false,
          unique   : true
        },

        password: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        name: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        job: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        biography: {
          type     : Sequelize.STRING,
          allowNull: true
        },   

        contact_info: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        avatar_url: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        role_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model: 'roles',
            key  : 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        is_verified: {
          type        : Sequelize.BOOLEAN,
          allowNull   : false,
          defaultValue: false
        },

        is_enabled: {
          type        : Sequelize.BOOLEAN,
          allowNull   : false,
          defaultValue: true
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      },{
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query(`
          CREATE INDEX email ON users (email);
        `, {
            type: Sequelize.QueryTypes.RAW,
            transaction: transaction
          });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('users');
  }
};
