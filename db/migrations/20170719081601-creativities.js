'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('creativities', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        name: {
          type     : Sequelize.STRING,
          allowNull: false
        },

        main_image_url: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        avalable_sizes: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        avalable_languages: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        feedback: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        jira_ticket_url: {
          type     : Sequelize.STRING,
          allowNull: true
        },

        is_used: {
          type        : Sequelize.BOOLEAN,
          allowNull   : false,
          defaultValue: true
        },

        game_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model: 'games',
            key  : 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        status_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model: 'statuses',
            key  : 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        format_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model: 'formats',
            key  : 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      },{
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query(`
          CREATE INDEX jira_ticket_url ON creativities (jira_ticket_url);
        `, {
            type: Sequelize.QueryTypes.RAW,
            transaction: transaction
          });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('creativities');
  }
};
