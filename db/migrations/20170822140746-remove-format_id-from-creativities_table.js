'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('creativities', 'format_id', {
        transaction: t
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'creativities',
        'format_id',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'formats',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }, {
          transaction: t
        });
    });
  }
};