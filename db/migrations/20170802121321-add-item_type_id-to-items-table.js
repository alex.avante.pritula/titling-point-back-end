'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'items',
        'item_type_id',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'item_types',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('items', 'item_type_id')
    ];
  }
};