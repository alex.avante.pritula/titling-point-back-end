'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'creativities',
        'owner_id',
        {
          type: Sequelize.INTEGER,
          allowNull: true,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'SET NULL'
        }, {
          transaction: t
        });
    });
  },

  down: function (queryInterface /*, Sequelize*/) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('creativities', 'owner_id', {
        transaction: t
      });
    });
  }
};