'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('items', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      name: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      block_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'blocks',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },

      icon_url: {
        type     : Sequelize.STRING,
        allowNull: true
      },

      source_url: {
        type     : Sequelize.STRING,
        allowNull: true
      },

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('items');
  }
};