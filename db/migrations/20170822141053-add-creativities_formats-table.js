'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('creativities_formats', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        format_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'formats',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        creativity_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'creativities',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('creativities_formats');
  }
};