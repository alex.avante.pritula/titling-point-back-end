'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'items',
        'position',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
        }
      )
    ];
  },

  down: function (queryInterface /*, Sequelize*/) {
    return [
      queryInterface.removeColumn('items', 'position')
    ];
  }
};