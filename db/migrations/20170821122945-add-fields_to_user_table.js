'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'users',
        'verifyToken',
        {
          type: Sequelize.STRING
        }, {
          transaction: t
        }).then(() => {
        return queryInterface.addColumn(
          'users',
          'resetToken',
          {
            type: Sequelize.STRING
          }, {
            transaction: t
          });
      });
    });
  },

  down: function (queryInterface /*, Sequelize*/) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('users', 'verifyToken', {
        transaction: t
      }).then(() => {
        return queryInterface.removeColumn('users', 'resetToken', {
          transaction: t
        });
      });
    });
  }
};