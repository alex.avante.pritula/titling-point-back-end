'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('confirm_codes', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      code: {
        type     : Sequelize.INTEGER,
        allowNull: false,
        unique   : true
      },

      user_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'users',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('confirm_codes');
  }
};
