'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'game_asset_docs',
        'position',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
        }, {
          transaction: t
        });
    });
  },

  down: function (queryInterface /*, Sequelize*/) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('game_asset_docs', 'position', {
        transaction: t
      });
    });
  }
};