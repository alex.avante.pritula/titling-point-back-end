'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'news_images',
        'position',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 1,
        }, {
          transaction: t
        });
    });
  },

  down: function (queryInterface /*, Sequelize*/) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('news_images', 'position', {
        transaction: t
      });
    });
  }
};