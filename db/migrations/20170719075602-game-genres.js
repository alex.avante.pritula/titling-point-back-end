'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('game_genres', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      name: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      game_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'games',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },

      genre_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'genres',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('game_genres');
  }
};
