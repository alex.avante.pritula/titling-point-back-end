'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('CREATE UNIQUE INDEX unique_game_similar_game_idx ON similar_games (game_id, similar_game_id);', {
      type: Sequelize.QueryTypes.RAW
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('DROP INDEX unique_game_similar_game_idx;', {
      type: Sequelize.QueryTypes.RAW
    });
  }
};
