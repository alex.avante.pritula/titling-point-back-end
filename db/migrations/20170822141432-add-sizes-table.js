'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('sizes', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },

        width: {
          type: Sequelize.INTEGER,
          allowNull: true
        },

        height: {
          type: Sequelize.INTEGER,
          allowNull: true
        },

        created_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      }, {
        transaction: transaction
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('sizes');
  }
};