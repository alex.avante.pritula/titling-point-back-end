'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return [
      queryInterface.removeColumn('game_genres', 'name')
    ];
  },

  down: function (queryInterface, Sequelize) {
    return [
      queryInterface.addColumn(
        'game_genres',
        'name',
        {
          type: Sequelize.STRING,
          allowNull: true
        }
      )
    ];
  }
};
