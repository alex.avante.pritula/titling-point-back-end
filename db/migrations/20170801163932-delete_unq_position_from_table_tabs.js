'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.sequelize.query('ALTER TABLE "public"."tabs" DROP COLUMN "position"; ', {
        type: Sequelize.QueryTypes.RAW,
        transaction: t
      }).then(() => {
        return queryInterface.sequelize.query('ALTER TABLE "public"."tabs" ADD COLUMN "position" INTEGER;', {
          type: Sequelize.QueryTypes.RAW,
          transaction: t
        });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.sequelize.query('ALTER TABLE "public"."tabs" DROP COLUMN "position"; ', {
        type: Sequelize.QueryTypes.RAW,
        transaction: t
      }).then(() => {
        return queryInterface.sequelize.query('ALTER TABLE "public"."tabs" ADD COLUMN "position" INTEGER UNIQUE; ', {
          type: Sequelize.QueryTypes.RAW,
          transaction: t
        });
      })
    });
  }
};
