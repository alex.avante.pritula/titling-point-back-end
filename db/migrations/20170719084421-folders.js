'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('folders', {
      id: {
        type         : Sequelize.INTEGER,
        primaryKey   : true,
        autoIncrement: true
      },

      position: {
        type     : Sequelize.INTEGER,
        allowNull: false,
        unique   : true
      },

      name: {
        type     : Sequelize.STRING,
        allowNull: false
      },

      tab_id: {
        type      : Sequelize.INTEGER,
        allowNull : false,
        references: {
          model: 'tabs',
          key  : 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },      

      created_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      },

      updated_at: {
        type        : Sequelize.DATE,
        defaultValue: Sequelize.literal('now()')
      }
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('folders');
  }
};