'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('game_asset_docs', 'folder_id', {
        transaction: t
      }).then(() => {
        return queryInterface.addColumn(
          'game_asset_docs',
          'folder_id',
          {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'game_asset_folders',
              key: 'id'
            },
            onUpdate: 'cascade',
            onDelete: 'cascade'
          }, {
            transaction: t
          });
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('game_asset_docs', 'folder_id', {
        transaction: t
      }).then(() => {
        return queryInterface.addColumn(
          'game_asset_docs',
          'folder_id',
          {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'game_asset_docs',
              key: 'id'
            },
            onUpdate: 'cascade',
            onDelete: 'cascade'
          }, {
            transaction: t
          });
      });
    });
  }
};