'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('creativities', 'status_id', {
        transaction: t
      });
    });
  },

  down: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'creativities',
        'status_id',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'statuses',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }, {
          transaction: t
        });
    });
  }
};