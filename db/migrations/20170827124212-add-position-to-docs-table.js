'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.addColumn(
        'docs',
        'position',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 1,
        }, {
          transaction: t
        });
    });
  },

  down: function (queryInterface /*, Sequelize*/) {
    const sequelize = queryInterface.sequelize;
    return sequelize.transaction(function (t) {
      return queryInterface.removeColumn('docs', 'position', {
        transaction: t
      });
    });
  }
};