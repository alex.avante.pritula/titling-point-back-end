'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((transaction) => {
      return queryInterface.createTable('events', {
        id: {
          type         : Sequelize.INTEGER,
          primaryKey   : true,
          autoIncrement: true
        },

        title: {
          type     : Sequelize.STRING,
          allowNull: false
        },

        description: {
          type     : Sequelize.STRING,
          allowNull: true
        },
        
        outlook_id: {
          type     : Sequelize.STRING,
          allowNull: false
        },

        item_id: {
          type      : Sequelize.INTEGER,
          allowNull : false,
          references: {
            model: 'items',
            key  : 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },      

        date: {
          type     : Sequelize.DATE,
          allowNull: false
        },

        created_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        },

        updated_at: {
          type        : Sequelize.DATE,
          defaultValue: Sequelize.literal('now()')
        }
      },{
        transaction: transaction
      }).then(() => {
        return queryInterface.sequelize.query(`
          CREATE INDEX outlook_id ON events (outlook_id);
        `, {
            type: Sequelize.QueryTypes.RAW,
            transaction: transaction
          });
      });
    });
  },

  down: function (queryInterface /*, Sequelize */) {
    return queryInterface.dropTable('events');
  }
};