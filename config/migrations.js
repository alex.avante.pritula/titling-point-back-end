const developmentUrl = require('./default.json').db.postgresConnectionUrl;
const productionUrl = require('./production.json').db.postgresConnectionUrl;

module.exports = {
  development: {
    dialect: 'postgres',
    url: developmentUrl
  },
  production: {
    dialect: 'postgres',
    url: productionUrl
  }
};
